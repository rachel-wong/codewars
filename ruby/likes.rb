# Implement a function likes :: [String] -> String, which must take in input array, containing the names of people who like an item. It must return the display text as shown in the examples:

def likes(names)
    case 
    when names.length == 0 
        p "No one likes this"
    when names.length == 1
        p "#{names[0]} likes this"
    when names.length == 2
        p "#{names[0]} and #{names[1]} like this"
    when names.length == 3
        p "#{names[0]}, #{names[1]} and #{names[2]} like this"
    when names.length == 4
        p "#{names[0]}, #{names[1]} and #{names.length - 2} others like this"
    when names.length > 4
        p "#{names[0]}, #{njjames[1]} and #{names.length - 2} other like this"
    end
end

# likes([]) # => must be "no one likes this"
# likes(["Peter"]) #=> must be "Peter likes this"
# likes(["Jacob", "Alex"]) # => must be "Jacob and Alex like this"
# likes(["Max", "John", "Mark"]) # => must be "Max, John and Mark like this"
# likes(["Alex", "Jacob", "Mark", "Max"]) #=> must be "Alex, Jacob and 2 others like this"
likes(["Alex", "Jacob", "Mark", "Max", "Sean", "baby", "Tramp"]) 