// If we were to set up a Tic-Tac-Toe game, we would want to know whether the board's current state is solved, wouldn't we? Our goal is to create a function that will check that for us!
// Assume that the board comes in the form of a 3x3 array, where the value is 0 if a spot is empty, 1 if it is an "X", or 2 if it is an "O", like so:
// [[0, 0, 1],
//  [0, 1, 2],
//  [2, 1, 0]]
// We want our function to return:
// -1 if the board is not yet finished (there are empty spots),
// 1 if "X" won,
// 2 if "O" won,
// 0 if it's a cat's game (i.e. a draw).
// You may assume that the board passed in is valid in the context of a game of Tic-Tac-Toe.

// What is a Win? (either 2 or 1. * is wildcard)
// [[*, *, 2],[*, 2, *],[2, *, *]] // diagonal
// [[2, *, *],[*, 2, *],[*, *, 2]] // diagonal
// [[*, *, 2],[*, *, 2],[*, *, 2]] // vertical
// [[*, 2, *],[*, 2, *],[*, 2, *]] // vertical
// [[2, *, *],[2, *, *],[2, *, *]] // vertical
// [[2, 2, 2],[*, *, *],[*, *, *]] // horizontal
// [[*, *, *],[2, 2, 2],[*, *, *]] // horizontal
// [[*, *, *],[*, *, *],[2, 2, 2]] // horizontal

function isSolved(board) {
  // tried regex, too inpractical
  // board = board.join('').replace(/,/g, '');
  // console.log('board:', board)
  // let horizontalRegex = /222|111/
  // let verticalRegex = /2..2..2|1..1..1/
  // let diagonalRegex = /2...2...2|2....2....2|1...1...1|1....1....1/
  // let unfinishedRegex = /0/
  if (board.length > 3 || board.length < 3) return false // handling silliness

  for (let x = 0; x < board.length; x++) {
    // console.log("x:", board[x]) // prints out each row
    for (let y = 0; y < x; y++) {
      console.log("x", board[x][y])
      // arr.push(board[x][y])
      // console.log(arr)
    }
  }
}

console.log(isSolved([
  [1, 1, 1],
  [0, 1, 2],
  [0, 1, 2]
])) //  === 2);