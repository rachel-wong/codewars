function reverseNumber(n) {
  if (n === 0) {
    return 0
  } else if (n > 0) {
    n = n.toString().split("")
    if (n.length === 1) return Number(n[0])
    return parseInt(Number(n.reverse().join("")), 10)
  } else {
    n = n.toString().split("").slice(1, n.length).reverse().join("")
    return Number("-" + n)
  }
}

console.log(reverseNumber(321))
console.log(reverseNumber(-654))
console.log(reverseNumber(1000))
console.log(reverseNumber(0))

// other solutions
function reverseNumber(n) {
  let isNegative = n < 0;
  let reverseAsString = Math.abs(n).toString().split('').reverse().join('');
  let result = Number(reverseAsString);

  return isNegative ? -result : result;
}

reverseNumber = n => (n > 0 ? 1 : -1) * Math.abs(n).toString().split('').reverse().join('')

function reverseNumber(n) {
  return Math.sign(n) * Math.abs(n)
    .toString()
    .split ``
    .reverse()
    .join ``;
}

function reverseNumber(n) {
  return (n + '').match(/\d/g).reverse().join('') * (n < 0 ? -1 : 1)
}