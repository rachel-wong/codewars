// Complete the solution so that it splits the string into pairs of two characters. If the string contains an odd number of characters then it should replace the missing second character of the final pair with an underscore ('_').

// solution('abc') // should return ['ab', 'c_']
// solution('abcdef') // should return ['ab', 'cd', 'ef']

function splitPairs(string) {
  let pairs = []
  for (let i = 0; i < string.length; i += 2) {
    pairs.push(string.substring(i, i + 2))
  }

  for (let x = 0; x < pairs.length; x++) {
    pairs[x] = pairs[x].padEnd(2, "_")
  }
  return pairs
}

console.log(splitPairs("abcdefghijk"))

// other solution
function splitString(string) {
  let check = string.split("")
  let result = []
  if (check.length % 2 != 0) {
    for (let i = 0; i < check.length; i = i + 2) {
      if (i != check.length - 1) {
        result.push(check[i] + check[i + 1])
      } else {
        result.push(check[i] + "_")
      }
    }
    return result
  } else {
    for (let i = 0; i < check.length; i = i + 2) {
      result.push(check[i] + check[i + 1])
    }
    return result
  }
}