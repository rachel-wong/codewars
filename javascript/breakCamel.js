// Complete the solution so that the function will break up camel casing, using a space between words.

// https://stackoverflow.com/questions/7888238/javascript-split-string-on-uppercase-characters
function solution(str) {
  return str.split(/(?=[A-Z])/).join(" ")
}

console.log(solution('camelCasing')) // => should return 'camel Casing'

// other solutions
function solution(string) {
  return string.replace(/([a-z])([A-Z])/g, "$1 $2");
}

function solution(string) {
  string = string.split('').map(function (el) {
    if (el === el.toUpperCase()) {
      el = ' ' + el
    }
    return el
  })
  return string.join('')
}

function solution(string) {
  return string.replace(/[A-Z]/g, function (c) {
    return " " + c;
  });
}

function breakCamel(str) {
  str = str.split("");
  for (let i = 0; i < str.length; i++) {
    if (str[i] === str[i].toUpperCase()) {
      str[i] = " " + str[i]
    }
  }
  return str.join("").trim()
}