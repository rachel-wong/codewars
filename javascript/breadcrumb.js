// truly, trainwreck to end all trainwrecks. 

function generateBC(url, operator) {
  let check = url.split("/")
  // remove any index.html ending
  if (check[check.length - 1].includes("index")) {
    return check.slice(0, -1)
  }

  if (check[check.length - 1].includes('.')) {
    check[check.length - 1] = check[check.length - 1].slice(0, check[check.length - 1].indexOf('.'))
  }

  let linkName = check[1].toUpperCase()
  let link = linkName.toLowerCase()

  let spanName = check[check.length - 1].toUpperCase().replace("-", " ")
  if (linkName.length > 30) {
    linkName = acronym(linkName)
  }
  return `<a href="/">HOME</a>${operator}<a href="/${link}/">${linkName}</a>${operator}<span class="active">${spanName}</span>`
}

// helper
function acronym(array) {
  let skip = ["the", "of", "in", "from", "by", "with", "and", "or", "for", "to", "at", "a"].map(item => item.toUpperCase())
  // get rid of any -, change it all to single case, and filter out any excludable words
  array = array.split("-").map(item =>
    item.toUpperCase()
  ).filter(item => !skip.includes(item))

  let result = []

  // get only the first letter of each word to form abbreviation
  array.forEach((item) => {
    result.push(item.charAt(0))
  })
  return result.join("")
}


generateBC("mysite.com/pictures/holidays.html", " : ")
// '<a href="/">HOME</a> : 
// <a href="/pictures/">PICTURES</a> : 
// <span class="active">HOLIDAYS</span>'
generateBC("www.codewars.com/users/GiacomoSorbi", " / ")
// '<a href="/">HOME</a> / 
// <a href="/users/">USERS</a> / 
// <span class="active">GIACOMOSORBI</span>'
generateBC("www.microsoft.com/important/confidential/docs/index.htm#top", " * ")
// '<a href="/">HOME</a> * 
// <a href="/important/">IMPORTANT</a> * 
// <a href="/important/confidential/">CONFIDENTIAL</a> * 
// <span class="active">DOCS</span>'
generateBC("mysite.com/very-long-url-to-make-a-silly-yet-meaningful-example/example.asp", " > ")
// '<a href="/">HOME</a> > 
// <a href="/very-long-url-to-make-a-silly-yet-meaningful-example/">VLUMSYME</a> > 
// <span class="active">EXAMPLE</span>'
generateBC("www.very-long-site_name-to-make-a-silly-yet-meaningful-example.com/users/giacomo-sorbi", " + ")
// '<a href="/">HOME</a> + 
// <a href="/users/">USERS</a> + 
// <span class="active">GIACOMO SORBI</span>'

function generateBC(url, separator) {
  var ignore = ["THE", "OF", "IN", "FROM", "BY", "WITH", "AND", "OR", "FOR", "TO", "AT", "A"],
    ref = ['<a href="', '">', '</a>', '<span class="active">', '</span>'];
  var arr = url.split("/"),
    crumbs = []
  arr[arr.length - 1].includes("index") ? arr.pop() : arr
  arr[0].includes("http") ? arr.shift() : arr
  arr = arr.filter(function (item) {
    return (item.length > 0);
  })

  var ans = arr.map(function (array, index) {
    if (array.includes("?")) {
      array = array.slice(0, array.lastIndexOf("?"));
    }
    if (array.includes("#")) {
      array = array.slice(0, array.lastIndexOf("#"));
    }
    if (array.includes(".")) {
      array = array.slice(0, array.lastIndexOf("."));
    }
    var name = (index === 0) ? "HOME" : array.toUpperCase();
    if (index === 0) {
      str = "/"
    } else {
      crumbs.push(array);
      str = "/" + crumbs.join("/") + "/"
    }

    if (name.length > 30 && name.includes("-")) {
      name = name.split("-").map(function (string) {
        if (ignore.indexOf(string) < 0) return string[0]
      }).join("")
    } else if (name.length <= 30 && name.includes("-")) {
      name = name.split("-").join(" ")
    }

    if (index == arr.length - 1) {
      return (ref[3] + name + ref[4]);
    } else {
      return (ref[0] + str + ref[1] + name + ref[2]);
    }
  });

  return ans.join(separator);