function backSpace(arr) {
  if (arr === "" || arr === "#") return ""
  arr = arr.split("")
  let isAlpha = /[a-zA-Z]/
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].test(isAlpha) == true && arr[i + 1] === "#") {
      arr = arr.splice(i, 2).join("")
      backSpace(arr)
    } else {
      i++
    }
  }
}