console.log(partsSums([1, 2, 3, 4, 5, 6])) // => [21, 20, 18, 15, 11, 6, 0]
console.log(partsSums([744125, 935, 407, 454, 430, 90, 144, 6710213, 889, 810, 2579358]))

// first version caused execution time errors when order of complexity reaches O(n^2)
function parts_sums(ls) {
  let count = ls.length
  let sum = 0
  let result = []
  while (count > 0) {
    sum = ls.reduce((a, b) => {
      return a + b
    })
    result.push(sum)
    count--
    ls.shift()
  }
  result.push(0)
  return result
}

// other versions
function partsSums(ls) {
  ls.unshift(0);
  let sum = ls.reduce((p, c) => p + c, 0);
  return ls.map(v => sum = sum - v);
}

function partsSums(ls) {
  var result = [];
  result.push(ls.reduce((a, b) => a + b, 0))
  for (i = 0; i < ls.length; i++) {
    result.push((result[result.length - 1]) - ls[i])
  }
  return result
}