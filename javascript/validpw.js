// The rules for a valid password are as follows:

// There needs to be at least 1 uppercase letter.
// There needs to be at least 1 lowercase letter.
// There needs to be at least 1 number.
// The password needs to be at least 8 characters long.

// sneaky extra with \W in regex, it leaves the underscore
// https://stackoverflow.com/questions/20864893/replace-all-non-alpha-numeric-characters-new-lines-and-multiple-white-space-wi
function password(string) {
  let passed = string.replace(/[\W_]+/g, '')
  let uppercase = passed.replace(/[a-z0-9]/g, '')
  let lowercase = passed.replace(/[A-Z0-9]/g, '')
  let numeric = passed.replace(/[a-z]/ig, '')
  return uppercase.length >= 1 && lowercase.length >= 1 && numeric.length >= 1 && string.length >= 8 ? true : false
}

console.log(password("Abcd1234")) // === > true
console.log(password("Abcd123")) // === > false
console.log(password("abcd1234")) // === > false
console.log(password("AbcdefGhijKlmnopQRsTuvwxyZ1234567890")) // === > true
console.log(password("ABCD1234")) // === > false
console.log(password("Ab1!@#$%^&*()-_+={}[]|\:;?/>.<,")) // === > true;
console.log(password("!@#$%^&*()-_+={}[]|\:;?/>.<,")) // === > false;

// other solutions
function password(str) {
  return str.length >= 8 &&
    /[a-z]/.test(str) &&
    /[A-Z]/.test(str) &&
    /\d/.test(str);
}

const password = str => /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d).{8,}$/.test(str);

function password(str) {
  return str.length >= 8 && /\d/.test(str) && /[a-z]/.test(str) && /[A-Z]/.test(str)
}

const password = str => /[A-Z]/.test(str) && /[a-z]/.test(str) && /[0-9]/.test(str) && str.length > 7;