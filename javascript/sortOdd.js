// You have an array of numbers. Your task is to sort ascending odd numbers but even numbers must be on their places.
// Zero isn't an odd number and you don't need to move it. If you have an empty array, you need to return it.
// sortArray([5, 3, 2, 8, 1, 4]) == [1, 3, 2, 8, 5, 4]

function sortArray(arr) {
  // collect all odds
  let odds = []
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] % 2 != 0) {
      odds.push(arr[i])
    }
  }
  odds.sort((a, b) => a - b)

  let moveNext = 0
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] % 2) {
      arr[i] = odds[moveNext]
      moveNext++
    }
  }
  return arr
}
console.log(sortArray([5, 3, 2, 8, 1, 4]))

// other solutions
function sortArray(array) {
  const odd = array.filter((x) => x % 2).sort((a, b) => a - b);
  return array.map((x) => x % 2 ? odd.shift() : x);
}

function sortArray(array) {
  var odds = [];
  //loop, if it's odd, push to odds array
  for (var i = 0; i < array.length; ++i) {
    if (array[i] % 2 !== 0) {
      odds.push(array[i]);
    }
  }
  //sort odds from smallest to largest
  odds.sort(function (a, b) {
    return a - b;
  });

  //loop through array, replace any odd values with sorted odd values
  for (var j = 0; j < array.length; ++j) {
    if (array[j] % 2 !== 0) {
      array[j] = odds.shift();
    }
  }
  return array;
}