// Complete the method/function so that it converts dash/underscore delimited words into camel casing. The first word within the output should be capitalized only if the original word was capitalized (known as Upper Camel Case, also often referred to as Pascal case).

function toCamelCase(str) {
  str = str.split(/[ \-_]+/)
  for (let i = 1; i < str.length; i++) {
    str[i] = str[i].charAt(0).toUpperCase() + str[i].substring(1)
  }
  return str.join("")
}

function toCamelCase(str) {
  if (str.includes("-") == true) {
    return str.split("-").map((word, idx) => {
      if (idx === 0 && word.charAt(0) === word.charAt(0).toUpperCase()) {
        return word.charAt(0).toUpperCase() + word.substring(1).toLowerCase()
      } else if (idx === 0 && word.charAt(0) === word.charAt(0).toLowerCase()) {
        return word
      } else {
        return word.charAt(0).toUpperCase() + word.substring(1).toLowerCase()
      }
    }).join("")
  } else if (str.includes("_") == true) {
    return str.split("_").map((word, idx) => {
      if (idx === 0 && word.charAt(0) === word.charAt(0).toUpperCase()) {
        return word.charAt(0).toUpperCase() + word.substring(1).toLowerCase()
      } else if (idx === 0 && word.charAt(0) === word.charAt(0).toLowerCase()) {
        return word
      } else {
        return word.charAt(0).toUpperCase() + word.substring(1).toLowerCase()
      }
    }).join("")
  } else if (str === "") {
    return ""
  }
}

console.log(toCamelCase("the-stealth-warrior")) // returns "theStealthWarrior"
console.log(toCamelCase("The_Stealth_Warrior")) // returns "TheStealthWarrior"
console.log(toCamelCase("the_Stealth_Warrior")) // returns "theStealthWarrior"

// other solutions
function toCamelCase(str) {
  var regExp = /[-_]\w/ig;
  return str.replace(regExp, function (match) {
    return match.charAt(1).toUpperCase();
  });
}

function toCamelCase(str) {
  return str.replace(/[-_](.)/g, (_, c) => c.toUpperCase());
}

function toCamelCase(str) {
  return str.replace(/([-_])(\w)/g, function (match, dash, letter) {
    return letter.toUpperCase()
  });
}

function toCamelCase(str) {
  var strArray;
  if (str.indexOf('-') !== -1) { //if delineated by -
    strArray = str.split('-');
  } else {
    strArray = str.split('_'); //if delineated by _
  }
  var camelCase = strArray[0]; //keeps first word value as is
  for (var i = 1, len = strArray.length; i < len; i++) {
    var capitalized = strArray[i].substr(0, 1).toUpperCase() + strArray[i].slice(1);
    camelCase += capitalized;
  }
  return camelCase;
}