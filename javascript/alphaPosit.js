// // In this kata you are required to, given a string, replace every letter with its position in the alphabet. If anything in the text isn 't a letter, ignore it and don't return it.
// // "a" = 1, "b" = 2, etc.

function alphabetPosition(text) {
	let onlyChars = text.split("").filter(char => char.match(/^[a-zA-Z]+$/))
	let count = onlyChars.map(char => (char = parseInt(char, 36) - 9))
	return count.join(" ")
}

// second attempt
function alphaPosition(string) {
	return string.split("").filter(item =>
		/^[a-zA-Z]$/.test(item) == true).map(char => {
		return char.toLowerCase().charCodeAt() - 96
	}).join(" ")
}

// console.log(alphabetPosition("The sunset sets at twelve o' clock."))

// converting alphabets to numbers
// 'a'.charCodeAt() - 96; // 1
// 'z'.charCodeAt() - 96; // 26
// 'A'.charCodeAt() - 64; // 1
// 'Z'.charCodeAt() - 64; // 26
// parseInt('a', 36) - 9; // 1
// parseInt('z', 36) - 9; // 26
// parseInt('A', 36) - 9; // 1
// parseInt('Z', 36) - 9; // 26

// other solutions
function alphabetPosition(text) {
	return text
		.toUpperCase()
		.match(/[a-z]/gi)
		.map((c) => c.charCodeAt() - 64)
		.join(' ');
}

function alphabetPosition(text) {
	var result = "";
	for (var i = 0; i < text.length; i++) {
		var code = text.toUpperCase().charCodeAt(i)
		if (code > 64 && code < 91) result += (code - 64) + " ";
	}

	return result.slice(0, result.length - 1);
}

function alphabetPosition(text) {
	return text.match(/[a-zA-Z]/g).map((el) => el.toLowerCase().charCodeAt() - 96).join(' ');
}