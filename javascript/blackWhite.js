// Complete the function that returns the color of the given square on a normal, 8x8 chess board:

function blackWhite(col, row) {
  let columns = "abcdefgh".split("")
  // if col is odd && row is even => white
  // if col is even && row is even => black
  // if col is odd && row is odd => black
  // if col is even && row is odd  => white
  let col_ind = columns.indexOf(col.toLowerCase()) + 1
  if (col_ind % 2 != 0 && row % 2 == 0 || col_ind % 2 == 0 && row % 2 != 0) {
    return "white"
  } else if (col_ind % 2 == 0 && row % 2 == 0 || col_ind % 2 != 0 && row % 2 != 0) {
    return "black"
  }
}

console.log(blackWhite("a", 8)) // => white
console.log(blackWhite("b", 2)) // => black
console.log(blackWhite("f", 5)) // => white

// other solutions
let mineColor = (l, n) => (l.charCodeAt() - 97 + n) % 2 ? "black" : "white";

function mineColor(line, number) {
  var line = line.charCodeAt(0) - 96
  return (line + number) % 2 ? 'white' : 'black'
}

function mineColor(line, number) {
  return ['white', 'black'][('abcdefgh'.indexOf(line) + number) % 2];
}