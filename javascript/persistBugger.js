// Write a function, persistence, that takes in a positive parameter num and returns its multiplicative persistence, which is the number of times you must multiply the digits in num until you reach a single digit.

// took far longer than it did
function persistence(int) {
  let count = 0
  let result = 0
  let arr = int.toString().split("").map(Number)
  while (arr.length > 1) {
    result = arr.reduce((a, b) => {
      return a * b
    })
    count++
    arr = result.toString().split("").map(Number)
  }
  return count
}
console.log(persistence(39)) //=> 3

// other solutions
function persistence(num) {
  var times = 0;

  num = num.toString();

  while (num.length > 1) {
    times++;
    num = num.split('').map(Number).reduce((a, b) => a * b).toString();
  }

  return times;
}