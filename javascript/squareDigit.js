// For example, if we run 9119 through the function, 811181 will come out, because 92 is 81 and 12 is 1.

function squareit(int) {
  return Number(int.toString().split("").map(item => {
    return item = Math.pow(parseInt(item), 2)
  }).join(""))
}
console.log(squareit(9119)) // => 811181