// In this kata your task is to create bit calculator. Function arguments are two bit representation of numbers ("101","1","10"...), and you must return their sum in decimal representation.

// Test.expect(calculate("10","10") == 4);
// Test.expect(calculate("10", "0") == 2);
// Test.expect(calculate("101", "10") == 7);

// parseInt and some Math functions are disabled.
// Those Math functions are enabled: pow, round, random


function calculate(num1, num2) {
  return decConvert(num1) + decConvert(num2)
}

function decConvert(num) {
  num = num.split("").reverse().map(Number)
  console.log('num:', num)
  for (let i = 0; i < num.length; i++) {
    if (num[i] === 1) {
      num[i] = Math.pow(2, i)
    } else {
      num[i] === 0
    }
  }
  return num.reduce((a, b) => a + b)
}