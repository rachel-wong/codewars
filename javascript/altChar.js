// change the string so that the characters alternate and there are no matching adjacent characters.
// The function will return the number of matching adjacent character deletions it will take

function altChar(string) {
  string = string.split("")
  let toDelete = 0
  console.log('string:', string)
  string.forEach((item, idx) => {
    if (item == string[idx + 1]) {
      toDelete++
    }
  })
  return toDelete
}

console.log(altChar("ABAABBAB")) // => 2

