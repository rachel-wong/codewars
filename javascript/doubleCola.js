// Sheldon, Leonard, Penny, Rajesh and Howard are in the queue for a "Double Cola" drink vending machine; there are no other people in the queue. 
// The first one in the queue (Sheldon) buys a can, drinks it and doubles! The resulting two Sheldons go to the end of the queue. Then the next in the queue (Leonard) buys a can, drinks it and gets to the end of the queue as two Leonards, and so on.
// For example, Penny drinks the third can of cola and the queue will look like this:
// Rajesh, Howard, Sheldon, Sheldon, Leonard, Leonard, Penny, Penny
// Write a program that will return the name of the person who will drink the n-th cola.
// Input: The input data consist of an array which contains at least 1 name, and single integer n which may go as high as the biggest number your language of choice supports (if there's such limit, of course).
// Output: Return the single line — the name of the person who drinks the n-th can of cola. The cans are numbered starting from 1.

// this helped https://math.stackexchange.com/questions/1827641/codewars-double-cola-brainteaser
function whoIsNext(people, r) {
  let queue = people.length
  let check = 1 //cans numbering
  while (r > queue) {
    queue = queue * 2
    r = queue - r
    check = check * 2
  }
  return names[Math.floor((r - 1) / check) % queue]
}

console.log(whoIsNext(["Sheldon", "Leonard", "Penny", "Rajesh", "Howard"], 1)) // == "Sheldon"
console.log(whoIsNext(["Sheldon", "Leonard", "Penny", "Rajesh", "Howard"], 52)) // == "Penny"
console.log(whoIsNext(["Sheldon", "Leonard", "Penny", "Rajesh", "Howard"], 7230702951)) // == "Leonard"

// unsuccessful - no idea
function earlyWhoIsNext(people, r) {
  let count = people.length
  let queue = people.length
  let check = 0
  let result = 0

  while (true) {
    if (queue > r) {
      result = (r - check) / (count / people.length)
    }
  }
  return people[result]
}


// other solutions
function whoIsNext(names, r) {
  var l = names.length;
  while (r >= l) {
    r -= l;
    l *= 2;
  }
  return names[Math.ceil(names.length * r / l) - 1];
}

function whoIsNext(names, r) {

  var numOfGeeks = names.length;
  var loga = Math.log((r / numOfGeeks) + 1) / Math.log(2);
  var completeCycles = Math.floor(loga)
  var fullCycleColas = (Math.pow(2, completeCycles) - 1) * numOfGeeks;
  var currCycleSize = Math.pow(2, completeCycles) * numOfGeeks;
  var geekCode = Math.ceil((r - fullCycleColas) / currCycleSize * numOfGeeks);

  return names[geekCode - 1]
}

function whoIsNext(names, n) {
  x = names.length;
  i = 1;

  while (n > x) {
    n -= x;
    x *= 2;
    i *= 2;
  }

  return (names[parseInt((n - 1) / i)]);
}

function whoIsNext(names, n) {
  var chunk = names.length;
  while (n > chunk) {
    n -= chunk;
    chunk *= 2;
  }
  return names[Math.ceil(n / (chunk / names.length)) - 1];
}

function whoIsNext(names, r) {
  var l = names.length,
    ini = l;
  while (r > ini) {
    r -= ini;
    ini *= 2;
  }
  return names[Math.ceil(r * l / ini) - 1];
}