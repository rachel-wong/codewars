// the way of the dumbass ╭∩╮（︶︿︶）╭∩╮
function battle(goodStr, evilStr) {
  goodStr = goodStr.split(" ").map(Number)
  evilStr = evilStr.split(" ").map(Number)

  // why on gods green earth do i have to do it this way when i have a computer
  let goodSum = goodStr[0] * 1 + goodStr[1] * 2 + goodStr[2] * 3 + goodStr[3] * 3 + goodStr[4] * 4 + goodStr[5] * 10
  let evilSum = evilStr[0] * 1 + evilStr[1] * 2 + evilStr[2] * 2 + evilStr[3] * 2 + evilStr[4] * 3 + evilStr[5] * 5 + evilStr[6] * 10

  if (goodSum > evilSum) {
    return "Battle Result: Good triumphs over Evil"
  } else if (goodSum < evilSum) {
    return "Battle Result: Evil eradicates all trace of Good"
  } else {
    return "Battle Result: No victor on this battle field"
  }
}

console.log(battle('0 0 1 1 1 0', '0 0 0 0 0 0 1')) //=> draw
console.log(battle('0 0 0 0 0 10', '0 1 1 1 1 0 0')) //=> good
console.log(battle('1 0 0 0 0 0', '1 0 0 0 0 0 0')) //=> draw

// version 1 - not sure why it failed 9 tests
function battle(goodStr, evilStr) {
  console.log("goodStr", goodStr)
  console.log("evilStr", evilStr)

  let goodCount = [1, 2, 3, 3, 4, 10]
  let evilCount = [1, 2, 2, 2, 3, 5, 10]
  goodStr = goodStr.split(" ").map(Number)
  evilStr = evilStr.split(" ").map(Number)

  if (getSum(goodStr, goodCount) > getSum(evilStr, evilCount)) {
    return "Battle Result: Good triumphs over Evil"
  } else if (getSum(goodStr, goodCount) < getSum(evilStr, evilCount)) {
    return "Battle Result: Evil eradicates all trace of Good"
  } else if (getSum(goodStr, goodCount) == getSum(evilStr, evilCount)) {
    return "Battle Result: No victor on this battle field"
  }
}

// helper
function getSum(array, count) {
  for (let i = 0; i < array.length; i++) {
    array[i] *= count[i]
  }
  return array.reduce((a, b) => {
    return a + b
  })
}

// other solutions
function goodVsEvil(good, evil) {
  var getWorth = function (side, worth) {
    return side.split(' ').reduce(function (result, value, index) {
      return result + (worth[index] * value);
    }, 0);
  }

  var result = getWorth(good, [1, 2, 3, 3, 4, 10]) - getWorth(evil, [1, 2, 2, 2, 3, 5, 10]);

  return result > 0 ? "Battle Result: Good triumphs over Evil" :
    result < 0 ? "Battle Result: Evil eradicates all trace of Good" :
    "Battle Result: No victor on this battle field";
}

// i knew you can fit a .reduce in there somewhere just couldn't make it sing
function goodVsEvil(good, evil) {
  var worth = [
    [1, 2, 3, 3, 4, 10],
    [1, 2, 2, 2, 3, 5, 10]
  ];
  var good = good.split(' ').reduce((s, v, i) => s + worth[0][i] * v, 0);
  var evil = evil.split(' ').reduce((s, v, i) => s + worth[1][i] * v, 0);
  if (good > evil) return "Battle Result: Good triumphs over Evil";
  else if (evil > good) return "Battle Result: Evil eradicates all trace of Good";
  else return "Battle Result: No victor on this battle field";
}