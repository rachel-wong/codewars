// In this simple Kata your task is to create a function that turns a string into a Mexican Wave. You will be passed a string and you must return that string in an array where an uppercase letter is a person standing up.
// 1.  The input string will always be lower case but maybe empty.
// 2. If the character in the string is whitespace then pass over it as if it was an empty seat.

function wave(string) {
  let result = []
  for (i = 0; i < string.length; i++) {
    // move this inside the for loop to use a fresh lowercase array everytime
    let check = string.toLowerCase().split("")
    // if it is character, change it. Move over any spaces in the string
    if (check[i] != " ") {
      check[i] = check[i].toUpperCase()
      result.push(check.join(""))
    }
  }
  return result
}

console.log(wave("hello"))
console.log(wave("gap"))
console.log(wave("Two words"))

// other solutions
function wave(str) {
  let result = [];

  str.split("").forEach((char, index) => {
    if (/[a-z]/.test(char)) {
      result.push(str.slice(0, index) + char.toUpperCase() + str.slice(index + 1));
    }
  });

  return result;
}

const wave = str => [...str].map((s, i) =>
  str.slice(0, i) + s.toUpperCase() + str.slice(i + 1)
).filter(x => x != str);