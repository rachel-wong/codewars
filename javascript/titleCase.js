  // First argument (required): the original string to be converted.
  // Second argument (optional): space-delimited list of minor words that must always be lowercase except for the first word in the string. The JavaScript/CoffeeScript tests will pass undefined when this argument is unused.

  function titleCase(string, exclude) {
    if (exclude === undefined) {
      return string.toLowerCase().split(" ").map(word => word.charAt(0).toUpperCase() + word.substring(1)).join(" ")
    } else {
      exclude = exclude.toLowerCase().split(" ").map(word => word.charAt(0).toUpperCase() + word.substring(1))
      string = string.toLowerCase().split(" ").map(word => word.charAt(0).toUpperCase() + word.substring(1))
      for (let i = 1; i < string.length; i++) {
        if (exclude.includes(string[i]) == true) {
          string[i] = string[i].charAt(0).toLowerCase() + string[i].substring(1)
        }
      }
      return string.join(" ")
    }
  }

  titleCase('a clash of KINGS', 'a an the of') // should return: 'A Clash of Kings'
  titleCase('THE WIND IN THE WILLOWS', 'The In') // should return: 'The Wind in the Willows'
  titleCase('the quick brown fox') // should return: 'The Quick Brown Fox'

  // other solutions
  function titleCase(title, minorWords) {
    var minorWords = typeof minorWords !== "undefined" ? minorWords.toLowerCase().split(' ') : [];
    return title.toLowerCase().split(' ').map(function (v, i) {
      if (v != "" && ((minorWords.indexOf(v) === -1) || i == 0)) {
        v = v.split('');
        v[0] = v[0].toUpperCase();
        v = v.join('');
      }
      return v;
    }).join(' ');
  }

  String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
  }

  function titleCase(title, minorWords) {
    var titleAr = title.toLowerCase().split(' '),
      minorWordsAr = minorWords ? minorWords.toLowerCase().split(' ') : [];

    return titleAr.map(function (e, i) {
        return minorWordsAr.indexOf(e) === -1 || i === 0 ? e.capitalize() : e
      })
      .join(' ');
  }

  function titleCase(title, minorWords) {
    minorWords = (minorWords || "").toLowerCase().split(' ');
    title = title.toLowerCase();
    return title.replace(/(\w)\w*/g, (word, firstChar, index) => {
      if (index === 0 || minorWords.indexOf(word) === -1)
        word = word.replace(firstChar, firstChar.toUpperCase());
      return word;
    });
  }