// Write a method that takes an array of consecutive (increasing) letters as input and that returns the missing letter in the array.

// You will always get an valid array.And it will be always exactly one letter be missing.The length of the array will always be at least 2.
// The array will always contain letters in only one case.

// have you ever seen anything as ugly as this? no? yeh me neither
function missingLetter(array) {
  let alphabet = [..."abcdefghijklmnopqrstuvwxyz"]
  if (array === null) return false //codewars error handling
  let check = array.map(char => {
    return char = char.toLowerCase()
  })
  let temp = alphabet.indexOf(check[0])
  alphabet = alphabet.slice(temp, temp + check.length)
  let missing = alphabet.filter(char => !check.includes(char))
  if (array[0] === array[0].toUpperCase()) {
    return missing[0].toUpperCase()
  } else {
    return missing[0]
  }
}

console.log(missingLetter(['a', 'b', 'c', 'd', 'f'])) // => 'e'
console.log(missingLetter(['O', 'Q', 'R', 'S'])) // => 'P'
console.log(missingLetter(['D', 'E', 'G', 'H'])) // => 'F'

// other solutions (yeh charCode i knew i'd find u here)
function findMissingLetter(array) {
  let first = array[0].charCodeAt(0)
  for (let i = 1; i < array.length; i++) {
    if (first + i !== array[i].charCodeAt(0)) {
      return String.fromCharCode(first + i)
    }
  }
  throw new Error("Invalid input")
}

function findMissingLetter(array) {
  var i = array[0].charCodeAt();
  array.map(x => x.charCodeAt() == i ? i++ : i);
  return String.fromCharCode(i);
}

function findMissingLetter(letters) {
  for (var i = 0; i < letters.length; i++) {
    if (letters[i].charCodeAt() + 1 !== letters[i + 1].charCodeAt()) {
      return String.fromCharCode(letters[i].charCodeAt() + 1);
    }
  }
}

const findMissingLetter = (array) => {
  const alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
  const start = alphabet.indexOf(array[0]);
  return alphabet.slice(start, start + array.length).find(el => !array.includes(el));
};

function findMissingLetter(array) {
  for (i = 1; i < array.length; ++i)
    if (array[i].charCodeAt() - 1 != array[i - 1].charCodeAt())
      return String.fromCharCode(array[i].charCodeAt() - 1);
}