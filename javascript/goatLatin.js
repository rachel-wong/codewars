// A sentence S is given, composed of words separated by spaces. Each word consists of lowercase and uppercase letters only.

// We would like to convert the sentence to "Goat Latin" (a made-up language similar to Pig Latin.)

// The rules of Goat Latin are as follows:

// If a word begins with a vowel (a, e, i, o, or u), append "ma" to the end of the word.
// For example, the word 'apple' becomes 'applema'.

// If a word begins with a consonant (i.e. not a vowel), remove the first letter and append it to the end, then add "ma".
// For example, the word "goat" becomes "oatgma".

// Add one letter 'a' to the end of each word per its word index in the sentence, starting with 1.
// For example, the first word gets "a" added to the end, the second word gets "aa" added to the end and so on.
// Return the final sentence representing the conversion from S to Goat Latin. 

var toGoatLatin = function (string) {
  return string.split(" ").map((word, index) => {
    let isVowel = /[aeiou]/gi
    if (isVowel.test(word.charAt(0)) == true) {
      word = word.concat("ma") // assign, not return
    } else if (isVowel.test(word.charAt(0)) == false) {
      word = word.slice(1) + word.charAt(0) + "ma" // assign, not return
    }
    return word + "a".repeat(index + 1) // return only goes here because it's the final transform
  }).join(" ")
}

console.log(toGoatLatin("I speak Goat Latin")) //=> "Imaa peaksmaaa oatGmaaaa atinLmaaaaa"