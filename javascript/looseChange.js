// Welcome young Jedi! In this Kata you must create a function that takes an amount of US currency in cents, and returns a dictionary/hash which shows the least amount of coins used to make up that amount. The only coin denominations considered in this exercise are: Pennies (1¢), Nickels (5¢), Dimes (10¢) and Quarters (25¢). Therefor the dictionary returned should contain exactly 4 key/value pairs.

// If the function is passed either 0 or a negative number, the function should return the dictionary with all values equal to 0.
// If a float is passed into the function, its value should be be rounded down, and the resulting dictionary should never contain fractions of a coin.

// Examples
// loose_change(56)    ==>  {'Nickels': 1, 'Pennies': 1, 'Dimes': 0, 'Quarters': 2}
// loose_change(-435)  ==>  {'Nickels': 0, 'Pennies': 0, 'Dimes': 0, 'Quarters': 0}
// loose_change(4.935) ==>  {'Nickels':

// Pennies (1¢)
// Nickels (5¢)
// Dimes (10¢)
// Quarters (25¢)
// using the math.floor method to round down to the nearest integar

// tried %, didnt work
function loose_change(cents) {

  let wallet = {
    "Nickels": 0,
    "Pennies": 0,
    "Dimes": 0,
    "Quarters": 0
  }

  // these can be consts
  let quarter_val = 25
  let dime_val = 10
  let nickel_val = 5
  let pennie_val = 1

  if (cents < 0 || typeof cents != "number") {
    return wallet //error handling for negative, zero & silliness
  } else {
    // roundedCents carry remainder over to the next calc
    let roundedCents = Math.floor(cents) // in case of decimal inputs

    let quarters = Math.floor(roundedCents / quarter_val)

    roundedCents = roundedCents - (quarters * quarter_val)
    let dimes = Math.floor(roundedCents / dime_val)

    roundedCents = roundedCents - (dimes * dime_val)
    let nickels = Math.floor(roundedCents / nickel_val)

    roundedCents = roundedCents - (nickels * nickel_val)
    let pennies = Math.floor(roundedCents / pennie_val)

    wallet["Nickels"] = nickels
    wallet["Pennies"] = pennies
    wallet["Dimes"] = dimes
    wallet["Quarters"] = quarters

    return wallet
  }
}

console.log(loose_change(56)) //==>  {'Nickels': 1, 'Pennies': 1, 'Dimes': 0, 'Quarters': 2}
console.log(loose_change(-435)) //==>  {'Nickels': 0, 'Pennies': 0, 'Dimes': 0, 'Quarters': 0}
console.log(loose_change(4.935)) //==>  {'Nickels': 0, 'Pennies': 4, 'Dimes': 0, 'Quarters': 0}

// other solutions
function looseChange(cents) {
  var a = {
    Nickels: 0,
    Pennies: 0,
    Dimes: 0,
    Quarters: 0
  };
  if (cents <= 0) return a;
  a.Quarters = Math.floor(cents / 25);
  a.Dimes = Math.floor(cents % 25 / 10);
  a.Nickels = Math.floor(cents % 25 % 10 / 5);
  a.Pennies = Math.floor(cents % 25 % 10 % 5);
  return a;
}

const looseChange = (cents) => (
  cents = Math.max(cents, 0), {
    Quarters: cents / 25 | 0,
    Dimes: cents % 25 / 10 | 0,
    Nickels: cents % 25 % 10 / 5 | 0,
    Pennies: cents % 5 | 0
  })