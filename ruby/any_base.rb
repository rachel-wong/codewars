def any_base(decimal, base)
    result = []
    until decimal < base
        result << decimal % base
        decimal = decimal / base
    end
    result << decimal
    return result.reverse.map(&:to_s).join.to_i
end

# p any_base(25, 2)
# p any_base(30, 2)
# p any_base(115, 4)
# p any_base(71, 3)
# p any_base(100,3)

def to_decimal(input, base)
    input.to_s.split("").map(&:to_i)
    result += input ** (0..base)
end

to_decimal(49, 16)