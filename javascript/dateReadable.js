// write a function which formats a duration, given as a durationber of seconds, in a human - friendly way.

// The function must accept a non - negative integer.If it is zero, it just returns "now".Otherwise, the duration is expressed as a combination of years, days, hours, minutes and seconds.

// It is much easier to understand with an example:
//   formatDuration(62) // returns "1 minute and 2 seconds"
// formatDuration(3662) // returns "1 hour, 1 minute and 2 seconds"
// For the purpose of this Kata, a year is 365 days and a day is 24 hours.

// Note that spaces are important.


// needs refactoring
function readableDuration(duration) {
  let seconds = duration
  let minutes = 0
  let hours = 0
  let days = 0
  let years = 0

  // calculate the time using remainders
  if (seconds === 0) {
    return "now"
  } else if (seconds === 1) {
    return "1 second"
  } else if (seconds < 60) {
    return seconds + " seconds"
  }

  if (seconds >= 60) {
    minutes = (duration - duration % 60) / 60
    seconds = seconds - (minutes * 60)
  }

  if (minutes >= 60) {
    hours = (minutes - minutes % 60) / 60
    minutes = minutes - (hours * 60)
  }

  if (hours >= 24) {
    days = (hours - hours % 24) / 24
    hours = hours - (days * 24)
  }

  if (days > 365) {
    years = (days - days % 365) / 365
    days = days - (years * 365)
  }

  return printDuration(years, days, hours, minutes, seconds)
}

function printDuration(years, days, hours, minutes, seconds) {

  // format the calculated output
  let result = []
  let unit = ["years", "days", "hours", "minutes", "seconds"]
  let duration_val = [years, days, hours, minutes, seconds]

  for (let i in duration_val) {
    if (duration_val[i] !== 0) {
      if (duration_val[i] === 1) {
        result.push(duration_val[i] + " " + unit[i].slice(0, -1) + ", ")
      } else {
        result.push(duration_val[i] + " " + unit[i] + ", ")
      }
    }
  }
  if (result.length === 1) {
    return result[0].slice(0, -2)
  }

  let lastUnit = " and " + result[result.length - 1].slice(0, -2)
  let secondToLastUnit = result[result.length - 2].slice(0, -2)
  return result.splice(length - 2, 2, secondToLastUnit, lastUnit).join("")
}