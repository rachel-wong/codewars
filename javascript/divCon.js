function divCon(arr) {
  let nums = arr.filter(item => typeof item === 'number').reduce((a, b) => { return a + b}, 0)
  let str = arr.filter(item => typeof item === 'string').map(item => parseInt(item)).reduce((a, b) => { return a + b}, 0)
  return nums - str
}

console.log(divCon(['5', '0', 9, 3, 2, 1, '9', 6, 7])) // => 2

// other solutions
function divCon(x){
  return x.reduce((acc, cur) => typeof cur === 'number'? acc + cur : acc - Number(cur),0)
}