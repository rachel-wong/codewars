// A Narcissistic Number is a number which is the sum of its own digits, each raised to the power of the number of digits in a given base. In this Kata, we will restrict ourselves to decimal (base 10).

// Your code must return true or false depending upon whether the given number is a Narcissistic number in base 10.

// Error checking for text strings or other invalid inputs is not required, only valid integers will be passed into the function.

function is_narcissistic(num) {
  let check = num.toString().split("").map(Number)
  let result = check.reduce((acc, currentVal) => {
    return acc + Math.pow(currentVal, check.length)
  }, 0)
  return result == num ? true : false
}

console.log(is_narcissistic(153))
console.log(is_narcissistic(371))

// other solutions

function narcissistic(value) {
  return ('' + value).split('').reduce(function (p, c) {
    return p + Math.pow(c, ('' + value).length)
  }, 0) == value;
}

narcissistic = num => num.toString().split("").reduce(function (prev, el) {
  return prev + Math.pow(el, String(num).length)
}, 0) == num;