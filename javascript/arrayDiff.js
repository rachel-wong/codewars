// Your goal in this kata is to implement a difference
// function, which subtracts one list from another and returns the result.

// It should remove all values from list a, which are present in list b.

// array_diff([1, 2], [1]) == [2]
// If a value is present in b, all of its occurrences must be removed from the other:

//   array_diff([1, 2, 2, 2, 3], [2]) == [1, 3]

// second attempt
function array_diff(arr1, arr2) {
  return arr1.filter(x => !arr2.includes(x))
}

// first attempt
function array_diff(a, b) {
  // for every item in list a
  // for every item in list b
  // if item a = item in list b
  // then remove all items in list b
  a.map(String)
  b.map(String)
  let difference = a.filter(item => !b.includes(item))
  return difference
}
array_diff([1, 2, 2, 2, 3], [2])
array_diff([1, 2], [1])

// other solutions 
// function array_diff(a, b) {
//   return a.filter(e => !b.includes(e));
// }

// let a = [1, 2, 2, 2, 3]
// let b = [2]

// // Difference
// const difference2 = (a, b) => (a.filter(item => !b.includes(item)))
// console.log(difference2)

// // Intersection
// const intersection = (a, b) => (a.filter(item => b.includes(item)))
// console.log('intersection:', intersection)

// // Symmetrical Difference
// const symmetrical_difference = (a, b) => (a.filter(x => !b.includes(item).concat(b.filter(item => !a.includes(item)))))
// console.log('symmetrical_difference:', symmetrical_difference)

// // Union
// const union = (a, b) => (Array.from(a, b))
// console.log('union:', union)