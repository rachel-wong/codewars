function shortestStepsToNum(num) {
  if (num >= 1 && num <= 10000) {
    let step = 0

    // pass the num through, increment the step
    // if even,  halved everytime
    // else decrement it by one
    while (num > 1) {
      num % 2 == 0 ? num /= 2 : num--
      step++
    }
    return step
  } else {
    // if it falls outside the original num limits, return 0 
    return 0
  }
}

console.log(shortestStepsToNum(71))
console.log(shortestStepsToNum(16))
console.log(shortestStepsToNum(1))
console.log(shortestStepsToNum(12))

// recursion option
function shortestStepsToNum(num) {
  if (num <= 1) return 0;
  if (num % 2 == 1) return 1 + shortestStepsToNum(num - 1);
  return 1 + shortestStepsToNum(num / 2);
}