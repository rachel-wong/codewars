function shortestWord(str) {
  return str.split(" ").sort((a, b) => { return a.length - b.length}).shift().length
}

console.log(shortestWord("bitcoin take over the world maybe who knows perhaps"))