// Pirates have notorious difficulty with enunciating. They tend to blur all the letters together and scream at people.
// At long last, we need a way to unscramble what these pirates are saying.
// Write a function that will accept a jumble of letters as well as a dictionary, and output a list of words that the pirate might have meant.
// Return matches in the same order as in the dictionary. Return an empty array if there are no matches.

function grabscrab(target, dictionary) {
  let result = []
  target = getSorted(target)
  let check = dictionary.map(item => getSorted(item))
  for (let i = 0; i < check.length; i++) {
    if (check[i].includes(target)) {
      result.push(dictionary[i])
    }
  }
  return result
}

// helper
function getSorted(string) {
  return string.split("").sort().join("")
}

console.log(grabscrab("ortsp", ["sport", "parrot", "ports", "matey"])) // => ["sport", "ports"]

// other solutions
function grabscrab(anagram, dictionary) {
  anagram = anagram.split('').sort().join('');
  return dictionary.filter(a => a.split('').sort().join('') === anagram)
}

function grabscrab(s, a) {
  return a.filter(x => [...x].sort().join() == [...s].sort().join())
}

function grabscrab(anagram, dictionary) {
  let array = [];
  for (let i = 0; i < dictionary.length; i++) {
    if (anagram.split('').sort().join('') == dictionary[i].split('').sort().join('')) {
      array.push(dictionary[i])
    };
  }
  return array;
}

const grabscrab = (anagram, dictionary) => {
  anagram = sort(anagram);
  return dictionary.filter(e => sort(e) === anagram);
}

const sort = s => s.split ``.sort((x, y) => x.localeCompare(y)).join ``;

// should consider the prototype methods next time
String.prototype.sort = function () {
  return this
    .split('')
    .sort((a, b) => a.localeCompare(b))
    .join('');
}

function grabscrab(anagram, dictionary) {
  var s = anagram.sort();
  return dictionary.filter(word => word.length == s.length && word.sort() == s);
}