function reverse(str) {
  str = str.trim().split(" ")
  for (let i = 0; i < str.length; i++) {
    if (i % 2 != 0) {
      str[i] = str[i].split("").reverse().join("")
    }
  }
  return str.join(" ")
}

console.log(reverse("Reverse this string, please!")) //"Reverse siht string, !esaelp"

// other solutions are
function reverse(string) {
  return string
    .split` `
    .map((w, i) => i & 1 ? [...w].reverse().join`` : w)
    .join` `
    .trim();
}