// Given a string, return a new string that has transformed based on the input:

// Change case of every character, ie. lower case to upper case, upper case to lower case.
// Reverse the order of words from the input.
// Note: You will have to handle multiple spaces, and leading/trailing spaces.


// Brute force stupidity. 
function transform(string) {
  string = string.split("").map(char => switchCase(char)) // switch the case before putting it back together
  return string.join("").split(" ").reverse().join(" ") // ur ugly and silly
}

// halp halp
function switchCase(char) {
  return char === char.toUpperCase() ? char.toLowerCase() : char.toUpperCase()
}

console.log(transform("Example Input")) // == > "iNPUT eXAMPLE"

// other solutions

// ur so pretty mwah <3
function stringTransformer(str) {
  return str
    .split(' ')
    .reverse()
    .join(' ')
    .split('')
    .map(v => v == v.toUpperCase() ?
      v.toLowerCase() :
      v.toUpperCase())
    .join('');
}

// const stringTransformer = s => s.split(' ').reverse().join(' ').split('').map(invertCase).join('');
// const invertCase = c => /[a-z]/.test(c) ? c.toUpperCase() : c.toLowerCase();