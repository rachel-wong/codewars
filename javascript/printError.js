
function printerError(str) {
  let errorTest = /^[n-zN-Z]+$/
  let errors = str.split("").filter(item => errorTest.test(item))
  return errors.length + "/" + str.length
}

console.log(printerError("aaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz")) // 3/56

// other solutions are

function printerError(s) {
    return s.match(/[^a-m]/g).length + "/" + s.length;
}

function printerError(s) {
    var count = 0;
    for(var i = 0; i < s.length; i++) {
      if (s[i] > "m") {
        count++;
      }
    }
    return count+"/"+s.length;
}