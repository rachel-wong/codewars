// Complete the solution so that it strips all text that follows any of a set of comment markers passed in. Any whitespace at the end of the line should also be stripped out.

// Given an input string of:
// apples, pears # and bananas
// grapes
// bananas!apples

// The output expected would be:
// apples, pears
// grapes
// bananas

// The code would be called like so:
// var result = solution("apples, pears # and bananas\ngrapes\nbananas !apples", ["#", "!"])
// result should == "apples, pears\ngrapes\nbananas"

// WAT --- & I cant' stress this enough --- ON EARTH???!!!

function checkComments(input, markers) {
  // \n (line drop) is the signpost-separator. Not punctuation
  let check = input.split('\n')

  for (let i = 0; i < markers.length; i++) {
    for (let j = 0; j < check.length; j++) {

      // this part is hard
      // for every marker, get its position in the input
      let index = check[j].indexOf(markers[i])
      // extracts a new string from start to the first instance of the marker in the input  
      if (index >= 0) {
        check[j] = check[j].substring(0, index).trim()
      }
    }
  }
  // add everything back together by \n
  console.log(check.join('\n'))
}

checkComments("apples, plums % and bananas\npears\noranges !applesauce", ["%", "!"]) // => "apples, plums\npears\noranges"
checkComments("Q @b\nu\ne -e f g", ["@", "-"]) // => "Q\nu\ne"

// other solutions
function solution(input, markers) {
  return input.split('\n').map(
    line => markers.reduce(
      (line, marker) => line.split(marker)[0].trim(), line
    )
  ).join('\n')
}

// ye olde regex to put you all to shame, TO SHAME
function solution(input, markers) {
  return input.replace(new RegExp("\\s?[" + markers.join("") + "].*(\\n)?", "gi"), "$1");
}