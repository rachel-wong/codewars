// Write a function that when given a URL as a string, parses out just the domain name and returns it as a string.

// The substring() method returns the part of the string BETWEEN the start and end indexes, or to the end of the string.
// substring(include, exclude)

function domainName(str) {
  // START after -> www., http://, https://
  // END before -> .com

  if (str.includes("www.")) {
    str = str.split("www.").pop()
  } else if (str.includes("http://")) {
    str = str.split("http://").pop()
  } else if (str.includes("https://")) {
    str = str.split("https://").pop()
  }
  return str.split(".").shift()
}

console.log(domainName("http://github.com/carbonfive/raygun")) // == "github"
console.log(domainName("http://www.zombie-bites.com")) // == "zombie-bites"
console.log(domainName("https://www.cnet.com")) // == "cnet"
console.log(domainName("www.xakep.ru")) // == "xakep"
console.log(domainName("https://youtube.com")) // => "youtube"

// other solutions
function domainName(url) {
  url = url.replace("https://", '');
  url = url.replace("http://", '');
  url = url.replace("www.", '');
  return url.split('.')[0];
};

// ye olde regex
function domainName(url) {
  return url.match(/(?:http(?:s)?:\/\/)?(?:w{3}\.)?([^\.]+)/i)[1];
}

function domainName(url) {
  return url.replace(/(https?:\/\/)?(www\.)?/, '').split('.')[0]
}

function domainName(url) {
  return url.replace('http://', '')
    .replace('https://', '')
    .replace('www.', '')
    .split('.')[0];
}