function getOrder(input) {
  let menu = "Burger, Fries, Chicken, Pizza, Sandwich, Onionrings, Milkshake, Coke".split(",").map(item => item.trim())
  return input.toLowerCase().match(/chicken|fries|burger|pizza|sandwich|onionrings|milkshake|coke/gi).map(item => {
    return item.charAt(0).toUpperCase() + item.slice(1) // capitalise first char 
  }).sort((a, b) => {
    return menu.indexOf(a) - menu.indexOf(b) // sort input by the order of items in menu
  }).join(" ") // output needs to be string not array
}

console.log(getOrder("milkshakepizzachickenfriescokeburgerpizzasandwichmilkshakepizza")) //=> "Burger Fries Chicken Pizza Pizza Pizza Sandwich Milkshake Milkshake Coke"
console.log(getOrder("pizzachickenfriesburgercokemilkshakefriessandwich")) //=> "Burger Fries Fries Chicken Pizza Sandwich Milkshake Coke"

// other solutions
const MENU = {
  Burger: 1,
  Fries: 2,
  Chicken: 3,
  Pizza: 4,
  Sandwich: 5,
  Onionrings: 6,
  Milkshake: 7,
  Coke: 8,
};
const REG_CMD = new RegExp(Object.keys(MENU).join('|'), 'gi');

function getOrder(cmd) {
  return cmd.match(REG_CMD)
    .map(s => s.charAt(0).toUpperCase() + s.slice(1))
    .sort((x, y) => MENU[x] - MENU[y])
    .join(' ');
}

const getOrder = input => {
  const menu = ['Burger', 'Fries', 'Chicken', 'Pizza', 'Sandwich', 'Onionrings', 'Milkshake', 'Coke'];
  let orders = input.match(new RegExp(menu.join('|'), 'gi'))
    .map(item => item[0].toUpperCase() + item.slice(1))
    .sort((a, b) => menu.indexOf(a) - menu.indexOf(b));
  return orders.join(' ');
};

function getOrder(input) {
  let menu = ['Burger', 'Fries', 'Chicken', 'Pizza', 'Sandwich', 'Onionrings', 'Milkshake', 'Coke'];
  let result = '';

  menu.forEach(item => {
    result += (item + ' ').repeat((input.match(new RegExp(item, 'gi')) || []).length);
  });
  return result.trim();
}