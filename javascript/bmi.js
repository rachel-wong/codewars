// function high(x) {
//   let alphabets = [..."abcdefghijklmnopqrstuvwxyz"]
//   let words = x.split(" ")
//   let scores = words.map(word =>
//     word.split("").reduce((acc, b) => {
//       return acc + (alphabets.indexOf(b) + 1)
//     }), 0)
//   console.log('scores:', scores)
// }
// console.log(high('man i need a taxi up to ubud')) //=> taxi

function bmi(weight, height) {
  let bmi = (weight / Math.pow(height, 2)).toFixed(1)
  if (bmi > 0 && bmi <= 18.5) {
    return "Underweight"
  } else if (bmi > 18.5 && bmi <= 25.0) {
    return "Normal"
  } else if (bmi > 25.0 && bmi <= 30.0) {
    return "Overweight"
  } else if (bmi > 30) {
    return "Obese"
  }
}

console.log(bmi(80, 1.80)) //=> "Normal"

// other solutions

const bmi = (w, h, bmi = w / h / h) => bmi <= 18.5 ? "Underweight" :
  bmi <= 25 ? "Normal" :
  bmi <= 30 ? "Overweight" : "Obese";


function bmi(weight, height) {
  let bmi = weight / (height * height);
  switch (true) {
    case bmi <= 18.5:
      return "Underweight";
    case bmi <= 25.0:
      return "Normal";
    case bmi <= 30.0:
      return "Overweight";
    case bmi > 30:
      return "Obese";
  }
}

function bmi(weight, height) {

  var bmi = weight / (height * height);

  return bmi < 18.5 ? "Underweight" : bmi <= 25 ? "Normal" : bmi <= 30 ? "Overweight" : "Obese";

}