//Complete the solution so that it splits the string into pairs of two characters. If the string contains an odd number of characters then it should replace the missing second character of the final pair with an underscore ('_').

// second attempt
function solution(string) {
  if (string.length % 2 === 0) {
    return string.match(/.{1,2}/g)
  } else {
    return string.match(/.{1,2}/g).map(item => {
      return item.padEnd(2, "_")
    })
  }
}

// first attempt
function solution(str) {
  let pairs = []
  for (let i = 0; i < str.length; i += 2) {
    pairs.push(str.substring(i, i + 2))
  }

  for (let x = 0; x < pairs.length; x++) {
    pairs[x] = pairs[x].padEnd(2, "_")
  }
  return pairs
}

console.log(solution('abc')) // should return ['ab', 'c_']
console.log(solution('abcdef')) // should return ['ab', 'cd', 'ef']

// other solutions
function solution(str) {
  return (str.length % 2 ? str + '_' : str).match(/../g);
}

function solution(str) {
  return (str + "_").match(/../g);
}

function solution(str) {
  var i = 0;
  var result = new Array();
  if (str.length % 2 !== 0) {
    str = str + '_';
  }
  while (i < str.length) {
    result.push(str[i] + str[i + 1]);
    i += 2;
  }
  return result;
}