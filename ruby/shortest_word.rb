def find_short(s)
    list = s.split(" ")
    length = []
    for word in list do
        length << word.chars.length
    end
    p length.min
end

find_short("apple duck boy cat minion") #=> 3

def find_short(s)
    s.split.map(&:size).min
  end

  def find_short(s)
    l = s.split.min_by(&:length).size
    return l
end