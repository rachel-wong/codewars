// The vowel substrings in the word codewarriors are o,e,a,io. The longest of these has a length of 2. Given a lowercase string that has alphabetic characters only and no spaces, return the length of the longest vowel substring. Vowels are any of aeiou.

function vowelChain(string) {
  let collect = []
  let tempVowel = ""
  let vowels = 'aeiou'.split("")
  string.split("").forEach(char => {
    if (vowels.includes(char.toLowerCase())) {
      tempVowel += char
    } else {
      collect.push(tempVowel)
    }
  })
  collect = collect.filter(item => item != "")
  return
}

console.log(vowelChain("codewarriors")) // +> 2 for io