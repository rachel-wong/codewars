// Find maximum length of subarray created from the array "a" whose sum is equalled to "k"
function maxLength(a, k) {
  let arrs = []
  let len = 1
  while (len <= a.length) {
    for (let i = 0; i < a.length; i++) {
      arrs.push(a.slice(i, len))
    }
    len++
  }
  arrs = arrs.filter(arr => arr.length > 0)
  let totals = []
  arrs.forEach((item, idx) => {
    let total = item.reduce((a, b) => a + b)
    if (total === k) {
      totals.push(idx)
    }
  })
  let length = []
  totals.forEach(item => length.push(arrs[item].length))
  return Math.max(...length)
}

console.log(maxLength([1, 2, 3], 3))
console.log(maxLength([3, 1, 2, 1], 4))