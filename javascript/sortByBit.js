// // In this kata you're expected to sort an array of 32-bit integers in ascending order of the number of on bits they have.

// E.g Given the array [7, 6, 15, 8]

// 7 has 3 on bits (000...0111)
// 6 has 2 on bits (000...0011)
// 15 has 4 on bits (000...1111)
// 8 has 1 on bit (000...1000)
// So the array in sorted order would be [8, 6, 7, 15].

// In cases where two numbers have the same number of bits, compare their real values instead.

// E.g between 10 (...1010) and 12 (...1100), they both have the same number of on bits '2' but the integer 10 is less than 12 so it comes first in sorted order.

// Your task is to write the function sortBybit() that takes an array of integers and sort them as described above.

// Note: Your function should modify the input rather than creating a new array.

function sortByBit(array) {
  return array = array.sort((a, b) => {
    return getBits(a) - getBits(b) || a - b // don't understand from pipes onwards
  })
}

// helper
function getBits(num) {
  return num.toString(2).split("").map(Number).filter(num => num != 0).length
}

// unsuccessful
function firstSortByBit(array) {
  let bitCount = []
  array.forEach(num =>
    bitCount.push(num.toString(2))
  )
  for (let i = 0; i < bitCount.length; i++) {
    bitCount[i] = bitCount[i].split("").map(Number).filter(num => num != 0).length
  }
  array.sort((a, b) => {
    return bitCount.indexOf(a) - bitCount.indexOf(b)
  })
  return array
}

console.log(sortByBit([3, 8, 3, 6, 5, 7, 9, 1])) // => [1, 8, 3, 3, 5, 6, 9, 7]

// other solutions
const sortByBit = arr => arr.sort((a, b) => a.toString(2).replace(/0/g, '') - b.toString(2).replace(/0/g, '') || a - b)

function sortByBit(arr) {

  function bits(num) {
    return Array.from((num).toString(2), (a) => Number(a)).reduce((a, b) => a + b);
  };

  function cmp(a, b) {
    var result = bits(a) - bits(b);
    if (result != 0) {
      return result;
    } else {
      return a - b;
    };
  };
  arr.sort(cmp);
}

function countBits(a) {
  let count = 0;
  a = a.toString(2);
  for (var i = 0; i < a.length; ++i) {
    if (a[i] == 1) count++
  }
  return count;
}

let compare = (a, b) => {
  if (countBits(a) < countBits(b)) return -1;
  if (countBits(b) < countBits(a)) return 1;
  if (countBits(a) == countBits(b)) return a - b;
}

function sortByBit(arr) {
  return arr.sort(compare);
}