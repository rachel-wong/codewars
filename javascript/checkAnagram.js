// Write a function that will find all the anagrams of a word from a list. You will be given two inputs a word and an array with words. You should return an array of all the anagrams or an empty array if there are none. For example:

// 'abba' & 'baab' == true
// 'abba' & 'bbaa' == true
//   'abba' & 'abbba' == false
// 'abba' & 'abca' == false

function anagrams(target, array) {
  let check_target = sort(target)
  let result = []
  let sorted_array = []
  array.forEach(item => {
    sorted_array.push(sort(item))
  })
  for (let i = 0; i < sorted_array.length; i++) {
    if (sorted_array[i] == check_target) {
      result.push(array[i])
    }
  }
  return result
}

// helper
function sort(string) {
  return string.split("").sort().join("")
}

console.log(anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada'])) //=> ['aabb', 'bbaa']
console.log(anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer'])) //=> ['carer', 'racer']
console.log(anagrams('laser', ['lazing', 'lazy', 'lacer'])) //=> []

// other solutions
function anagrams(word, words) {
  word = word.split('').sort().join('');
  return words.filter(function (v) {
    return word == v.split('').sort().join('');
  });
}

let anagrams = (word, words) => words.filter(w => w.split('').sort().join('') === word.split('').sort().join(''));

function anagrams(word, words) {
  return words.filter(function (e) {
    return e.split('').sort().join('') === word.split('').sort().join('');
  })
}