// Define a function that takes an integer argument and returns logical value true or false depending on if the integer is a prime.

// Per Wikipedia, a prime number(or a prime) is a natural number greater than 1 that has no positive divisors other than 1 and itself.

// Checks if the number is 0, 1, or negative.Returning true if the number is within this range.
// Checks if the number is 2 or 3. Returning true if the number is 2 or 3.
// returns that the number is not prime if it is divisible by 2 or 3.
// check  every odd number that is not divisible by 3 until it becomes larger than the square root of the num.
// For example, the first i and i + 2 will be 5 and 7. The next i and i + 2 will be 11 and 13. It does not check 9 or 15 because both are divisible by 3 and would have been caught in the last else if statement.
// returns true if the number was not divisible by any i or i + 2 entries because the number is prime.


function is_prime(num) {
  if (num <= 1) {
    return true
  } else if (num <= 3) {
    return true
  } else if (num % 2 === 0 || num % 3 === 0) {
    return false
  }
  let i = 5
  while (i * i <= num) {
    if (num % i === 0 || num % (i + 2) === 0) {
      return false
    }
    i += 6
  }
  return true
}

function is_prime(int) {
  if (int <= 1) return false
  if (int % 2 === 1 || int % 1 === 0) {
    return true
  } else {
    return false
  }
}

console.log(is_prime(1)) /* false */
console.log(is_prime(2)) /* true  */
console.log(is_prime(3)) /* true  */
console.log(is_prime(-1)) /* false */