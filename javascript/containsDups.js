// Given an array of integers, find if the array contains any duplicates.

// Your function should return true if any value appears at least twice in the array, and it should return false if every element is distinct.

var containsDuplicate = function (nums) {
  return nums.length != [...new Set(nums)].length ? true : false
}

console.log(containsDuplicate([1, 2, 3, 1])) //=> true
console.log(containsDuplicate([1, 1, 1, 3, 3, 4, 3, 2, 4, 2])) //=> true
console.log(containsDuplicate([1, 2, 3, 4])) //=> false

// other solutions
var containsDuplicate = function (nums) {
  return new Set(nums).size < nums.length;
};

