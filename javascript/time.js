function time(s) {
  s = s.split(":")
  let meridiem = s[2].slice(-2)
  let seconds = s[2].substring(0, s[2].length - 2)
  let minutes = s[1]
  let hours = s[0]

  if (meridiem === "PM" && parseInt(hours) < 12) {
    hours = parseInt(hours) + 12
  } else if (meridiem == "PM") {
    hours = hours
  } else if (meridiem === "AM" && parseInt(hours) >= 12) {
    hours = parseInt(hours) - 12
  }
  if (hours === 0) {
    hours = "00"
  }
  return `${hours}:${minutes}:${seconds}`
}

console.log(time("12:45:54PM"))
