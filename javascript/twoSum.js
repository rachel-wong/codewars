// Write a function that takes an array of numbers (integers for the tests) and a target number. It should find two different items in the array that, when added together, give the target value. The indices of these items should then be returned in an array like so: [index1, index2].

function twoSum(array, target) {
  for (let i = 0; i < array.length; i++) {
    // this second for-loop will always look ahead of first for-loop
    // the two loops will never look at the same item at the same iteration
    for (let next = 1; next < array.length; next++) {
      if (array[i] + array[next] === target) {
        let result = [i, next]
        return result
      }
    }
  }
}

console.log(twoSum([1, 2, 3], 4)) //=> [0,2]
console.log(twoSum([1234, 5678, 9012], 14690)) //=> [1,2]
console.log(twoSum([2, 2, 3], 4)) // => [0,1]

// other solutions
function twoSum(numbers, target) {
  let seen = new Map();
  for (let i = 0; i < numbers.length; i++) {
    let x = numbers[i],
      y = target - x;
    if (seen.has(y))
      return [seen.get(y), i];
    seen.set(x, i);
  }
}

function twoSum(nums, target) {
  const arr = [];
  nums.map(function (x, ind1) {
    nums.map(function (y, ind2) {
      if (x + y == target && ind1 != ind2)
        arr.push(ind1, ind2)
    });
  });
  return [arr[0], arr[1]]
}