def swap(st)
    input_array = st.chars
    new_array = []
    vowels = ["a", "e", "i", "o", "u"]
    for letter in input_array do
        if vowels.include?(letter) == true
            new_array << letter.upcase
        else
            new_array << letter
        end
    end
    p new_array.join
end

capitalise("Hello World") #=> "HEllO WOrld!"

def swap(s)
    s.tr("aeiou", "AEIOU")
  end