// Write a function that takes a single string (word) as argument. The function must return an ordered list containing the indexes of all capital letters in the string.

function capitals(word) {
  word = word.split("")
  let capitals = []
  for (let i = 0; i < word.length; i++) {
    if (word[i] === word[i].toUpperCase()) {
      capitals.push(i)
    }
  }
  return capitals
}

Test.assertSimilar(capitals('CodEWaRs'), [0, 3, 4, 6]);

// other solutions
var capitals = function (word) {
  return word.split('').reduce(function (memo, v, i) {
    return v === v.toUpperCase() ? memo.concat(i) : memo;
  }, []);
};