// Given an array, find the int that appears an odd number of times.
// There will always be only one integer that appears an odd number of times.

function findOdd(array) {
  let count = {}
  for (i = 0; i < array.length; i++) {
    count[array[i]] = (count[array[i]] || 0) + 1
  }
  return parseInt(Object.keys(count).find(key => count[key] % 2 != 0))
}
console.log(findOdd([20, 1, -1, 2, -2, 3, 3, 5, 5, 1, 2, 4, 20, 4, -1, -2, 5])) // => 5

// other solutions
const findOdd = (xs) => xs.reduce((a, b) => a ^ b);

function findOdd(A) {
  var obj = {};
  A.forEach(function (el) {
    obj[el] ? obj[el]++ : obj[el] = 1;
  });

  for (prop in obj) {
    if (obj[prop] % 2 !== 0) return Number(prop);
  }
}

function findOdd(A) {
  return A.reduce(function (c, v) {
    return c ^ v;
  }, 0);
}

function findOdd(arr) {
  return arr.find((item, index) => arr.filter(el => el == item).length % 2)
}

const findOdd = A => A.filter(x => A.filter(v => x === v).length % 2 === 1).reduce(a => a);