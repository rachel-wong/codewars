// Given a positive integer of up to 16 digits, return true if it is a valid credit card number, and false if it is not.
// Double every other digit, scanning from right to left, starting from the second digit (from the right).

// Another way to think about it is: if there are an even number of digits, double every other digit starting with the first; if there are an odd number of digits, double every other digit starting with the second:

// If a resulting number is greater than 9, replace it with the sum of its own digits (which is the same as subtracting 9 from it):

// Sum all of the final digits:

// Finally, take that sum and divide it by 10. If the remainder equals zero, the original credit card number is valid.

function validate(int) {
  if (int.toString().length % 2 === 0) {
    int = int.toString().split("").map(Number).map((int, idx) => {
      return idx % 2 === 0 ? int * 2 : int
    })
  } else {
    int = int.toString().split("").map(Number).map((int, idx) => {
      return idx % 2 != 0 ? int * 2 : int
    })
  }
  return int.map(item => {
    return item > 9 ? item.toString().split("").map(Number).reduce((a, b) => a + b) : item
  }).reduce((a, b) => a + b) % 10 === 0 ? true : false
}

console.log(validate(123)) //=> false
console.log(validate(1)) //=> false
console.log(validate(2121)) //=> true
console.log(validate(1230)) //=> true
console.log(validate(91)) //=> true
console.log(validate(92)) //=> false
console.log(validate(912030)) //=> true
console.log(validate(922030)) //=> false

// other solutions
function validate(n) {
  var sum = 0;
  while (n > 0) {
    var a = n % 10;
    n = Math.floor(n / 10);
    var b = (n % 10) * 2;
    n = Math.floor(n / 10);
    if (b > 9) {
      b -= 9;
    }
    sum += a + b;
  }
  return sum % 10 == 0;
}

function validate(n) {
  n = n.toString().split('').map(Number).reverse();
  return n.reduce(function (sum, digit, index) {
    if (index & 1) digit <<= 1;
    if (digit > 9) digit -= 9;
    return sum + digit;
  }, 0) % 10 == 0;
}