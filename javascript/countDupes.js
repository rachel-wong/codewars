// Write a function that will return the count of distinct case-insensitive alphabetic characters and numeric digits that occur more than once in the input string. The input string can be assumed to contain only alphabets (both uppercase and lowercase) and numeric digits.

// "abcde" -> 0 # no characters repeats more than once
// "aabbcde" -> 2 # 'a' and 'b'
// "aabBcde" -> 2 # 'a' occurs twice and 'b' twice (`b` and `B`)
// "indivisibility" -> 1 # 'i' occurs six times
// "Indivisibilities" -> 2 # 'i' occurs seven times and 's' occurs twice
// "aA11" -> 2 # 'a' and '1'
// "ABBA" -> 2 # 'A' and 'B' each occur twice

function duplicateCount(text) {
  let check = text.split("").map(char => char.toLowerCase())
  let count = {}
  check.forEach(item => {
    count[item] = (count[item] || 0) + 1
  })
  let found = Object.keys(count).map(key => {
    if (count[key] > 1) {
      return key
    }
  })
  return found.filter(item => item != undefined).length
}

console.log(duplicateCount("aA11"))

// other solutions
function duplicateCount(text) {
  return (text.toLowerCase().split('').sort().join('').match(/([^])\1+/g) || []).length;
}

function duplicateCount(text) {
  return text.toLowerCase().split('').filter(function (val, i, arr) {
    return arr.indexOf(val) !== i && arr.lastIndexOf(val) === i;
  }).length;
}

function duplicateCount(text) {
  var lower = text.toLowerCase();
  var count = 0;
  var used = [];

  lower.split('').forEach(function (letter) {
    if (!used.includes(letter) && (lower.split(letter).length - 1) > 1) {
      count++;
      used.push(letter);
    }
  });

  return count;
}

function duplicateCount(text) {
  return text
    .toLowerCase()
    .split('')
    .reduce(function (a, l) {
      a[l] = a[l] ? a[l] + 1 : 1;
      if (a[l] === 2) a.count++;
      return a;
    }, {
      count: 0
    }).count;
}

function duplicateCount(text) {
  var dup = [];
  text.toLowerCase().split('').forEach(function (v, i, arr) {
    if (i != arr.lastIndexOf(v) && dup.indexOf(v) == -1) dup.push(v);
  });
  return dup.length;
}