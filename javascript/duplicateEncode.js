// The goal of this exercise is to convert a string to a new string where each character in the new string is "(" if that character appears only once in the original string, or ")" if that character appears more than once in the original string. Ignore capitalization when determining if a character is a duplicate.

// "din" => "((("
// "recede" => "()()()"
// "Success" => ")())())"
// "(( @" => "))(("

function duplicateEncoder(string) {
  let check = string.toLowerCase().split("") // convert all to the same case to avoid counting silliness
  let count = {}
  // populate the counting hash
  check.forEach(char => {
    count[char] = (count[char] || 0) + 1
  })

  // refactored
  check = check.map(char => {
    return count[char] === 1 ? "(" : ")"
  }).join("")

  // for (let i = 0; i < check.length; i++) {
  //   if (count[check[i]] === 1) {
  //     result.push("(")
  //   } else {
  //     result.push(")")
  //   }
  // }
  // return result.join("")
  return check
}

console.log(duplicateEncoder("din"))
console.log(duplicateEncoder("recede"))
console.log(duplicateEncoder("Success"))
console.log(duplicateEncoder("(( @"))

// other solutions
function duplicateEncode(word) {
  return word
    .toLowerCase()
    .split('')
    .map(function (a, i, w) {
      return w.indexOf(a) == w.lastIndexOf(a) ? '(' : ')'
    })
    .join('');
}

function duplicateEncode(word) {
  var letters = word.toLowerCase().split('')
  return letters.map(function (c, i) {
    return letters.some(function (x, j) {
      return x === c && i !== j
    }) ? ')' : '('
  }).join('')
}