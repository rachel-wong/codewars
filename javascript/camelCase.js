// Write simple .camelCase method (camel_case function in PHP, CamelCase in C# or camelCase in Java) for strings. All words must have their first letter capitalized without spaces.

function camelCase2(string) {
  let check = string.split(" ")
  check = check.map(word => {
    return word.charAt(0).toUpperCase().concat(word.substr(1))
  })
  return check.join("")
}
// .substr(startIndex, howmanycharacters)
// .substring(startIndex, endIndex)

// failed tests on codewars but generally works
function camelCase(string) {
  let check = string.split(" ")
  let result = []
  check.forEach(word => {
    let chars = word.split("")
    chars[0] = chars[0].toUpperCase()
    result.push(chars.join(""))
  })
  return result.join(" ")
}

console.log(camelCase2("hello case"))

// other solutions

String.prototype.camelCase = function () {
  return this.split(' ').map(function (word) {
    return word.charAt(0).toUpperCase() + word.slice(1);
  }).join('');
}

String.prototype.camelCase = function () {
  return this.split(' ').map(w => w.slice(0, 1).toUpperCase() + w.slice(1)).join('');
}