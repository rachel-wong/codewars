// Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.
// You may assume that the array is non-empty and the majority element always exist in the array.

var majorityElement = function (nums) {
  let count = {}
  for (item of nums) {
    count[item] = (count[item] || 0) + 1
  }
  return Object.keys(count).filter(key => {
    return count[key] == Math.max.apply(null, Object.values(count))
  })
}

console.log(majorityElement([3, 2, 3])) //=> 3
console.log(majorityElement([2, 2, 1, 1, 1, 2, 2])) //=> 2