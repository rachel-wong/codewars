// In this Kata, you will implement the Luhn Algorithm, which is used to help validate credit card numbers.
// Given a positive integer of up to 16 digits, return true if it is a valid credit card number, and false if it is not.

// 1. Double every other digit, scanning from right to left, starting from the second digit (from the right).
// Another way to think about it is: if there are an even number of digits, double every other digit starting with the first; if there are an odd number of digits, double every other digit starting with the second

// 2. If a resulting number is greater than 9, replace it with the sum of its own digits (which is the same as subtracting 9 from it)

// 3. Sum all of the final digits

// 4. Finally, take that sum and divide it by 10. If the remainder equals zero, the original credit card number is valid.

// Hideous af
function luhn(array) {
  let check = array.toString().split("").map(Number)
  // if length of digits is ODD, double every other digit starting with SECOND
  if (check.length % 2 === 1) {
    for (let i = 1; i < check.length; i++) {
      check[i] = check[i] * 2
      if (check[i] > 9) {
        check[i] = check[i] - 9
      }
      i = i + 1
    }
  }
  // if length of digits is EVEN, double every other digit starting with FIRST
  else {
    for (let i = 0; i < check.length; i++) {
      check[i] = check[i] * 2
      if (check[i] > 9) {
        check[i] = check[i] - 9
      }
      i = i + 1
    }
  }

  // if the sum of all numbers % 10 neatly, return TRUE, otherwise FALSE it
  return check.reduce((a, b) => {
    return a + b
  }, 0) % 10 === 0 ? true : false
}

console.log(luhn(891)) // => false
console.log(luhn(1)) // => false
console.log(luhn(2121)) // => true
console.log(luhn(1230)) // => true

// other solutions
function validate(n) {
  var sum = 0;

  while (n > 0) {
    var a = n % 10;
    n = Math.floor(n / 10);

    var b = (n % 10) * 2;
    n = Math.floor(n / 10);

    if (b > 9) {
      b -= 9;
    }

    sum += a + b;
  }

  return sum % 10 == 0;
}

function validate(n) {
  n = n.toString().split('').map(Number).reverse();
  return n.reduce(function (sum, digit, index) {
    if (index & 1) digit <<= 1;
    if (digit > 9) digit -= 9;
    return sum + digit;
  }, 0) % 10 == 0;
}