// In DNA strings, symbols "A" and "T" are complements of each other, as "C" and "G". You have function with one side of the DNA (string, except for Haskell); you need to get the other complementary side. DNA strand is never empty or there is no DNA at all (again, except for Haskell).

function DNAStrand(dna) {
  let check = dna.split("")
  let result = []
  for (let i = 0; i < check.length; i++) {
    switch (check[i]) {
      case "A":
        result.push("T")
        break
      case "T":
        result.push("A")
        break
      case "G":
        result.push("C")
        break
      case "C":
        result.push("G")
        break
      default:
        result.push(check[i])
        break
    }
  }
  return result.join("")
}
console.log(DNAStrand("ATTGC")) // return "TAACG"
console.log(DNAStrand("GTAT")) // return "CATA" 

// other solutions
function DNAStrand(dna) {
  return dna.replace(/./g, function (c) {
    return DNAStrand.pairs[c]
  })
}

DNAStrand.pairs = {
  A: 'T',
  T: 'A',
  C: 'G',
  G: 'C',
}

var pairs = {
  'A': 'T',
  'T': 'A',
  'C': 'G',
  'G': 'C'
};

function DNAStrand(dna) {
  return dna.split('').map(function (v) {
    return pairs[v]
  }).join('');
}

function DNAStrand(dna) {
  //your code here
  var result = "";
  for (var i = 0; i < dna.length; i++) {
    if (dna[i] === "A") {
      result += "T";
    } else if (dna[i] === "T") {
      result += "A";
    } else if (dna[i] === "C") {
      result += "G";
    } else if (dna[i] === "G") {
      result += "C";
    } else {
      result += dna[i];
    }
  }
  return result;
}