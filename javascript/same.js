// Given two arrays a and b write a function comp(a, b) (compSame(a, b) in Clojure) that checks whether the two arrays have the "same" elements, with the same multiplicities. "Same" means, here, that the elements in b are the elements in a squared, regardless of the order.
let a = [121, 144, 19, 161, 19, 144, 19, 11]
let b = [121, 14641, 20736, 361, 25921, 361, 20736, 361]

function comp(a, b) {
  if (a == null || b == null || a == [] || b == []) {
    return false
  }
  a = a.sort((a, b) => {
    return a - b
  })
  for (i = 0; i < b.length; i++) {
    b[i] = Math.sqrt(b[i])
  }
  b.sort((a, b) => {
    return a - b
  })
  if (a.join() === b.join()) {
    return true
  } else {
    return false
  }
}

console.log(comp(a, b))

// other solutions
// function comp(array1, array2) {
//   if (array1 == null || array2 == null) return false;
//   array1.sort((a, b) => a - b);
//   array2.sort((a, b) => a - b);
//   return array1.map(v => v * v).every((v, i) => v == array2[i]);
// }