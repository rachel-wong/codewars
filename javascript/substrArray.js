// Given two arrays of strings a1 and a2 return a sorted array r in ***lexicographical*** order of the strings of a1 which are substrings of strings of a2.
// Don't mutate the inputs.

function inArray(array1, array2) {
  let result = []
  array1.forEach(item => {
    if (array2.join("").includes(item) == true) {
      result.push(item)
    }
  })
  return result.sort((a, b) => {
    return a.localeCompare(b) // lexicographical ordering
  })
}

console.log(inArray(["xyz", "live", "strong"], ["lively", "alive", "harp", "sharp", "armstrong"])) // => ["live", "strong"]
console.log(inArray(["live", "strong", "arp"], ["lively", "alive", "harp", "sharp", "armstrong"])) // => ["arp", "live", "strong"]

// other solutions
function inArray(arr1, arr2) {
  return arr1.filter(function (needle) {
    return arr2.some(function (haystack) {
      return haystack.indexOf(needle) > -1;
    });
  }).sort();
}

// be still my beating heart <3
function inArray(array1, array2) {
  return array1
    .filter(a1 => array2.find(a2 => a2.match(a1)))
    .sort()
}