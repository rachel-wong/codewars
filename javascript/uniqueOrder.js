// uniqueInOrder('AAAABBBCCDAABBB') == ['A', 'B', 'C', 'D', 'A', 'B']
// uniqueInOrder('ABBCcAD') == ['A', 'B', 'C', 'c', 'A', 'D']
// uniqueInOrder([1, 2, 2, 3, 3]) == [1, 2, 3]


var uniqueInOrder = function(iterable){
  let order = []
  if (typeof iterable === 'string') {
    iterable = iterable.split("")
  }
  iterable.forEach((item, idx, arr) => {
    if (arr[idx] != arr[idx - 1]) {
      order.push(item)
    }
  })
  return order
}

function uniqueInOrder(input) {
  let arr = []
  let check = []
  if (typeof input === String) {
    arr = input.split("")
  } else {
    arr = input
  }
  for (let i = 0; i < arr.length; i++) {
    //if the first item is the same as second item, ignore and move onto the next
    if (arr[i] === arr[i + 1] && check.includes(arr[i]) == false) {} else if (arr[i] != arr[i + 1]) {
      // if the first item is DIFFERENT to second item, add first item to CHECK
      check.push(arr[i])
    }
  }
  return check
}
console.log(uniqueInOrder('AAAABBBCCDAABBB'))
console.log(uniqueInOrder([1, 2, 2, 3, 3]))

// other solutions
function uniqueInOrder(it) {
  var result = []
  var last
  for (var i = 0; i < it.length; i++) {
    if (it[i] !== last) {
      result.push(last = it[i])
    }
  }
  return result
}

var uniqueInOrder = function (iterable) {
  return [].filter.call(iterable, (function (a, i) {
    return iterable[i - 1] !== a
  }));
}

// wrong interpretation of the question - not trying to find the ONLY unique, but the difference
// function uniqueInOrder(input) {
//   let arr = []
//   let check = []
//   if (typeof input === String) {
//     arr = input.split("")
//   } else {
//     arr = input
//   }
//   for (let i = 0; i < arr.length; i++) {
//     // if item IS IN check, ignore and move onto next item
//     if (check.includes(arr[i])) {
//       i++
//     } else {
//       // if item is not in check, add to check
//       check.push(arr[i])
//     }
//   }
//   return check
// }