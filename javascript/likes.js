// Implement a function likes :: [String] -> String, which must take in input array, containing the names of people who like an item. It must return the display text as shown in the examples:

function likes(names){
  if (Array.isArray(names) == true && names.every(item => typeof item === "string") == true) {
    if (names.length === 0) return "no one likes this"
    if (names.length === 1) return `${names[0]} likes this`
    if (names.length === 2) return `${names[0]} and ${names[1]} like this`
    if (names.length === 3) return `${names[0]}, ${names[1]} and ${names[2]} like this`
    if (names.length > 3) return `${names[0]}, ${names[1]} and ${names.length - 2} others like this`
  } else {
    return false
  }
}

console.log(likes ([])) // must be "no one likes this"
console.log(likes (["Peter"])) // must be "Peter likes this"
console.log(likes (["Jacob", "Alex"])) // must be "Jacob and Alex like this"
console.log(likes (["Max", "John", "Mark"])) // must be "Max, John and Mark like this"
console.log(likes (["Alex", "Jacob", "Mark", "Max"])) // must be "Alex, Jacob and 2 others like this"

// other solutions
function likes(names) {
  names = names || [];
  switch(names.length){
    case 0: return 'no one likes this'; break;
    case 1: return names[0] + ' likes this'; break;
    case 2: return names[0] + ' and ' + names[1] + ' like this'; break;
    case 3: return names[0] + ', ' + names[1] + ' and ' + names[2] + ' like this'; break;
    default: return names[0] + ', ' + names[1] + ' and ' + (names.length - 2) + ' others like this';
  }
}

function likes (names) {
  var templates = [
    'no one likes this',
    '{name} likes this',
    '{name} and {name} like this',
    '{name}, {name} and {name} like this',
    '{name}, {name} and {n} others like this'
  ];
  var idx = Math.min(names.length, 4);

  return templates[idx].replace(/{name}|{n}/g, function (val) {
    return val === '{name}' ? names.shift() : names.length;
  });
}

// so nice <3
function likes(names) {
  return {
    0: 'no one likes this',
    1: `${names[0]} likes this`,
    2: `${names[0]} and ${names[1]} like this`,
    3: `${names[0]}, ${names[1]} and ${names[2]} like this`,
    4: `${names[0]}, ${names[1]} and ${names.length - 2} others like this`,
  }[Math.min(4, names.length)]
}

function likes(names) {
  names.length === 0 && (names = ["no one"]);
  let [a, b, c, ...others] = names;
  switch (names.length) {
    case 1: return `${a} likes this`;
    case 2: return `${a} and ${b} like this`;
    case 3: return `${a}, ${b} and ${c} like this`;
    default: return `${a}, ${b} and ${others.length + 1} others like this`;
  }
}