function maskify(string) {
  if (string.length > 4) {
    string = Array.from(string.split(""))
    let keep = string.slice(-4)
    let hashed = string.length - 4
    return Array(hashed).fill("#").concat(keep).join("")
  } else {
    return string
  }
}

console.log(maskify("4556364607935616"))