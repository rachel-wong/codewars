// Your job is to write a function which increments a string, to create a new string.
// If the string already ends with a number, the number should be incremented by 1.
// If the string does not end with a number.the number 1 should be appended to the new string.
// Attention: If the number has leading zeros the amount of digits should be considered.

// second attempt
function incrementString(strng) {
  strng = strng.match(/[a-zA-Z]+|[0-9]+/g) // split string & numbers
  if (strng[1].match(/^\d+$/)) {
    console.log("has number")
    // if leading zero
    if (string[1].match(/^[1-9]\d*$/)) {
      strng[1] = strng[1] + 1
      console.log("leading zero")
      return strng.join("")
    } else {
      return false
    }
    // do stuffs
  } else {
    return strng.push("1").join("")
  }
}

console.log(incrementString("foobar23")) // -> foobar24
console.log(incrementString("foobar0023")) // -> foobar24

// first attempts
function firstIncrementString(string) {
  let check = string.match(/[^\d]+|\d+/g) // split string from the number part 

  if (check === null) return "1"

  // if there are no nums at all, add a 1
  if (check.length === 1) check.push(1)

  // if there are leading zeros in the num
  else if (check[1].charAt(0) === "0") {
    let len = check[1].length
    let sum = (parseInt(check[1]) + 1).toString()
    check[1] = sum.padStart(len, '0')
  }
  // otherwise increment the num part
  else {
    check[1] = parseInt(check[1]) + 1
  }
  return check.join("")
}

console.log(incrementString("")) // => 1

console.log(incrementString("foo")) // => foo1
console.log(incrementString("foo23")) // => foo24
console.log(incrementString("foo001")) //=> foo002
console.log(incrementString("1")) // => 2
console.log(incrementString("009")) // = > 010

// other solutions

function incrementString(input) {
  if (isNaN(parseInt(input[input.length - 1]))) return input + '1';
  return input.replace(/(0*)([0-9]+$)/, function (match, p1, p2) {
    var up = parseInt(p2) + 1;
    return up.toString().length > p2.length ? p1.slice(0, -1) + up : p1 + up;
  });
}

function incrementString(strng) {
  var str = strng.replace(/[0-9]/gi, '');
  var num = strng.replace(/[^0-9]/gi, '');
  num++;
  var t = str + num;
  if ((strng.length - t.length) == 2)
    str += '00';
  if ((strng.length - t.length) == 1)
    str += '0';
  return str + num;
}

function incrementString(input) {
  return input.replace(/([0-8]?)(9*)$/, function (s, d, ns) {
    return +d + 1 + ns.replace(/9/g, '0');
  });
}