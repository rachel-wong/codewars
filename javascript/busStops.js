//You are provided with a list (or array) of integer arrays (or tuples). Each integer array has two items which represent number of people get into bus (The first item) and number of people get off the bus (The second item) in a bus stop.

//Your task is to return number of people who are still in the bus after the last bus station(after the last array).Even though it is the last bus stop, the bus is not empty and some people are still in the bus, and they are probably sleeping there: D

var number = function (busStops) {
	// for the length of the busStops array
	// let finalPassengers = firstPassengers + busStops[0][0] - busStops[n+1][1] + busStops[n+1][0]
	let netOnBus = []
	for (i = 0; i < busStops.length; i++) {
		netOnBus.push(busStops[i][0] - busStops[i][1])
	}
	let finalPassengers = netOnBus.reduce((a, b) => {
		return a + b
	})
	return finalPassengers
}

number([
	[10, 0],
	[3, 5],
	[5, 8]
]) //=> 5
number([
	[3, 0],
	[9, 1],
	[4, 10],
	[12, 2],
	[6, 1],
	[7, 10]
])
number([
	[3, 0],
	[9, 1],
	[4, 8],
	[12, 2],
	[6, 1],
	[7, 8]
])

// other solutions
const number = busStops => busStops.reduce((rem, [on, off]) => rem + on - off, 0)

var number = function (busStops) {
	var totalPeople = 0
	for (var i = 0; i < busStops.length; i++) {
		totalPeople += busStops[i][0]
		totalPeople -= busStops[i][1]
	}
	return totalPeople
}