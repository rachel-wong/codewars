// function findUniq(arr) {
//   let check = arr.map(item => {
//     return item.toUpperCase().split("").sort().filter((x, i, a) => a.indexOf(x) === i)[0]
//   })
//   console.log(check)
//   for (let i = 0; i < check.length; i++) {
//     if (check.lastIndexOf(check[i]) === check.indexOf(check[i])) {
//       return arr[i]
//     }
//   }
// }

// console.log(findUniq([ 'Tom Marvolo Riddle', 'I am Lord Voldemort', 'Harry Potter' ]))

// var uniqueInOrder=function(iterable){
//   let order = []
//   if (typeof iterable === 'string') {
//     iterable = iterable.split("")
//   }
//   iterable.forEach((item, idx, arr) => {
//     if (arr[idx] != arr[idx - 1]) {
//       order.push(item)
//     }
//   })
//   return order
// }

// var uniqueInOrder=function(iterable){
//     return [...iterable].filter((a, i) => a !== iterable[i-1])
// }


// console.log(uniqueInOrder('AAAABBBCCDAABBB'))
// console.log(uniqueInOrder([1, 2, 2, 3, 3]))
// console.log(uniqueInOrder('ABBCcAD') )

function likes(arr) {
  let result = []
  if (arr.length === 0) {
    result.push("no one likes this")
  } else if (arr.length === 1) {
    result.push(arr[0])
    result.push("likes this")
    result = result.join(" ")
  } else if (arr.length === 2) {
    arr = arr.splice((arr.length - 1), 0, "and")
    arr.forEach(item => { result.push(item) })
    result.push("likes this")
    result = result.join(" ")
  } else if (arr.length === 3) {
    for (let i = 0; i < arr.length - 2; i++) {
      arr[i] = arr[i] + ","
    }
    return arr.splice((arr.length - 1), 0, "and").join(" ") + " likes this"
  } else {
    for (let i = 0; i < arr.length - 2; i++) {
      arr[i] = arr[i] + ","
    }
    return arr.splice((arr.length - 1), 0, "and").join(" ") + (arr.length - 2) + " others likes this"
  }
  return result
}

console.log(likes ([])) // must be "no one likes this"
console.log(likes (["Peter"])) // must be "Peter likes this"
console.log(likes (["Jacob", "Alex"])) // must be "Jacob and Alex like this"
console.log(likes (["Max", "John", "Mark"])) // must be "Max, John and Mark like this"
console.log(likes (["Alex", "Jacob", "Mark", "Max"])) // must be "Alex, Jacob and 2 others like this"
