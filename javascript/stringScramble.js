// Given a string and an array of index numbers, return the characters of the string rearranged to be in the order specified by the accompanying array.
// In other words, put the first character in the string at the index described by the first element of the array
// You can assume that you will be given a string and array of equal length and both containing valid characters(A - Z, a - z, or 0 - 9).

function scramble(str, arr) {
  let check = str.split("")
  let result = []
  let count = 0
  while (count < check.length) {
    result[arr[count]] = check[count]
    count++
  }
  return result.join("")
}

console.log(scramble("abcd", [0, 3, 1, 2])) // => 'acdb'

// other solution
function scramble(str, arr) {
  for (var r = [], i = 0; i < arr.length; i++) r[arr[i]] = str[i]
  return r.join("")
};

const scramble = (s, ar) => s.split('').reduce((a, e, i, arr) => a + arr[ar.indexOf(i)], '');

function scramble(str, arr) {
  let output = [];
  arr.forEach((charIdx, i) => output[charIdx] = str[i])
  return output.join('');
};