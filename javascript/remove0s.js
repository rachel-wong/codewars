  // Sort "array" so that all elements with the value of zero are moved to the
  // end of the array, while the other elements maintain order.
  // [0, 1, 2, 0, 3] --> [1, 2, 3, 0, 0]
  // Zero elements also maintain order in which they occurred.
  // [0, "0", 1, 2, 3] --> [1, 2, 3, 0, "0"]

  // Do not use any temporary arrays or objects. Additionally, you're not able
  // to use any Array or Object prototype methods such as .shift(), .push(), etc
  // the correctly sorted array should be returned.

  // I don't know why this works

  function removeZeros(array) {
    // empty array exception for tests
    if (array.length === 0) {
      return array
    }

    let zeros = 0 // track zeros to know when to stop looking

    for (i = 0, length = array.length; i < length; i++) {
      // if it's a zero in the array, push numbers back one down the queue and stick the zero in last
      if (array[i] === 0 || array[i] === '0') {
        zeros += 1
        let numMoveUp // swap any non-zero with a zero
        for (x = i + 1; x < length; x++) { // this looks for non-zero after i(ndex) of array
          numMoveUp = array[x]
          console.log('numMoveUp before move:', numMoveUp)
          // shuffle up non-zero up to zero index (left) 
          array[x] = array[x - 1]
          array[x - 1] = numMoveUp
          console.log('numMoveUp after move:', numMoveUp)
          console.log('length', length)
        }
        i = i - 1 // move overall index counting back up one // may be this could be done by for looping in reverse   
        console.log('zeros stored:', zeros)
      }
      if (i + 1 + zeros >= length) {
        return array // array sorted when all the 0s are pushed to the end of the queue
      }
    }
  }

  console.log(removeZeros([7, 2, 3, 0, 4, 6, 0, 0, 13, 0, 78, 0, 0, 19, 14])) //=> [7, 2, 3, 4, 6, 13, 78, 19, 14, 0, 0, 0, 0, 0, 0]

  //other solutions

  function shiftToEnd(array, n) {
    let end = array[n]
    for (i = n; i < array.length; i++) {
      array[i] = array[i + 1]
    }
    array[array.length - 1] = end;
    return array
  }

  function removeZeros(array) {
    var end = array.length;
    for (var i = 0; i < end; i++) {
      if (array[i] === 0 || array[i] === "0") {
        shiftToEnd(array, i)
        i--
        end--
      }
    }
    return array
  }