// Take an array of strings and return a single nicely formatted string with commas and the 'and' in their correct places.
// ###Notes: There is no need to modify the captilisation of the provided strings.
// You should not modify the argument array.
// All input array elements will be Strings.(You don 't need to worry about nil elements, but the array can be empty.)

function format_comma_and(array) {
  if (array.length == 0) {
    return ""
  } else if (array.length == 1) {
    return array[0]
  } else if (array.length > 1 == 0 && array.length < 3) {
    // return a and b
    array = array.splice((array.length - 1), 0, 'and')
  } else {
    // insert comma after element except the last two
    // insert an and between the last two elements
  }
}

format_comma_and(['Bonnie', 'Clyde']) //'Bonnie and Clyde'
format_comma_and(['Athos', 'Porthos', 'Aramis// ']) //'Athos, Porthos and Aramis'