// Given a list of integers and a single sum value, return the first two values (parse from the left please) in order of appearance that add up to form the sum.

// sum_pairs([11, 3, 7, 5], 10) => [3, 7]
// sum_pairs([10, 5, 2, 3, 7, 5], 10) => [3, 7]

function sumPairs(values, target) { // or "sum_pairs", but camelCase is the JS convention
  var limit = values.length,
    result,
    i, j;

  for (i = 0; i < limit; i++) {
    for (j = i + 1; j < limit; j++) {
      if (values[i] + values[j] == target) {
        result = [values[i], values[j]];
        limit = j;
      }
    }
  }
  return result;
}

// attempt only pushed indices of summed pairs 
// function sum_pairs(arr, sum) {
//   let pairs = []
//   for (let i = 0; i < arr.length; i++) {
//     let count = 1
//     while (count < arr.length)
//       if (arr[i] + arr[i + count] != sum) {
//         count++
//       } else {
//         let pair_index = [i, i + count]
//         pairs.push(pair_index)
//         count++
//       }
//   }
//   if (pairs.length === 1) {
//     return [arr[pairs[0][0]], arr[pairs[0][1]]]
//   } else {
//     for (let j = 0; j < pairs.length; j++) {
//       pairs[i].
//     }
//   }
// }

console.log(sum_pairs([11, 3, 7, 5], 10))
console.log(sum_pairs([10, 5, 2, 3, 7, 5], 10))