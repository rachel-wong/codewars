// REGEX (**fabulized**)
// Been avoiding it like Lynx in a peak hour bus on a school night. Putting on mah big girl pants on now and taking a deep breath.

// FYI .test returns a boolean

// You want one very specific thing (Bonus move: can include spaces yeehaw)
let crazyrichasians = "Whenever a man was finally done sowing his wild oats and ready to settle down, whichever girl happened to be there at the time would be the right one."
let theOneRegex = /wild oats/
let asianBachelor = theOneRegex.test(crazyrichasians)
console.log('asianBachelor:', asianBachelor) // => true

// Get back EXACTLY what you want
// Gotta come back to this one
let dirtyDancing = "Nobody puts Baby in a corner"
let johnnyCastleRegex = /Baby/
let timeOfMyLife = dirtyDancing.match(johnnyCastleRegex)
console.log('timeOfMyLife:', timeOfMyLife[0]) // => Baby. 
// .match returns an array so you have to specify the first element

// You want more than one possible thing. 
// The single pipe is OR
let theBigLebowski = "careful man there's a beverage here"
let drinkslistRegex = /beer|bourbon|beverage|jager bomb/
let theDude = drinkslistRegex.test(theBigLebowski)
console.log('theDude:', theDude) // => true. But false if you change the "beverage" to a "tea" or a "water", for example. 

// Case-desensitize everything
// the i flag means ignore case
let aladdin2019 = "there's a lot of gray area in 'make-me-a-Prince'."
let boybandRegex = /prince/i
let helooksnothinglikeaprince = boybandRegex.test(aladdin2019)
console.log('helooksnothinglikeaprince:', helooksnothinglikeaprince, "dat") // => true

// Want everything that matched
// the g flag
let macbeth = "Tomorrow, and tomorrow, and tomorrow"
let daggerRegex = /tomorrow/ig
let prophecy = macbeth.match(daggerRegex)
console.log('prophecy:', prophecy) // [ 'Tomorrow', 'tomorrow', 'tomorrow' ]

// Want everything
// . is the wildcard (not *)