// Move the first letter of each word to the end of it, then add "ay" to the end of the word.Leave punctuation marks untouched.

function pigIt(string) {
  let check = string.split(" ")
  let result = []
  //for every word in the array of words
  for (i = 0; i < check.length; i++) {
    // if the word IS alphabetical 
    if (check[i].match(/[A-Za-z]/i)) {
      result.push(check[i].substr(1) + check[i].charAt(0) + "ay")
    } else {
      // if not alphabetical, symbols, just add it to results
      result.push(check[i])
    }
  }
  // join them backtogether with spaces, and trim the ends
  return result.join(" ").trim()
}

console.log(pigIt('Pig latin i cool')) // igPay atinlay siay oolcay
console.log(pigIt('Hello world !')) // elloHay orldway !

// other solutions
function pigIt(str) {
  return str.replace(/\w+/g, (w) => {
    return w.slice(1) + w[0] + 'ay';
  });
}