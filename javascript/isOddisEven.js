function iqTest(string) {
  let check = string.split(" ").map(Number)
  let isEven = []
  let isOdd = []
  for (i = 0; i < check.length; i++) {
    if (check[i] % 2 != 0) {
      isOdd.push(check[i])
    } else {
      isEven.push(check[i])
    }
  }
  if (isEven.length === 1) {
    return check.indexOf(isEven[0]) + 1
  } else if (isOdd.length === 1) {
    return check.indexOf(isOdd[0]) + 1
  }
}

console.log(iqTest("2 4 7 8 10")) //=> 3 // Third number is odd, while the rest of the numbers are even

console.log(iqTest("1 2 1 1")) //=> 2 // Second number is even, while the rest of the numbers are odd

//other solutions
function iqTest(numbers) {
  numbers = numbers.split(" ").map(function (el) {
    return parseInt(el)
  });

  var odd = numbers.filter(function (el) {
    return el % 2 === 1
  });
  var even = numbers.filter(function (el) {
    return el % 2 === 0
  });

  return odd.length < even.length ? (numbers.indexOf(odd[0]) + 1) : (numbers.indexOf(even[0]) + 1);
}