// Complete the solution so that it returns a formatted string. The return value should equal "Value is VALUE" where value is a 5 digit padded number.

function padme(int) {
  let intString = int.toString()
  if (intString.length < 5) {
    intString = intString.padStart(5, '0')
  }
  return `Value is ${intString}`
}

console.log(padme(5)) // should return "Value is 00005"
console.log(padme(12344556))

// other solutions
function solution(value) {
  return "Value is " + ("00000" + value).slice(-5);
}

const solution = value => 'Value is ' + value.toString().padStart(5, '0');

function solution(value) {
  return "Value is 00000".slice(0, -value.toString().length) + value;
}