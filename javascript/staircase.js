function staircase(n) {
  let space = " "
  let hash = "#"
  for (let i = 1; i <= n; i++) {
    let numHash = i
    let numSpace = n - i
    console.log(space.repeat(numSpace) + hash.repeat(numHash))
  }
}

staircase(3)