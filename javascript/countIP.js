// Implement a function that receives two IPv4 addresses, and returns the number of addresses between them (including the first one, excluding the last one).

// All inputs will be valid IPv4 addresses in the form of strings.The last address will always be greater than the first one.

function ipsBetween(start, finish) {
  start = start.split(".").map(Number)
  console.log('start:', start)
  finish = finish.split(".").map(Number)
  console.log('finish:', finish)

  
}

console.log(ipsBetween("10.0.0.0", "10.0.0.50")) //=== 50
// console.log(ipsBetween("10.0.0.0", "10.0.1.0")) //=== 256
// console.log(ipsBetween("20.0.0.10", "20.0.1.0")) //=== 246