//  It was decided to attribute a "weight" to numbers. The weight of a number will be from now on the sum of its digits.

// For example 99 will have "weight" 18, 100 will have "weight" 1 so in the list 100 will come before 99. Given a string with the weights of FFC members in normal order can you give this string ordered by "weights" of these numbers?

// Example: "56 65 74 100 99 68 86 180 90" ordered by numbers weights becomes: "100 180 90 56 65 74 68 86 99"

// When two numbers have the same "weight", let us class them as if they were strings and not numbers: 100 is before 180 because its "weight" (1) is less than the one of 180 (9) and 180 is before 90 since, having the same "weight" (9), it comes before as a string.


// console.log(orderWeight("56 65 74 100 99 68 86 180 90")) // => "100 180 90 56 65 74 68 86 99"
console.log(orderWeight("103 123   4444 99 2000")) // => "2000 103 123 4444 99"

// unsuccessful
function firstOrderWeight(weight) {
  if (weight === [] || weight === [""]) return [] // codewars error handlings
  let weightCopy = weight.trim().split(" ").filter(char => char != "")
  let positions = []
  for (let i = 0; i < weightCopy.length; i++) {
    positions[i] = weightCopy[i].split("").map(Number).reduce((a, b) => {
      return a + b
    }, 0)
  }
  weightCopy = weightCopy.sort((a, b) => {
    return a - b
  })
  let result = weightCopy.sort((a, b) => {
    return positions.indexOf(a) - positions.indexOf(b)
  })
  return result
}