function getCount(str) {
  let vowels = "aeiou".split("")
  return str.split("").sort().filter(char =>  vowels.includes(char) == true).join("").length
}

function getCount(str) {
  let vowelsCount = 0;
  let vowels = ["a", "e", "i", "o", "u"]
  let check = str.split("")
  for (i = 0; i < check.length; i++) {
    check[i] = check[i].toLowerCase()
    if (vowels.includes(check[i])) {
      vowelsCount++
    }
  }
  return vowelsCount
}

console.log(getCount("abracadabra"))

// other solutions
function getCount(str) {
  return (str.match(/[aeiou]/ig) || []).length;
}

function getCount(str) {
  var vowelsCount = 0;
  vowelsCount = str.match(/[aeiou]/gi);
  return vowelsCount ? vowelsCount.length : 0;
}