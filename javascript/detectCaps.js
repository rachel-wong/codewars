// Given a word, you need to judge whether the usage of capitals in it is right or not.

// We define the usage of capitals in a word to be right when one of the following cases holds:

// All letters in this word are capitals, like "USA".
// All letters in this word are not capitals, like "leetcode".
// Only the first letter in this word is capital, like "Google".
// Otherwise, we define that this word doesn't use capitals in a right way.

var detectCapitalUse = function (word) {
  if (word === word.toUpperCase() || word === word.toLowerCase()) {
    return "True"
  } else if (word.charAt(0) === word.charAt(0).toUpperCase()) {
    if (word.slice(1) === word.slice(1).toLowerCase()) {
      return "True"
    } else {
      return "False" // essential to keep this here
    }
  } else {
    return "False"
  }
}

console.log(detectCapitalUse("FlaG")) //=> false
console.log(detectCapitalUse("USA")) //=> true
console.log(detectCapitalUse("leetcode")) //=> true
console.log(detectCapitalUse("Google")) //=> true

// other solutions
var detectCapitalUse = function (word) {
  // either all capitals, all small cases, or Capital follow by small cases
  return /^[A-Z]+$|^[a-z]+$|^[A-Z][a-z]+$/.test(word);
};

var detectCapitalUse = function (word) {
  return word === word.toUpperCase() || word === word[0] + word.substr(1).toLowerCase();
};