def first_non_repeating(s)
    hash = {}
    arr = s.chars
        for item in arr do
            if hash.key?(item)
                hash[item] += 1
            else
                hash[item] = 1
            end
        end
        hash.each{|key,value|
            if value == 1 && hash.has_key?(key.upcase) == false
                p key
                break
            elsif value == 1 && hash.has_key?(key.downcase) == false
                p key
                break
            end
        }
end
# Test your code here
# first_non_repeating('stress') #=> 't'
# first_non_repeating('moonmen') #=> 'e'
first_non_repeating('') #=> ''
# first_non_repeating("aaaabbbcccdeeefgh")
# first_non_repeating("wwwhhhggge")
# first_non_repeating("wwwhhhggg")
# first_non_repeating("sTreSS") #=> "T"
# first_non_repeating("abba") #=> "" or NIL
# first_non_repeating('stress') #=> t