function plusMinus(arr) {
  // calculate the ratios of its elements that are positive, negative, and zero

  console.log(arr.filter(item => item > 0).length / arr.length)
  console.log(arr.filter(item => item < 0).length / arr.length)
  console.log(arr.filter(item => item == 0).length / arr.length)
}

console.log(plusMinus([ -4, 3, -9, 0, 4, 1 ]))