// Write a function toWeirdCase (weirdcase in Ruby) that accepts a string, and returns the same string with all even indexed characters in each word upper cased, and all odd indexed characters in each word lower cased. The indexing just explained is zero based, so the zero-ith index is even, therefore that character should be upper cased.

// The passed in string will only consist of alphabetical characters and spaces(' '). Spaces will only be present if there are multiple words. Words will be separated by a single space(' ').

// second attempt
function toWeirdCase(string) {
  let words = string.split(" ")
  return words.map(word => {
    return word.split("").map((char, idx) => {
      return idx % 2 != 0 ? char.toLowerCase() : char.toUpperCase()
    }).join("")
  }).join(" ").trim()
}

// first attempt
function toWeirdCase(string) {
  if (!string || typeof string != "string") return false
  string = string.split(" ")
  console.log('string:', string)
  string = string.map(word =>
    word.split("")
    .map((char, i) => {
      if (i % 2 === 0) {
        return char = char.toUpperCase() // the key was the return 
      } else {
        return char = char.toLowerCase()
      }
    }).join(""))
  console.log('string:', string)
  return string.join(" ")
}

// function secondWeirdCase(string) {
//   if (!string || typeof string != "string") return false // handling silliness
//   string = string.split(" ")

//   string.map((word) => {
//     word = word.split("")
//     for (let i = 1; i < word.length; i++) {
//       if (i % 2 != 0) {
//         word[i] = word[i].toLowerCase()
//       } else {
//         word[i] = word[i].toUpperCase()
//       }
//     }
//     word.join("")
//   })
// }

console.log(toWeirdCase("Weird string case"))
console.log(toWeirdCase("This is a test"))
// console.log(toWeirdCase("This"))

// other solutions
function toWeirdCaseCharacter(chr, index) {
  return index % 2 ? chr.toLowerCase() : chr.toUpperCase();
}

function toWeirdCaseWord(word) {
  return word.split("").map(toWeirdCaseCharacter).join("");
}

function toWeirdCase(string) {
  return string.split(" ").map(toWeirdCaseWord).join(" ");
}

function toWeirdCase(string) {
  return string.replace(/(\w{1,2})/g, (m) => m[0].toUpperCase() + m.slice(1))
}

function toWeirdCase(string) {
  var i = 0;
  return [].map.call(string.toLowerCase(), function (char) {
    if (char == " ") {
      i = -1;
    }
    return i++ % 2 ? char : char.toUpperCase();
  }).join('');
}