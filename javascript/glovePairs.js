// A pair of gloves is constituted of two gloves of the same color.
// You are given an array containing the color of each glove.
// You must return the number of pair you can constitute.
// You must not change the input array.

function numberOfPairs(array) {
  // count up the colours first
  let count = {}
  for (i = 0; i < array.length; i++) {
    count[array[i]] = (count[array[i]] || 0) + 1
  }
  let pairs = 0
  Object.keys(count).forEach(key => {
    let value = count[key]
    // only one pair
    if (value === 2) {
      pairs++
    }
    // get max number of pairs
    else if (value > 2) {
      pairs += Math.floor(value / 2)
    }
  })
  return pairs
}

console.log(numberOfPairs(['red', 'green', 'red', 'blue', 'blue'])) // => 2 (red and blue)
console.log(numberOfPairs(['red', 'red', 'red', 'red', 'red', 'red', 'red', 'red', 'red', 'red'])) // => 3 (red)

// other solutions
function numberOfPairs(gloves) {
  gloves = gloves.sort();
  let count = 0;
  for (var i = 0; i < gloves.length; i++) {
    if (gloves[i] === gloves[i + 1]) {
      count++
      i++
    }
  }
  return count;
} // smart and painless and lo-drama

function numberOfPairs(gloves) {
  var res = {};
  gloves.map(function (x) {
    res[x] = res[x] ? res[x] + 1 : 1;
  });
  return Object.keys(res).reduce(function (a, b) {
    return a + (res[b] > 1 ? Math.floor(res[b] / 2) : 0);
  }, 0);
}

const numberOfPairs = gloves => (gloves.sort().join().match(/(\w+),\1/g) || []).length