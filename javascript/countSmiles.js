// Given an array (arr) as an argument complete the function countSmileys that should return the total number of smiling faces.
// Each smiley face must contain a valid pair of eyes. Eyes can be marked as : or ;
// A smiley face can have a nose but it does not have to. Valid characters for a nose are - or ~
// Every smiling face must have a smiling mouth that should be marked with either ) or D.
// No additional characters are allowed except for those mentioned.

// Valid smiley face examples:
// :) :D ;-D :~)

// CRYING
function countSmileys(array) {
  let faces = [":-)", ":-D", ":~)", ":~D", ":)", ":D", ";-)", ";-D", ";~)", ";~D", ";)", ";D"] // will revisit to **calculate** for eyes, nose, mouths instead of initialising all the possible permutations which is inpractical irl
  let count = 0

  for (let i = 0; i < array.length; i++) {
    if (faces.includes(array[i]) == true) count++
  }
  return count
}

countSmileys([':)', ';(', ';}', ':-D']); // should return 2;
countSmileys([';D', ':-(', ':-)', ';~)']); // should return 3;
countSmileys([';]', ':[', ';*', ':$', ';-D']); // should return 1;

// other solutions
function countSmileys(arr) {
  return arr.filter(x => /^[:;][-~]?[)D]$/.test(x)).length;
}

const SMILING = /[:;]{1}[-~]?[)D]{1}/;

const countSmileys = (faces) => faces.filter(face => SMILING.test(face)).length;