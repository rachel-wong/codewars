// A pangram is a sentence that contains every single letter of the alphabet at least once. For example, the sentence "The quick brown fox jumps over the lazy dog" is a pangram, because it uses the letters A-Z at least once (case is irrelevant).

// Given a string, detect whether or not it is a pangram. Return True if it is, False if not. Ignore numbers and punctuation.

function isPangram(string) {
  let alphabet = [..."abcdefghijklmnopqrstuvwxyz"]
  let check = [...new Set(string.replace(/\s/g, '').split("").map(char => char.toLowerCase()))]
  let count = 0
  for (let i = 0; i < check.length; i++) {
    if (alphabet.includes(check[i])) {
      count++
    } else {}
  }
  return count === 26 ? true : false
}

console.log(isPangram("The quick brown fox jumps over the lazy dog"))

// other solutions
function isPangram(string) {
  let alphabet = "abcdefghijklmnopqrstuvwxyz".split("")
  string = Array.from(new Set(string.toLowerCase().replace(/\s/g, '').split("").sort()))
  return alphabet.every(item => string.indexOf(item) != -1) ? true : false
}

// nicer solutions
function isPangram(string){
  return 'abcdefghijklmnopqrstuvwxyz'
    .split('')
    .every((x) => string.toLowerCase().includes(x));
}