// Given: an array containing hashes of names
// Return: a string formatted as a list of names separated by commas except for the last two names, which should be separated by an ampersand.

// second attempt
function list(array) {
  if (array === "") {
    return ""
  } else {
    let result = []
    array.forEach(obj => {
      result.push(obj.name)
    })
    for (let i = 0; i < result.length - 1; i++) {
      if (i != result.length - 2) {
        result[i] += ", "
      } else {
        result[i] += " & "
      }
    }
    return result.join("").trim()
  }
}

// first attempt
function list(array) {
  let result = []
  for (let i = 0; i < array.length; i++) {
    if (array[i].name) {
      result.push(array[i].name)
    }
  }
  if (result === []) {
    return ""
  } else if (result.length === 1) {
    return result[0]
  } else {
    for (let i = 1; i < result.length - 1; i++) {
      result[i] = ", " + result[i]
    }
    result[result.length - 1] = " & " + result[result.length - 1]
  }
  return result.join("")
}

list([{
  name: 'Bart'
}, {
  name: 'Lisa'
}, {
  name: 'Maggie'
}])
// returns 'Bart, Lisa & Maggie'

list([])
// returns ''

list([{
  name: 'Bart'
}])
// returns 'Bart'

// other solutions
function list(names) {
  return names.reduce(function (prev, current, index, array) {
    if (index === 0) {
      return current.name;
    } else if (index === array.length - 1) {
      return prev + ' & ' + current.name;
    } else {
      return prev + ', ' + current.name;
    }
  }, '');
}

function list(names) {
  var xs = names.map(p => p.name)
  var x = xs.pop()
  return xs.length ? xs.join(", ") + " & " + x : x || ""
}