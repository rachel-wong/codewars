// Create a function that returns the sum of the two lowest positive numbers given an array of minimum 4 positive integers. No floats or non-positive integers will be passed.

function sumLowest(arr) {
  arr = arr.sort((a, b) => a - b)
  return arr[0] + arr[1]
}

// no sort 
function sumLowest(arr) {
  let firstLow = Math.min.apply(Math, arr)
  let secondLow = Math.min.apply(Math, arr.filter(val => val != firstLow))
  return firstLow + secondLow
}

// numArray.sort((a, b) => a - b); // For ascending sort
// numArray.sort((a, b) => b - a); // For descending sort

console.log(sumLowest([19, 5, 42, 2, 77])) // => 7
console.log(sumLowest([10, 343445353, 3453445, 3453545353453])) // => 3453455

// other solutions
function sumTwoSmallestNumbers(numbers) {
  var lowest = numbers.sort(function (a, b) {
    return b - a
  }).slice(-2);
  return lowest.pop() + lowest.pop();
};