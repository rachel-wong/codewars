function solution(number) {
  let matches = []
  if (number < 0 ) return 0
  for (let i = 0; i < number; i++) {
    if (i % 5 === 0 || i % 3 === 0) {
      matches.push(i)
    }
  }
  return matches.reduce((a, b) => { return a + b }, 0) || 0
}

console.log(solution(4)) // => 23

// other Solutions
function solution(number){
  var sum = 0;

  for(var i = 1;i< number; i++){
    if(i % 3 == 0 || i % 5 == 0){
      sum += i
    }
  }
  return sum;
}