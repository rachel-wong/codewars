// Given a list lst and a number N, create a new list that contains each number of lst at most N times without reordering. For example if N = 2, and the input is [1,2,3,1,2,1,2,3], you take [1,2,3,1,2], drop the next [1,2] since this would lead to 1 and 2 being in the result 3 times, and then take 3, which leads to [1,2,3,1,2,3].

function deleteNth(nums, times) {
  if (times <= 0) return false
  let count = {}
  for (i = 0; i < nums.length; i++) {
    if (count[nums[i]] > times) {
      nums = nums.splice(i, 1)
    } else {
      count[nums[i]] = (count[nums[i]] || 0) + 1
    }
  }
  return nums
}

deleteNth([1, 1, 3, 3, 7, 2, 2, 2, 2], 3) //=> [1, 1, 3, 3, 7, 2, 2, 2]
deleteNth([1, 1, 1, 1], 2) // return [1,1]
deleteNth([1, 2, 3, 1, 2, 1, 2, 3], 2) // return [1,2,3,1,2,3]

// unsuccessful: wrong understanding of the question
// the key is "**at most** n times"
function wrongDeleteNth(nums, times) {
  if (times <= 0) return false
  let unique = [...new Set(nums)]
  let accumulator = []
  while (times > 0) {
    accumulator.push(unique)
    times--
  }
  let result = [].concat.apply([], result)
  return result
}