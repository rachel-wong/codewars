// Your task is to write a funtion luck_check(str), which returns true/True if argument is string decimal representation of a lucky ticket number, or false/False for all other numbers. 
// It should throw errors for empty strings or strings which don't represent a decimal number.

// 003111    #             3 = 1 + 1 + 1
// 813372    #     8 + 1 + 3 = 3 + 7 + 2
// 17935     #         1 + 7 = 3 + 5  // if the length is odd, you should ignore the middle number when adding the halves.
// 56328116  # 5 + 6 + 3 + 2 = 8 + 1 + 1 + 6


// omg trainwreck
function luckCheck(string) {
  if (string === "") {
    return false // say what
  } else {
    string = string.split("")
  }
  let onlyNum = /^\d+$/
  if (string.every(item => onlyNum.test(item))) {
    string = string.map(Number)
    let midpoint = Math.floor(string.length / 2)
    let leftSide = string.slice(0, midpoint)
    let rightSide = [] // initialise this out here cos scoping
    if (string.length % 2 === 0) {
      rightSide = string.slice(midpoint, string.length + 1)
    } else {
      rightSide = string.slice(midpoint + 1, string.length + 1)
    }
    if (leftSide.reduce((a, b) => {
        return a + b
      }) === rightSide.reduce((a, b) => {
        return a + b
      })) {
      return true // i'm feelin' lucky
    } else {
      return false // totes not lucky
    }
  } else {
    return false // yeh whatevs
  }
}

luckCheck('683179') //=> true
luckCheck('683000') // => false
luckCheck('6F43E8') //=> false
luckCheck('17935') // => true
luckCheck("") //=> false

// other solutions
function luckCheck(ticket) {
  var long = ticket.length;
  var left = ticket.split('').splice(0, long / 2);
  var right = ticket.split('').splice((long % 2 == 0 ? long / 2 : long / 2 + 1), long);
  return left.reduce((a, b) => +a + +b) == right.reduce((a, b) => +a + +b);
}

function luckCheck(ticket) {
  var sum = s => s.split('').map(n => Number(n)).reduce((t, n) => t + n);
  var half = ticket.length / 2;
  return sum(ticket.slice(0, half)) == sum(ticket.slice(Math.ceil(half)));
}