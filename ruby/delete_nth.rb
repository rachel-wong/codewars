# Given a list lst and a number N, create a new list that contains each number of lst at most N times without reordering. 

# For example if N = 2, and the input is [1,2,3,1,2,1,2,3], you take [1,2,3,1,2], drop the next [1,2] since this would lead to 1 and 2 being in the result 3 times, and then take 3, which leads to [1,2,3,1,2,3].

def delete_nth(array, multiple)
    new_array = array.uniq
    result_array = []
    index = 0 
    multiple.times{
        for item in new_array do
            result_array << item
        end
    }
    p result_array
end

# delete_nth([1, 2, 3, 2, 5, 4], 3)
# delete_nth([1,1,1,1],2) # return [1,1]
delete_nth([1,1,3,3,7,2,2,2,2],3) # return [20,37,21]

# array = [1,2,3,4]
# index =0 
# new_array = []
# multiple = 1
# multiple.times {
# for item in array do
#     new_array << item
# end
# }
# p new_array