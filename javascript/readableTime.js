// Write a function, which takes a non-negative integer (seconds) as input and returns the time in a human-readable format (HH:MM:SS)

// HH = hours, padded to 2 digits, range: 00 - 99
// MM = minutes, padded to 2 digits, range: 00 - 59
// SS = seconds, padded to 2 digits, range: 00 - 59
// The maximum time never exceeds 359999 (99:59:59)

function humanReadable(seconds) {
  if (seconds > 359999) return false
  let h = Math.floor(seconds / 3600).toString()
  h = pad(h)
  let m = Math.floor((seconds % 3600) / 60).toString()
  m = pad(m)
  let s = Math.floor((seconds % 3600) % 60).toString()
  s = pad(s)
  return `${h}:${m}:${s}`
}

function pad(num) {
  if (num.length === 1) {
    num = num.padStart(2, '0')
    return num
  } else {
    return num
  }
}

console.log(humanReadable(00)) // => '00:00:00'
console.log(humanReadable(359999)) // => '99:59:59'
console.log(humanReadable(86399)) // => '23:59:59'
console.log(humanReadable(360000)) // => '99:59:59'

// other solutions
function humanReadable(seconds) {
  var pad = function (x) {
    return (x < 10) ? "0" + x : x;
  }
  return pad(parseInt(seconds / (60 * 60))) + ":" +
    pad(parseInt(seconds / 60 % 60)) + ":" +
    pad(seconds % 60)
}

function humanReadable(seconds) {
  hh = Math.floor(seconds / 3600).toString();
  mm = Math.floor((seconds - hh * 3600) / 60).toString();
  ss = (seconds % 60).toString();

  hh = Array(3 - hh.length).join('0') + hh;
  mm = Array(3 - mm.length).join('0') + mm;
  ss = Array(3 - ss.length).join('0') + ss;

  return hh + ':' + mm + ':' + ss;
}

function humanReadable(seconds) {
  return [seconds / 3600, seconds % 3600 / 60, seconds % 60].map(function (v) {
    v = Math.floor(v).toString();
    return v.length == 1 ? '0' + v : v;
  }).join(':');
}