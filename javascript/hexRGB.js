// When working with color values in JavaScript it can sometimes be useful to extract the individual red, green, and blue (RGB) component values for a color. Implement a hexStringToRGB() function that meets these requirements:

// Accepts a case-insensitive hexadecimal color string as its parameter (ex. "#FF9933" or "#ff9933")
// Returns an object with the structure {r:255, g:153, b:51} where r, g, and b range from 0 through 255
// Note, your implementation does not need to support the shorthand form of hexadecimal notation (ie "#FFF").

// i wasted so much time (╯°□°）╯︵ ┻━┻ why are you the way you are javascript
function hexStringToRGB(string) {
  let rgb = {
    r: 0,
    g: 0,
    b: 0
  }
  if (typeof string != "string" || string === null || string.charAt(0) != "#" || string.length > 7) return false // handling general silliness
  let regexp = /^[0-9a-zA-Z]+$/ // only alphanumeric
  let hex = string.substring(1)
  if (hex.match(regexp)) {
    hex = hex.match(/.{1,2}/g) // split it into array of 2 characters
    rgb.r = parseInt(hex[0], 16)
    rgb.g = parseInt(hex[1], 16)
    rgb.b = parseInt(hex[2], 16)
    return rgb
  } else {
    return false
  }
}

console.log(hexStringToRGB("#FF9933")) // returns {r:255, g:153, b:51}
console.log(hexStringToRGB("FF9933")) // returns false
console.log(hexStringToRGB(123)) // returns false

// other solutions
function hexStringToRGB(h) {
  return {
    r: parseInt(h.slice(1, 3), 16),
    g: parseInt(h.slice(3, 5), 16),
    b: parseInt(h.slice(5, 7), 16)
  };
}

function hexStringToRGB(hex) {
  hex = parseInt(hex.substring(1), 16)
  return {
    r: hex >> 16,
    g: (hex & 0x00FF00) >> 8,
    b: (hex & 0x0000FF)
  }
}

function hexStringToRGB(hexString) {
  var rgb = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexString);
  return {
    r: parseInt(rgb[1], 16),
    g: parseInt(rgb[2], 16),
    b: parseInt(rgb[3], 16)
  };
}