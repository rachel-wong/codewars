// ROT13 is a simple letter substitution cipher that replaces a letter with the letter 13 letters after it in the alphabet. ROT13 is an example of the Caesar cipher.

// Create a function that takes a string and returns the string ciphered with Rot13. 
// If there are numbers or special characters included in the string, they should be returned as they are. 
// Only letters from the latin/english alphabet should be shifted, like in the original Rot13 "implementation".
// Case sensitive

// You'd think things will be better by now.

function rot13(message) {
  let alphabetChars = [...'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'] // just a bit hamfisted
  let rot13Chars = [...'nopqrstuvwxyzabcdefghijklmNOPQRSTUVWXYZABCDEFGHIJKLM']
  message = message.split("")
  for (let i = 0; i < message.length; i++) {
    // if alphabetChars.indexOf(message[i]) === -1 { i++} won't work because it'll increment after nonalphbets 
    if (alphabetChars.includes(message[i]) == true) {
      let index = alphabetChars.indexOf(message[i])
      message[i] = rot13Chars[index]
    }
  }
  return message.join("")
}

// NOT CASE SENSITIVE 
// function rot13(message) {
//   let original = message.split("").map(char => char.toLowerCase()) // change everything to single case to avoid silliness
//   let alphabet = [..."abcdefghijklmnopqrstuvwxyz"]
//   let result = []
//   for (let i = 0; i < check.length; i++) {
//     if (alphabet.includes(check[i]) == false) {
//       i++
//     } else {
//       let index = alphabet.indexOf(check[i])
//       if (index <= 13) {
//         result.push(alphabet[index + 13])
//       } else {
//         result.push(alphabet[13 - (26 - index)])
//       }
//     }
//   }
//   return result.join("")
// }

console.log(rot13("Ruby is cool!")) //=> "Ehol vf pbby!"
console.log(rot13("Test Test 123")) //=> "Grfg Grfg 123"

// other solutions
function rot13(message) {
  var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
  var b = "nopqrstuvwxyzabcdefghijklmNOPQRSTUVWXYZABCDEFGHIJKLM"
  return message.replace(/[a-z]/gi, c => b[a.indexOf(c)])
}

// i don't mind this since you don't need to manually create a dictionary
var codeA = 'A'.charCodeAt(0),
  codeZ = 'Z'.charCodeAt(0),
  codea = 'a'.charCodeAt(0),
  codez = 'z'.charCodeAt(0);

function rot13(message) {
  return message.split('').map(function (char) {
    var code = char.charCodeAt(0);
    if (codeA <= code && code <= codeZ) {
      return String.fromCharCode(((code - codeA) + 13) % 26 + codeA);
    } else if (codea <= code && code <= codez) {
      return String.fromCharCode(((code - codea) + 13) % 26 + codea);
    }
    return char;
  }).join('');
}

function rot13(message) {
  return message.split('').map(l => {
    let charCode = l.charCodeAt(0);
    if (charCode >= 97 && charCode <= 122) {
      charCode = (charCode - 97 + 13) % 26 + 97;
    } else if (charCode >= 65 && charCode <= 90) {
      charCode = (charCode - 65 + 13) % 26 + 65;
    }
    return String.fromCharCode(charCode);
  }).join('');
}