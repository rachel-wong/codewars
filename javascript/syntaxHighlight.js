function highlight(string) {
  let isNumRegex = /^-{0,1}\d+$/ // regex here is wrong
  // split string along same characters
  return string.match(/(.)\1*/g).map(item => {
    if (item.includes("F") == true) {
      return `<span style=\"color: pink\">${item}</span>`
    } else if (item.includes("R")) {
      return `<span style=\"color: green\">${item}</span>`
    } else if (item.includes("L")) {
      return `<span style=\"color: red\">${item}</span>`
    } else if (item.match(isNumRegex)) {
      return `<span style=\"color: orange\">${item}</span>`
    } else {
      return item
    }
  }).join("")
}

// console.log(highlight("F3RF5LF7")) // => "<span style=\"color: pink\">F</span><span style=\"color: orange\">3</span><span style=\"color: green\">R</span><span style=\"color: pink\">F</span><span style=\"color: orange\">5</span><span style=\"color: red\">L</span><span style=\"color: pink\">F</span><span style=\"color: orange\">7</span>"

console.log(highlight("RRRRR(F45L3)F2")) // => '<span style="color: green">RRRRR</span>(<span style="color: pink">F</span><span style="color: orange">45</span><span style="color: red">L</span><span style="color: orange">3</span>)<span style="color: pink">F</span><span style="color: orange">2</span>'
// console.log(highlight("FFFR345F2LL")) // => "<span style=\"color: pink\">FFF</span><span style=\"color: green\">R</span><span style=\"color: orange\">345</span><span style=\"color: pink\">F</span><span style=\"color: orange\">2</span><span style=\"color: red\">LL</span>"