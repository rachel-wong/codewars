// Greed is a dice game played with five six-sided dice. Your mission, should you choose to accept it, is to score a throw according to these rules. You will always be given an array with five six-sided dice values.

//  Three 1 's => 1000 points
//  Three 6 's =>  600 points
//  Three 5 's =>  500 points
//  Three 4 's =>  400 points
//  Three 3 's =>  300 points
//  Three 2 's =>  200 points
//  One 1 => 100 points
//  One 5 => 50 point

function score(dice) {
  // count up the dice values first
  let count = {
    "1": 0,
    "2": 0,
    "3": 0,
    "4": 0,
    "5": 0,
    "6": 0
  } // need to initialise the base values or it will throw up undefined and NaN errors when calculating totalscore
  for (i = 0; i < dice.length; i++) {
    count[dice[i]] = (count[dice[i]] || 0) + 1
  }
  let totalscore = 0

  // order of the conditions don't matter
  count["1"] >= 3 ? totalscore += 1000 + (count["1"] - 3) * 100 : totalscore += count["1"] * 100
  count["2"] >= 3 ? totalscore += 200 : totalscore += 0
  count["3"] >= 3 ? totalscore += 300 : totalscore += 0
  count["4"] >= 3 ? totalscore += 400 : totalscore += 0
  count["5"] >= 3 ? totalscore += 500 + (count["5"] - 3) * 50 : totalscore += count["5"] * 50
  count["6"] >= 3 ? totalscore += 600 : totalscore += 0

  return totalscore
}

console.log(score([2, 3, 4, 6, 2])) //=> 0
console.log(score([5, 1, 3, 4, 1])) //=> 250
console.log(score([1, 1, 1, 3, 1])) //=> 0
console.log(score([2, 4, 4, 5, 4])) // => 450
console.log(score([4, 4, 4, 3, 3])) //=> 400
console.log(score([2, 3, 4, 6, 2])) //=> 0

// other solutions
function score(dice) {
  let score = 0;
  const counts = [0, 0, 0, 0, 0, 0, 0];
  for (const value of dice) {
    counts[value]++;
  }
  score += counts[1] < 3 ? counts[1] * 100 : 1000 + 100 * (counts[1] - 3);
  score += counts[2] >= 3 ? 200 : 0;
  score += counts[3] >= 3 ? 300 : 0;
  score += counts[4] >= 3 ? 400 : 0;
  score += counts[5] < 3 ? counts[5] * 50 : 500 + 50 * (counts[5] - 3);
  score += counts[6] >= 3 ? 600 : 0;
  return score;
}

function score(dice) {
  var dc = [0, 0, 0, 0, 0, 0];
  var tdr = [1000, 200, 300, 400, 500, 600];
  var sdr = [100, 0, 0, 0, 50, 0];
  dice.forEach(function (x) {
    dc[x - 1]++;
  });
  return dc.reduce(function (s, x, i) {
    return s + (x >= 3 ? tdr[i] : 0) + sdr[i] * (x % 3);
  }, 0);
}

function score(dice) {
  var six = 0,
    five = 0,
    four = 0,
    three = 0,
    too = 0,
    one = 0;
  var i = 0;
  while (i < 5) {
    if (dice[i] == 6) {
      six++;
    }
    if (dice[i] == 5) {
      five++;
    }
    if (dice[i] == 4) {
      four++;
    }
    if (dice[i] == 3) {
      three++;
    }
    if (dice[i] == 2) {
      too++;
    }
    if (dice[i] == 1) {
      one++;
    }
    i++;
  }
  var r = 0;
  if (one > 2) {
    r += 1000;
    one -= 3;
  }
  if (six > 2) {
    r += 600;
  }
  if (five > 2) {
    r += 500;
    five -= 3;
  }
  if (four > 2) {
    r += 400;
  }
  if (three > 2) {
    r += 300;
  }
  if (too > 2) {
    r += 200;
  }
  r += one * 100;
  r += five * 50;
  return r;
}

// regex seems a bit overkill ?
function score(dice) {
  if (dice.length !== 5) return 0;

  let diceStr = dice.sort().join('');
  let score = 0;
  const rules = [{
      reg: /111/,
      score: 1000
    },
    {
      reg: /666/,
      score: 600
    },
    {
      reg: /555/,
      score: 500
    },
    {
      reg: /444/,
      score: 400
    },
    {
      reg: /333/,
      score: 300
    },
    {
      reg: /222/,
      score: 200
    },
    {
      reg: /1/,
      score: 100
    },
    {
      reg: /5/,
      score: 50
    },
  ];

  rules.forEach(rule => {
    while (rule.reg.test(diceStr)) {
      diceStr = diceStr.replace(rule.reg, '');
      score += rule.score;
    }
  });

  return score;
}