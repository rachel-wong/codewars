// The rgb() method is incomplete.Complete the method so that passing in RGB decimal values will result in a hexadecimal representation being returned.The valid decimal values
// for RGB are 0 - 255. Any(r, g, b) argument values that fall out of that range should be rounded to the closest valid value.

// The following are examples of expected output values:


function componentToHex(c) {
  let hex = 0
  if (c < 0) {
    return hex = "00"
  } else if (c > 255) {
    return hex = "FF"
  } else {
    hex = c.toString(16)
    return hex.length == 1 ? "0" + hex.toUpperCase() : hex.toUpperCase()
  }
}

function rgb(r, g, b) {
  return componentToHex(r) + componentToHex(g) + componentToHex(b)
}

// rgb(255, 255, 255) // returns FFFFFF
// rgb(255, 255, 300) // returns FFFFFF
// rgb(0, 0, 0) // returns 000000
// rgb(148, 0, 211) // returns 9400D3