// It must start with a hashtag(#).
// All words must have their first letter capitalized.
// If the final result is longer than 140 chars it must return false.
// If the input or the result is an empty string it must return false.

// this took longer than it needed to wrangle silly test cases
function generateHashtag(string) {
  // this "error handling" leaves much to be desired ¯\_(ツ)_/¯
  string = string.trim() // catch silly edge cases where it's all spaces
  if (string === "" || string === " " || string === ['']) return false // so silly

  // capitalise first char of every word
  string = string.split(" ").map(word => word.charAt(0).toUpperCase() + word.substring(1)).join("")
  let hashtag = "#".concat(string)
  if (hashtag.length > 140) {
    return false
  } else {
    return hashtag
  }
}

// There was an attempt
// function generateHashtag(string) {
//   string === "" || string === " " || string === null || string === undefined ? false : string = string.replace(/\s/g, '')
//   let array = Array.from(string)
//   if (array[0] == undefined) {
//     return false
//   }
//   if (array[0] == array[0].toLowerCase()) {
//     array[0] = array[0].toUpperCase()
//     array.unshift("#")
//   } else {
//     array.unshift("#")
//   }
//   array.length > 140 ? false : array.join("")
// }

console.log(generateHashtag(" hello there thanks for trying my Kata")) // => "#HelloThereThanksForTryingMyKata"
console.log(generateHashtag("    jello     World   ")) // => "#HelloWorld"
console.log(generateHashtag(" ".repeat(20))) // => false
console.log(generateHashtag("Do We have A Hashtag")), "#DoWeHaveAHashtag", "Expected a Hashtag (#) at the beginning."
console.log(generateHashtag("CodeWars")) //"#Codewars", "Should handle a single word.")
console.log(generateHashtag("Codewars Is Nice")) //, "#CodewarsIsNice", "Should remove spaces.")
console.log(generateHashtag("Codewars is nice")) //, "#CodewarsIsNice", "Should capitalize first letters of words.")
console.log(generateHashtag("code" + " ".repeat(140) + "wars"), "#CodeWars")
console.log(generateHashtag("Looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong Cat"), false, "Should return false if the final word is longer than 140 chars.")
console.log(generateHashtag("a".repeat(139)))
console.log(generateHashtag("a".repeat(140)))

// other solutions
function generateHashtag(str) {
  return str.length > 140 || str === '' ? false :
    '#' + str.split(' ').map(capitalize).join('');
}

function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}
//
function generateHashtag(str) {
  if (!str || str.length < 1) return false;

  var r = '#' + str.split(' ').map(function (el) {
    return el.charAt(0).toUpperCase() + el.slice(1).toLowerCase();
  }).join('');
  return r.length > 140 ? false : r;
}

// is brevity the beauty standard?
function generateHashtag(str) {
  var hashed = '#' + str.split(' ').map(function (v) {
    return v.charAt(0).toUpperCase() + v.slice(1);
  }).join('');
  return hashed.length > 140 || str == "" ? false : hashed;
}