function dirReduc(arr) {
  let directions = {
    "NORTH": 0,
    "SOUTH": 0,
    "WEST": 0,
    "EAST": 0
  }
  let result = []
  // count up all the directions
  for (i = 0; i < arr.length; i++) {
    directions[arr[i]] = (directions[arr[i]] || 0) + 1
  }

  // calculator
  if (directions["NORTH"] > directions["SOUTH"]) {
    let final = directions["NORTH"] - directions["SOUTH"]
    result.push("NORTH".repeat(final))
  } else if (directions["NORTH"] < directions["SOUTH"]) {
    let final = directions["SOUTH"] - directions["NORTH"]
    result.push("SOUTH".repeat(final))
  }

  if (directions["EAST"] > directions["WEST"]) {
    let final = directions["EAST"] - directions["WEST"]
    result.push("EAST".repeat(final))
  } else if (directions["EAST"] < directions["WEST"]) {
    let final = directions["WEST"] - directions["EAST"]
    result.push("WEST".repeat(final))
  }
  return result
}

console.log(dirReduc(["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"])) // => "WEST"

// other solutions
function dirReduc(arr) {
  arr = arr.join('1')
  while (/NORTH1+SOUTH|EAST1+WEST|WEST1+EAST|SOUTH1+NORTH/.test(arr)) {
    arr = arr.replace(/NORTH1+SOUTH|EAST1+WEST|WEST1+EAST|SOUTH1+NORTH/, '')
  }
  return arr.split('1').filter((s) => /[A-Z]+/.test(s))
}

function dirReduc(plan) {
  var opposite = {
    'NORTH': 'SOUTH',
    'EAST': 'WEST',
    'SOUTH': 'NORTH',
    'WEST': 'EAST'
  };
  return plan.reduce(function (dirs, dir) {
    if (dirs[dirs.length - 1] === opposite[dir])
      dirs.pop();
    else
      dirs.push(dir);
    return dirs;
  }, []);
}

function dirReduc(arr) {
  var opposite = {
    "SOUTH": "NORTH",
    "NORTH": "SOUTH",
    "WEST": "EAST",
    "EAST": "WEST"
  }
  return arr.reduce(function (a, b, i) {
    opposite[a.slice(-1)] === b ? a.pop() : a.push(b)
    return a
  }, [])
}

function isOppo(dir1, dir2) {
  if (dir1 + dir2 === 'SOUTHNORTH') return true;
  if (dir1 + dir2 === 'NORTHSOUTH') return true;
  if (dir1 + dir2 === 'EASTWEST') return true;
  if (dir1 + dir2 === 'WESTEAST') return true;
  return false;
}

function dirReduc(arr) {
  var len = arr.length
  for (var i = 0; i < len - 1; i++) {
    if (isOppo(arr[i], arr[i + 1])) {
      arr.splice(i, 2);
      return dirReduc(arr);
    }
  }
  return arr;
}

// set up a map for the opposing values
var cancelMap = {
  "NORTH": "SOUTH",
  "SOUTH": "NORTH",
  "EAST": "WEST",
  "WEST": "EAST"
};

function dirReduc(arr) {
  // flag to track if the array was changed in order to recurse
  var changed = false;
  // loop through the array, eliminating opposing pairs
  for (var i = 0; i < arr.length - 1; i++) {
    if (arr[i + 1] == cancelMap[arr[i]]) {
      arr.splice(i, 2);
      changed = true;
    }
  }
  // if array was altered at all
  // recurse to check again for new matches
  return changed ? dirReduc(arr) : arr
}

var opp = {
  "NORTH": "SOUTH",
  "SOUTH": "NORTH",
  "EAST": "WEST",
  "WEST": "EAST"
}

function dirReduc(arr) {

  var fil = arr.filter((dir, i, arr) =>
    opp[dir] !== arr[i + 1] && opp[arr[i - 1]] !== dir || false
  );

  if (fil.length === arr.length) {
    return fil;
  } else {
    return dirReduc(fil);
  }
}