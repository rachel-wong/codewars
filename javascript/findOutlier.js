function findOutlier(integers) {
  let even = 0
  let odd = 0

  // find what is unique in the list
  for (i = 0; i < integers.length; i++) {
    if (integers[i] % 2 != 0) {
      odd++
    } else {
      even++
    }
  }
  // if even is the outlier, return the value
  if (even === 1) {
    for (i = 0; i < integers.length; i++) {
      if (integers[i] % 2 === 0) {
        return integers[i]
      }
    }
  }
  //if odd is the outlier, return the value
  if (odd === 1) {
    for (i = 0; i < integers.length; i++) {
      if (integers[i] % 2 != 0) {
        return integers[i]
      }
    }
  }
}
console.log(findOutlier([2, 4, 0, 100, 4, 11, 2602, 36]))
console.log(findOutlier(
  [160, 3, 1719, 19, 11, 13, -21]))

// other solutions
function findOutlier(integers) {
  const even = integers.filter(int => int % 2 === 0);
  const odd = integers.filter(int => int % 2 !== 0);
  return even.length === 1 ? even[0] : odd[0];
}


function findOutlier(integers) {
  return integers.slice(0, 3).filter(even).length >= 2 ? integers.find(odd) : integers.find(even);
}

function even(num) {
  return (num % 2 == 0);
}

function odd(num) {
  return !even(num)
}