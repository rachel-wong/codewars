// Write a function that takes in a string of one or more words, and returns the same string, but with all five or more letter words reversed (Just like the name of this Kata). Strings passed in will consist of only letters and spaces. Spaces will be included only when more than one word is present.

// second attempt
function spinWords(string) {
  return string.split(" ").map(word => {
    return word.length >= 5 ? word.split("").reverse().join("") : word
  }).join(" ")
}

// first attempt
function spinWords(string) {
  let check = string.split(" ")
  for (let i = 0; i < check.length; i++) {
    if (check[i].length >= 5) {
      check[i] = check[i].split("").reverse().join("")
    }
  }
  return check.join(" ").trim()
}

console.log(spinWords("Hey fellow warriors")) //=> returns "Hey wollef sroirraw"
console.log(spinWords("This is a test")) //=> returns "This is a test"
console.log(spinWords("This is another test")) //=> returns "This is rehtona test"

// other solutions
function spinWords(words) {
  return words.split(' ').map(function (word) {
    return (word.length > 4) ? word.split('').reverse().join('') : word;
  }).join(' ');
}