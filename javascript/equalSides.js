// You are going to be given an array of integers. 
// Your job is to take that array and find an index N where the sum of the integers to the left of N is equal to the sum of the integers to the right of N. If there is no index that would make this happen, return -1.

// You are given the array {20,10,-80,10,10,15,35}
// At index 0 the left side is {}
// The right side is {10,-80,10,10,15,35}
// They both are equal to 0 when added. (Empty arrays are equal to 0 in this problem)
// Index 0 is the place where the left side and right side are equal.

// Input:
// An integer array of length 0 < arr < 1000. The numbers in the array can be any integer positive or negative.

// Output:
// The lowest index N where the side to the left of N is equal to the side to the right of N. If you do not find an index that fits these rules, then you will return -1.

// Note: If you are given an array with multiple answers, return the lowest correct index.

// had to look this up
function findEvenIndex(arr) {
  let leftSum = 0
  let rightSum = 0

  for (let i = 0; i < arr.length; i++) {
    // left to current item
    let left = arr.slice(0, i)
    // right of current item
    let right = arr.slice(i + 1, arr.length)

    // if current posit is larger than 0 (after first item in array), sum up everything to the left
    if (i > 0) {
      leftSum = left.reduce((a, b) => a + b)
    } else {
      leftSum = 0
    }

    // if current posit is smaller than last index position (before last item in array), sum up everything on the right
    if (i < arr.length - 1) {
      rightSum = right.reduce((a, b) => a + b)
    } else {
      rightSum = 0
    }

    // if left = sum, then return current posit
    if (leftSum === rightSum) {
      return i
    }
    // if reaches to the end of the array and still don't equal 
    else if (i == arr.length - 1) {
      return -1
    }
  }
}

// no one passes the test first try on the dry i'll come back for u
function findEvenIndex(array) {
  let leftSum = 0
  for (let i = 0; i < array.length; i++) {
    leftSum += array[i]
    console.log('leftSum:', leftSum)
    if (i + 2 < array.length) {
      let rightArr = array.slice(i + 2, array.length)
      let rightSum = rightArr.reduce((a, b) => a + b)
      // console.log('leftSum:', leftSum, 'rightSum:', rightSum)
      // console.log('rightArr:', rightArr)
      if (leftSum == rightSum) {
        return i + 1
      }
    } else {
      return -1
    }
  }
}

// console.log(findEvenIndex([1, 2, 3, 4, 5, 6])) // => -1
// console.log(findEvenIndex([20, 10, 30, 10, 10, 15, 35])) // => 3
// console.log(findEvenIndex([20, 10, -80, 10, 10, 15, 35])) // => 0 whut
console.log(findEvenIndex([8, 0])) // => 0 whut whut
// console.log(findEvenIndex([7, 3, -3])) // => 0 whut whut whut