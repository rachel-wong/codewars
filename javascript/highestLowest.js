//In this little assignment you are given a string of space separated numbers, and have to return the highest and lowest number.

function highAndLow(str) {
  let check = str.split(" ").map(Number).sort((a, b) => {
    return a - b
  })
  return `${check[check.length - 1]} ${check[0]}`.toString()
}


console.log(highAndLow("1 2 3 4 5")) // return "5 1"
console.log(highAndLow("1 2 -3 4 5")) // return "5 -3"