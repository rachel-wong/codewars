// Sometimes it's useful to know on which day of the week Christmas, the holly holiday, will occur.
// Write a function which takes the date of Christmas, and outputs the day of the week it falls on. Just don't limit yourself to this year. 
// Only valid Christmas dates will be passed to the function.
// Date parameter could be a string or a Date object. If it's a string here are possible date parameter formats:
// Note: calendar used in the kata is Gregorian.

function findOutChristmasWeekday(string) {
  let date = new Date(string)
  let day = date.getDay()
  switch (day) {
    case 0:
      return "Sunday"
      break
    case 1:
      return "Monday"
      break
    case 2:
      return "Tuesday"
      break
    case 3:
      return "Wednesday"
      break
    case 4:
      return "Thursday"
      break
    case 5:
      return "Friday"
      break
    case 6:
      return "Saturday"
      break
    default:
      return "Wrong date"
      break
  }
}

console.log(findOutChristmasWeekday('2013 12 25')) // returns 'Wednesday'

// other solutions
function findOutChristmasWeekday(date) {
  var day = new Date(date).getDay();
  return ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][day]
}

function findOutChristmasWeekday(date) {
  var weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  return weekDays[new Date(date).getDay()];
}

const findOutChristmasWeekday = date =>
  (new Date(date + '')).toLocaleDateString('en-US', {
    weekday: 'long'
  })

function findOutChristmasWeekday(date) {
  // your magic
  const weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

  return weekday[new Date(date).getDay()];

}