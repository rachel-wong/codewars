// There is an array with some numbers. All numbers are equal except for one. Try to find it!

// findUniq([1, 1, 1, 2, 1, 1]) === 2
// findUniq([0, 0, 0.55, 0, 0]) === 0.55
// It’ s guaranteed that array contains more than 3 numbers.

function findUniq(arr) {
  let unique;
  arr.forEach((item, idx, arr) => {
    if (arr.indexOf(item) === arr.lastIndexOf(item)) {
      unique = item
    }
  })
  return unique
}

function findUniq(array) {
  let count = {}
  array.forEach(num => {
    count[num] = (count[num] || 0) + 1
  })
  let result = Object.keys(count).filter(key => {
    return count[key] === 1
  })[0] // result here returns a string
  return parseFloat(result)
}

// this works but failed the edge cases (infinity, large number tests)
function firstFindUniq(array) {
  let count = {}
  for (i = 0; i < array.length; i++) {
    count[array[i]] = (count[array[i]] || 0) + 1
  }
  //parseInt wont' work here because need to retain for infinity, large +/-ive numbers
  return parseFloat(findValue(count, 1))
}
// helper
function findValue(hash, target) {
  // Object.keys return the property names in a hash
  return Object.keys(hash).find(key => hash[key] === target)
}

console.log(findUniq([1, 1, 1, 2, 1, 1]))
console.log(findUniq([0, 0, 0.55, 0, 0]))

// other solution
function findUniq(arr) {
  arr.sort((a, b) => a - b);
  return arr[0] == arr[1] ? arr.pop() : arr[0]
}

function findUniq(arr) {
  let [a, b, c] = arr.slice(0, 3);
  if (a != b && a != c) return a;
  for (let x of arr)
    if (x != a) return x
}

function findUniq(arr) {
  return +arr.filter((value) => {
    return arr.indexOf(value) == arr.lastIndexOf(value)
  });
}

function findUniq(arr) {
  return arr.filter(function (elem) {
    return arr.indexOf(elem) === arr.lastIndexOf(elem)
  })[0]

}