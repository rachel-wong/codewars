// Parse through a string in the following format
// accum("abcd") - > "A-Bb-Ccc-Dddd"
// accum("RqaEzty") - > "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
// accum("cwAt") - > "C-Ww-Aaa-Tttt"

// second attempt
function accum(string) {
  let result = []
  string = string.split("").forEach((char, index) => {
    result.push(char.toUpperCase() + char.repeat(index).toLowerCase())
  })
  return result.join("-").trim()
}

// first attempt
function accum(s) {
  if (s === "" || s === null) return false // no bueno for empty silliness
  let array = s.split("")
  for (let i = 0; i < array.length; i++) {
    if (typeof array[i] === Number) return false //no bueno for anything that is not a string
    if (i < array.length - 1) {
      array[i] = array[i].toUpperCase() + array[i].repeat(i).toLowerCase() + "-"
    } else {
      array[i] = array[i].toUpperCase() + array[i].repeat(i).toLowerCase()
    }
  }
  return array.join("")
}

console.log(accum("abcd"))
console.log(accum("RqaEzty"))

// other solutions
function accum(s) {
  return s.split('').map((c, i) => (c.toUpperCase() + c.toLowerCase().repeat(i))).join('-');
}

function accum(str) {
  var letters = str.split('');
  var result = [];
  for (var i = 0; i < letters.length; i++) {
    result.push(letters[i].toUpperCase() + Array(i + 1).join(letters[i].toLowerCase()));
  }
  return result.join('-');
}