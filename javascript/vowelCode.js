function encode(string) {
  let vowels = "aeiou".split("")
  return string.split("").map(item => {
    if (vowels.indexOf(item.toLowerCase()) != -1) {
      item = (vowels.indexOf(item.toLowerCase()) + 1)
      return item
    } else {
      return item
    }
  }).join("").toString()
}

function decode(string) {
  let vowels = "aeiou".split();
  return string.split("").map(item => {
    if (!isNaN(item) && item <= 5) {
      console.log("item", item)
      item = (vowels[item - 1])
      return item
    } else {
      return item
    }
  }).join("").toString();
}

// console.log(encode("hello"))
// console.log(encode("rach"))

console.log(decode("h3 th2r2"))