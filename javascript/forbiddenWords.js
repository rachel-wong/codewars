// You will be given the fan letter text and a list of forbiddenWords. Your algorithm should replace all occurrences of the forbidden words in the text with sequences of asterisks of the same length.
// Be careful to censor only words, no one want to see "classic" spelled as "cl***ic". The censored text. Its length should be the same as the length of text.
// Text to censor, composed of mixed case English (or non-English, for random cases) words separated by a single whitespace character each. No punctuation is used. All words may consist of Latin alphabet letters only.


function censorThis(string, array) {
  let check = string.split(" ")
  for (let i = 0; i < check.length; i++) {
    // not entirely case insensitive becaue it assumes array is always in lowercase ¯\_(ツ)_/¯ . don't poke the bear
    if (array.includes(check[i].toLowerCase()) == true) check[i] = "*".repeat(check[i].length)
  }
  return check.join(" ")
}
console.log(censorThis("The cat does not like the fire", ["cat", "fire"]))

// other solutions
const censorThis = (s, ws) => s.replace(new RegExp(`\\b(?:${ws.join('|')})\\b`, 'gi'), w => '*'.repeat(w.length));

function censorThis(text, forbiddenWords) {
  var words = text.split(' ');
  var censoredWords = words.map(function (word) {
    return forbiddenWords.indexOf(word.toLowerCase()) === -1 ? word : '*'.repeat(word.length);
  });
  return censoredWords.join(' ');
}