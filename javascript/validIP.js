// Write an algorithm that will identify valid IPv4 addresses in dot-decimal format. IPs should be considered valid if they consist of four octets, with values between 0 and 255, inclusive.
// Input to the function is guaranteed to be a single string.
// Valid inputs:
// 1.2.3.4
// 123.45 .67 .89

// Invalid inputs:
// 1.2.3
// 1.2 .3 .4 .5
// 123.456 .78 .90
// 123.045 .067 .089

// Note that leading zeros (e.g. 01.02.03.04) are considered invalid.

// leading zero check took way too long
function isValidIP(string) {
  if (string === "") return false
  string = string.split(".")
  // i dont wanna regex u cant make me 
  // are they all ONLY numbers & there are only 4 in the array?
  let onlyNum = /^\d+$/
  if (string.every(item => onlyNum.test(item)) && string.length === 4) {
    if (string.every(notLeadingZero)) {
      string = string.map(Number)
      // are they within range?
      if (string.every(item => item <= 255) && string.every(item => item >= 0)) {
        return true
      }
    }
  }
  return false
}

// this is a leading zero = it is NOT a "0" && it starts with a "0"
function notLeadingZero(string) {
  if (string === "0") {
    return true
  } else if (string.charAt(0) != "0") {
    return true
  }
  return false
}

console.log(isValidIP('79.080.38.143')) //=> false
console.log(isValidIP("137.255.156.100")) //=> true
console.log(isValidIP("0.0.0.0")) //=> true
console.log(isValidIP("00.0.0.0")) //=> false
console.log(isValidIP("137.255.156.100")) //=> true
console.log(isValidIP('\n1.2.3.4')) //=> false
console.log(isValidIP('1e0.1e1.1e2.2e2')) //=> false
console.log(isValidIP('1.2.3.4.5')) //=> false
console.log(isValidIP('')) // => false

// console.log(leadingZero("080"))
// console.log(leadingZero("80"))
// console.log(leadingZero("0"))

// ye olde regex
// function isValidIP(str) {
//   return /^(([1-9]?\d|1\d\d|2[0-4]\d|25[0-5])(\.(?!$)|$)){4}$/.test(str);
// }

// function isValidIP(str) {
//   var p = str.split('.');
//   return p.length == 4 && p.every(function (s) {
//     return /^\d+$/.test(s) && s >= 0 && s <= 255;
//   });
// }

// // favourite of the lot
// function isValidIP(str) {
//   return str.split('.').filter(function (v) {
//     return +v <= 255 && +v >= 0 && v.length == String(+v).length;
//   }).length == 4;
// }