// Some numbers have funny properties. For example:
// 89-- > 8¹ + 9² = 89 * 1
// 695-- > 6² + 9³ + 5⁴ = 1390 = 695 * 2
// 46288-- > 4³ + 6⁴ + 2⁵ + 8⁶ + 8⁷ = 2360688 = 46288 * 51

// Given a positive integer n written as abcd...(a, b, c, d...being digits) and a positive integer p

// we want to find a positive integer k, if it exists, such as the sum of the digits of n taken to the successive powers of p is equal to k * n.
// In other words: Is there an integer k such as: (a ^ p + b ^ (p + 1) + c ^ (p + 2) + d ^ (p + 3) + ...) = n * k

// If it is the case we will return k, if not return -1.
// Note: n and p will always be given as strictly positive integers.

// second attempt
function digPow(n, p) {
  let sum = n.toString().split("").map(Number).reduce((a, b, idx) => {
    return a + Math.pow(b, idx + p)
  }, 0)
  return sum % n ? -1 : sum / n
}

// first attempt
function digPow(num, pow) {

  //convert the number into an array of integers
  let arr = num.toString().split('').map(Number)

  // the pattern is pow starts with the leftmost interger and increments by 1
  // therefore the exponent pow for each integer is  index + pow
  let tempSum = arr.reduce((acc, currentVal, indexPosition) => {
    return acc + Math.pow(currentVal, indexPosition + pow)
  }, 0)

  // if the sum divides into num evenly then return the divisor, otherwise -1/false it
  if (tempSum % num === 0) {
    return tempSum / num
  } else {
    return -1
  }
}

console.log(digPow(89, 1)) //=> 1
console.log((digPow(92, 1))) //=> -1
console.log((digPow(46288, 3))) //=> 51

// other solutions

function digPow(n, p) {
  var x = String(n).split("").reduce((s, d, i) => s + Math.pow(d, p + i), 0)
  return x % n ? -1 : x / n
}

function digPow(n, p) {
  var ans = n.toString().split('')
    .map((v, i) => Math.pow(parseInt(v), i + p))
    .reduce((a, b) => a + b) / n;
  return ans % 1 == 0 ? ans : -1;
}

// for loop version
function digPow(n, p) {
  var num1 = n.toString();
  var num2 = p.toString();
  var sum = 0;

  for (var i = 0; i < num1.length; i++) {
    sum = sum + Math.pow(parseInt(num1.charAt(i)), p);
    p++;
    if (sum % n == 0) {
      return sum / n;
    }
  }
  return -1;
}