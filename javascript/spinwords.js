function spinWords(string) {
  return string
    .split(" ")
    .map(item => {
      if (item.length >= 5) {
        return item.split("").reverse().join("")
      } else {
        return item
      }
    })
  .join(" ")
}
console.log(spinWords("Just kidding there is still one more"))//Hey wollef sroirraw

// other solutions
function spinWords(words){
  return words.split(' ').map(function (word) {
    return (word.length > 4) ? word.split('').reverse().join('') : word;
  }).join(' ');
}
