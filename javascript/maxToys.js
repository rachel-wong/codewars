function maximumToys(prices, k) {
    var bought = 0
    var pricings = prices.sort((a, b) => a - b)
  var amtLeft = k;


    for (var i = 0; i < pricings.length; i++){
      if (amtLeft < pricings[i]) {
          // if the price is more than the amount of money remaining
        // look for the next toy
            break;
        } else {

          // make a purchase
          // find the next toy that will pay off the remainder
        // record down a purchase has been made
            amtLeft = amtLeft - pricings[i];
            bought++;
        }
    }
    return bought // return the number of toys that can be purchased
}

console.log(maximumToys([9, 2, 3, 4, 5], 7))