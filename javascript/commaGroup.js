//Finish the solution so that it takes an input n (integer) and returns a string that is the decimal representation of the number grouped by commas after every 3 digits.

function groupComma(n) {
  return n.toLocaleString()
}

console.log(groupComma(2221221221))

// other solutions
function groupByCommas(n) {
  return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function groupByCommas(n) {
  var s = n.toString(),
    r = [];

  // reverse number string so we can easily count up in blocks of 3
  s = reverse(s);

  for (var i = 0, l = s.length; i < l; i += 3) {
    r.push(s.substr(i, 3));
  }

  // combine the groups of 3 numbers into string, then reverse back to original order
  return reverse(r.join(','));
}

function reverse(s) {
  return s.split('').reverse().join('');
}