function candles(arr) {
  let max = Math.max(...arr)
  return arr.filter(item => item == max ).length
}

console.log(candles([4, 4, 1, 3])) // 2