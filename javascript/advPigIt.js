// Pig latin is created by taking all the consonants before the first vowel of a word and moving them to the back of the word followed by the letters "ay".

function pigIt(string) {
  return string.split(" ").map(word => {
    word = word.split("")
    let idx = word.findIndex(isVowel)
    if (idx === 0) {
      return word.join("") + "way"
    } else if (idx != 0 && word[0] === word[0].toUpperCase()) {
      word = word.map(char => {
        return char.toLowerCase()
      })
      word = word.slice(idx).concat(word.slice(0, idx)).join("") + "ay"
      return word.charAt(0).toUpperCase() + word.substring(1).toLowerCase()
    } else if (idx != 0 && word[0] === word[0].toLowerCase()) {
      return word.slice(idx).concat(word.slice(0, idx)).join("") + "ay"
    }
  }).join(" ")
}

function isVowel(char) {
  let vowel = /[aeiou]/ig
  return vowel.test(char)
}

console.log(pigIt("hello")) //=> "ellohay"
console.log(pigIt("creating")) //=> "eatingcray"
console.log(pigIt("algorithm")) //=> "algorithmway"
console.log(pigIt("Hello World")) //=> "Ellohay Orldway"
console.log(pigIt("Pizza? Yes please!")) //=> "Izzapay? Esyay easeplay!"