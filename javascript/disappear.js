// Given an array of integers where 1 ≤ a[i] ≤ n (n = size of array), some elements appear twice and others appear once.
// Find all the elements of [1, n] inclusive that do not appear in this array.
// Could you do it without extra space and in O(n) runtime? You may assume the returned list does not count as extra space.

// Assumption: difference between each number is always 1
var findDisappearedNumbers = function (nums) {
  let check = []
  let unique = [...new Set(nums)].sort()
  for (let i = unique[0]; i <= unique[unique.length - 1]; i++) {
    check.push(i)
  }
  console.log(check)
  let difference = check.filter(x => !unique.includes(x))
  if (nums.length - unique.length === difference.length) {
    return difference
  } else {}
}

// console.log(findDisappearedNumbers([4, 3, 2, 7, 8, 2, 3, 1])) // => [5, 6]
console.log(findDisappearedNumbers([1, 1]))