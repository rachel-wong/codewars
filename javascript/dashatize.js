// Given a number, return a string with dash'-'marks before and after each odd integer, but do not begin or end the string with a dash mark.

function dashatize(num) {
  num = num < 0 ? Math.abs(num) : num
  if (isNaN(num)) return "NaN"
  return num.toString().split("").map(item => parseInt(item, 10)).map(item => {
      if (item % 2 != 0) {
        item = "-" + item.toString() + "-"
        return item
      } else {
        return item
      }
   }).join("").replace(/--+/g, "-").replace(/^\-|\-$/g, '')
}

function dashatize(num) {
  // codewars error handling
  if (num.length === 1) return num
  if (isNaN(num)) return 'NaN'
  if (num < 0) {
    num = Math.abs(num) // convert any negatives to positive nums
  }
  let numArr = num.toString().split("").map(Number)
  for (let i = 0; i < numArr.length; i++) {
    // if current is odd & next is even
    if (numArr[i] % 2 != 0 && numArr[i + 1] % 2 === 0) {
      numArr[i] = "-" + numArr[i] + "-"
      // if current & next are both odd
    } else if (numArr[i] % 2 != 0 && numArr[i + 1] % 2 != 0) {
      numArr[i] = "-" + numArr[i]
    }
  }
  // get rid of dashes front and end
  return numArr.join("").replace(/(^-|-$)/g, "").trim()
}

console.log(dashatize(274)) // - > '2-7-4'
console.log(dashatize(6815)) // - > '68-1-5')

// unsuccessful
function firstDashatize(num) {
  num = num.toString().split("").map(Number)
  if (num.length === 1) return num[0]
  for (let i = 0; i < num.length; i++) {
    if (num[i] % 2 != 0 && i === 0) {
      num.splice(i + 1, 0, "-")
    } else if (num[i] % 2 != 0 && i === num.length - 1) {
      num.splice(i - 1, 0, "-")
    } else if (num[i] % 2 != 0) {
      num.splice(i - 1, 0, "-")
      num.splice(i + 1, 0, "-")
    } else {
      i++
    }
  }
  return num.join("").trim()
}

// other solutions
function dashatize(num) {
  return String(num)
    .replace(/([13579])/g, "-$1-")
    .replace(/--+/g, "-")
    .replace(/(^-|-$)/g, "")
}

function dashatize(num) {
  return isNaN(num) ? 'NaN' : num.toString().match(/([13579]|[02468]+)/g).join('-');
};

function dashatize(num) {
  let numStr = addDashes(num.toString());
  return stripDashes(numStr);
};

function addDashes(str) {
  return str.replace(/([13579])/g, '-$1-');
}

function stripDashes(str) {
  return str.replace(/--/g, '-').replace(/^-|-$/g, '');
}

function dashatize(num) {
  var dashed = Math.abs(num).toString().split('').map(a => +a % 2 == 1 ? '-' + a + '-' : a).join('');
  if (dashed[0] == '-') {
    dashed = dashed.slice(1);
  }
  if (dashed[dashed.length - 1] == '-') {
    dashed = dashed.slice(0, dashed.length - 1);
  }
  for (var i = 0; i < dashed.length; i++) {
    if (dashed[i] == '-') {
      if (dashed[i + 1] == '-') {
        dashed = dashed.substring(0, i + 1) + dashed.substring(i + 2);
      }
    }
  }
  return dashed;
};