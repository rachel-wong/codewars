// You have to create a function that takes a positive integer number and returns the **NEXT** BIGGER number formed by the same digits:
// 12 == > 21
// 513 == > 531
// 2017 == > 2071 (NOT 2710!!)

// If no bigger number can be composed using those digits, return -1:
// 9 == > -1
// 111 == > -1
// 531 == > -1
// https://medium.com/@destefanoflavio/kata-resolution-next-bigger-number-with-the-same-digits-4eab491852d2

function nextBigger(num) {

  let arr = num.toString().split('').map(Number)
  // reading from the right/end of the array (going from right to left doesn't change the index value)
  for (let i = arr.length - 1; i >= 0; i--) {
    console.log("value", arr[i], " is at ", i)
    // if the first/current digit is smaller than the one on the right, this is your PIVOT
    if (arr[i] < arr[i + 1]) {
      console.log("pivot is", arr[i], "at", i)
    }
  }
}
// replace PIVOT with the next bigger digit on the right side
// sort right side from smallest to largest
// join left side + PIVOT + right side and return

console.log(nextBigger(2017431))
// console.log(nextBigger(12))
// console.log(nextBigger(513))
// console.log(nextBigger(111))