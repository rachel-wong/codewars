// Earlier hackyriffic version of move0s

// Write an algorithm that takes an array and moves all of the zeros to the end, preserving the order of the other elements.

// Another attempt
var moveZeros = function (arr) {
  let zeroes = arr.filter(item => item === 0)
  if (zeroes.length != 0 || zeroes != null) {
    arr = arr.filter(e => e !== 0)
    return arr.concat(zeroes)
  } else {
    return arr
  }
}

// Refactored
let moveZeros = function (array) {
  // loop in reverse and delete up zeros and push them to the end
  for (var i = array.length - 1; i >= 0; i--) {
    if (array[i] === 0 || array[i] === "0") {
      array.splice(i, 1)
      array.push(0)
    }
  }
  return array
}

// First try
var moveZeros = function (arr) {
  let zeros = []
  let result = []

  if (arr.length === 0) {
    return arr
  }
  for (i = 0; i < arr.length; i++) {
    if (arr[i] === 0 || arr[i] === "0") {
      zeros.push(arr[i])
    } else(
      result.push(arr[i])
    )
  }
  return result.concat(zeros)
}

console.log(moveZeros([false, 1, 0, 1, 2, 0, 1, 3, "a"])) // returns[false,1,1,2,1,3,"a",0,0]

// other solutions
var moveZeros = function (arr) {
  return arr.filter(function (x) {
    return x !== 0
  }).concat(arr.filter(function (x) {
    return x === 0;
  }));
}

var moveZeros = function (arr) {
  var filtedList = arr.filter(function (num) {
    return num !== 0;
  });
  var zeroList = arr.filter(function (num) {
    return num === 0;
  });
  return filtedList.concat(zeroList);
}

var moveZeros = function (arr) {
  let result = arr.filter(c => c !== 0)
  let count = arr.length - result.length

  return result.concat(Array(count).fill(0))
}