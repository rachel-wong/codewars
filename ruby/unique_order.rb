# Implement the function unique_in_order which takes as argument a sequence and returns a list of items without any elements with the same value next to each other and preserving the original order of elements.

def unique_in_order(input)
    list = input.chars
    index = 0

    while index < list.length
        if list[index] == index[index + 1]
            list.delete_at(index + 1)
            index += 1
        else
            index += 1
        end
    end
    p list
end
## TESTS
 unique_in_order('AAAABBBCCDAABBB') #== ['A', 'B', 'C', 'D', 'A', 'B']
# unique_in_order('ABBCcAD')         #== ['A', 'B', 'C', 'c', 'A', 'D']
# unique_in_order([1,2,2,3,3])       #== [1,2,3]
