function miniMaxSum(arr) {
  let min = 0;
  let max = 0;
  arr = arr.sort((a,b) => a - b)
  for (let i = 0; i < arr.length; i++) {
    if (i < 4) {
      min = min + arr[i];
    }
    if (i > 0 && i < 5) {
      max += arr[i];
    }
  }
  console.log(`${min} ${max}`);
}

miniMaxSum([7 ,69, 2 ,221 ,8974])