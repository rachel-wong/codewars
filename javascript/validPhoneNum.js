// Write a function that accepts a string, and returns true if it is in the form of a phone number. 
// Assume that any integer from 0-9 in any of the spots will produce a valid phone number.

// Only worry about the following format:
// (123) 456-7890 (don't forget the space after the close parentheses) 

function validPhoneNumber(string) {
  let regex = new RegExp(/^(?:\d+|\(\d+\) \d+-\d+)$/)
  // let regexp = /([\b\d{3}\b]) [\b\d{3}\b]-[\b\d{4}\b]/
  return regex.test(string)
}

console.log(validPhoneNumber("(123) 456-7890")) //=> returns true
console.log(validPhoneNumber("(1111)555 2345")) //=> returns false
console.log(validPhoneNumber("(098) 123 4567")) //=> returns false

// other solutions
function validPhoneNumber(phoneNumber) {
  return /^\(\d{3}\) \d{3}-\d{4}$/.test(phoneNumber);
}

function validPhoneNumber(phoneNumber) {
  var regex = /^(\(...\))\s(...)-(....)$/;
  return regex.test(phoneNumber);
}

function validPhoneNumber(str) {

  if (str.length !== 14) {
    return false;
  }
  if (str[4] !== ")") {
    return false;
  }
  if (str[5] !== " ") {
    return false;
  }
  if (str[9] !== "-") {
    return false;
  } else {
    return true;
  }
}