// Complete the method/function so that it converts dash/underscore delimited words into camel casing. The first word within the output should be capitalized only if the original word was capitalized (known as Upper Camel Case, also often referred to as Pascal case).

// 1. find the _
// 2. make the next char after the _ a uppercase
// 3. remove the _
// 4. return .join("") everything

function toCamelCase(string) {
  let stringArr = [...string]
  for (i = 0; i < stringArr.length; i++) {
    if (stringArr[i] == "_" || stringArr[i] == "-") {
      stringArr[i + 1] = stringArr[i + 1].toUpperCase()
      stringArr[i] = ""
    }
  }
  return stringArr.join("")
}

// other solutions
function toCamelCase(str) {
  var regExp = /[-_]\w/ig;
  return str.replace(regExp, function (match) {
    return match.charAt(1).toUpperCase();
  });
}

function toCamelCase(str) {
  return str.replace(/[-_](.)/g, (_, c) => c.toUpperCase());
}