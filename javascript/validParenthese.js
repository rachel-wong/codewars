// Write a function called that takes a string of parentheses, and determines if the order of the parentheses is valid. The function should return true if the string is valid, and false if it's invalid.

// "()"              =>  true
// ")(()))"          =>  false
// "("               =>  false
// "(())((()())())"  =>  true

function validParentheses(parens) {
  if (parens === null || parens.length <= 0) return true // error handling
  let check = parens.split('')
  let count = []
  // no need for for (let i = 0; i < check.length; i++) because we don't to count the indexing here & u don't need to control direction flow
  for (let char of check) {
    if (char === '[') {
      count.push(']')
      console.log('count1:', count)
    } else if (char === '{') {
      count.push('}')
      console.log('count2:', count)
    } else if (char === '(') {
      count.push(')')
      console.log('count3:', count)
    } else if (count.length === 0 || char != count.pop()) {
      console.log("check4:", check)
      console.log("count4:", count)
      return false
    }
  }
  console.log("final count", count)
  if (count.length === 0) {
    return true
  } else {
    return false
  }
}

// console.log(validParentheses("(())((()())())")) // => true
// console.log(validParentheses(")(()))")) // => false
// console.log(validParentheses("()")) // => true
console.log(validParentheses(")(")) // => true
console.log(validParentheses("()(")) // => true

// first try
function earlyValidParentheses(parens) {
  parens = parens.split("")
  let currentCount = [] // count up every opener for its closing partner
  for (let i = 0; i < parens.length; i++) {
    if (parens[i] == '(')
      currentCount.push('(')
    else {
      let temp = currentCount.pop()
      if (temp != '(') {
        return false
      }
    }
  }
  if (currentCount.length === 0) {
    return true
  } else {
    return false
  }

}

console.log(earlyValidParentheses())