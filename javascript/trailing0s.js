function trailingZeros(n) {
  let count = 0
  for (let i = 5; n / i >= 1; i *= 5) {
    count += n / i
  }
  return Math.floor(count)
}

console.log(trailingZeros(0)) //=> 0
console.log(trailingZeros(30)) //=> 7