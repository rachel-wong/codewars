// You get an array of numbers, return the sum of all of the positives ones.
// Example[1, -4, 7, 12] => 1 + 7 + 12 = 20
// Note: if there is nothing to sum, the sum is default to 0.

function positiveSum(arr) {
  if (arr === [] || arr === "" || arr === null) return 0
  let result = 0
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      result += arr[i]
    }
  }
  return result
}


positiveSum([1, 2, 3, 4, 5]) //=> 15
positiveSum([1, -2, 3, 4, 5]) // => 13
positiveSum([]) // => 0
positiveSum([-1, -2, -3, -4, -5]) // => 0
positiveSum([-1, 2, 3, 4, -5]) // => 9

// other solutions

function positiveSum(arr) {
  return arr.reduce((a, b) => a + (b > 0 ? b : 0), 0);
}

function positiveSum(arr) {
  return arr.filter(x => x >= 0).reduce((a, c) => a + c, 0);
}