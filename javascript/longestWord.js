// Given a string and a string dictionary, find the longest string in the dictionary that can be formed by deleting some characters of the given string.

// If there are more than one possible results, return the longest word with the smallest lexicographical order. If there is no possible result, return the empty string.

// All the strings in the input will only contain lower-case letters.
// The size of the dictionary won 't exceed 1,000.
// The length of all the strings in the input won 't exceed 1,000.
// Performance should be on your mind

// what is the word in dictionary that a full subset of the target (and it is also the maximum string.length in the array)

function findLongestWord(target, dictionary) {
  let matches = []
  target = target.split("").sort().join("")
  let check = dictionary.forEach(word => {
    return word = word.split("").sort().join("")
  })
  console.log(check)
}

console.log(findLongestWord("abpcplea", ["ale", "apple", "monkey", "plea"]))
// => "apple" 

console.log(findLongestWord("abpcplea", ["a", "b", "c"]))
// => "a"

// unsuccessful
function firstFindLongestWord(target, dictionary) {
  let match = []
  target = target.split("").sort().join("")
  console.log('target:', target)
  let check = dictionary.map(word => word.split("").sort().join(""))
  console.log('dictionary:', dictionary)
  console.log('check:', check)
  let temp = []
  for (let i = 0; i < check.length; i++) {
    if (check[i].includes(target) == true) {
      temp.push(dictionary[i])
    }
  }
  temp = temp.sort(function (a, b) {
    return b.length - a.length
  })
  console.log('temp:', temp)

  return temp[0]
}