// Write a function that accepts an array of 10 integers(between 0 and 9), that returns a string of those numbers in the form of a phone number.

// createPhoneNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]) // => returns "(123) 456-7890"

// first attempt
// function createPhoneNumber(array) {
//   let areaCode = array.slice(0, 3)
//   let first = array.slice(3, 6)
//   let last = array.slice(6, 10)
//   return `(${areaCode.join("")}) ${first.join("")}-${last.join("")}`
// }

// second attempt
function createPhoneNumber(numbers) {
  return (numbers.length != 10 || numbers.every(isNumber) == false) ? false : `(${numbers.slice(0, 3).join("")}) ${numbers.slice(3, 6).join("")}-${numbers.slice(6).join("")}`
}

function isNumber(int) {
  return int >= 0 && int <= 9
}

console.log(createPhoneNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]))

// other solutions
function createPhoneNumber(numbers) {
  numbers = numbers.join('');
  return '(' + numbers.substring(0, 3) + ') ' +
    numbers.substring(3, 6) +
    '-' +
    numbers.substring(6);
}

function createPhoneNumber(numbers) {
  var format = "(xxx) xxx-xxxx";

  for (var i = 0; i < numbers.length; i++) {
    format = format.replace('x', numbers[i]);
  }

  return format;
}

function createPhoneNumber(numbers) {
  return numbers.join('').replace(/(...)(...)(.*)/, '($1) $2-$3');
}