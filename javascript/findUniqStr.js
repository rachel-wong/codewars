// There is an array of strings. All strings contains similar letters except one. Try to find it!
// Strings may contain spaces. Spaces is not significant, only non-spaces symbols matters. E.g. string that contains only spaces is like empty string.
// It’ s guaranteed that array contains more than 3 strings.

function findUniq(array) {
  let check = array.map(word => {
    word = word.toLowerCase()
    return [...new Set(word)].sort().join("")
  })

  for (i = 0; i < check.length; i++) {
    check[i] = check[i].split('').filter((char, indexPosition, array) => {
      return array.indexOf(char) === indexPosition;
    }).sort().join('')
  }

  for (let x = 0; x < check.length; x++) {
    if (check.indexOf(check[x]) == check.lastIndexOf(check[x])) {
      return array[x] // map the position of the unique back to the original array
    }
  }
}

// unsuccessful - doesn't find the first element
function firstFindUniq(array) {
  let check = array.map(word => {
    word = word.toLowerCase()
    return [...new Set(word)].sort().join("")
  })
  check.forEach((item, index, src) => {
    if (check.indexOf(item) === check.lastIndexOf(item)) {
      return array[index]
    }
  })
}

console.log(findUniq(['Aa', 'aaa', 'aaaaa', 'BbBb', 'Aaaa', 'AaAaAa', 'a'])) // === 'BbBb'
console.log(findUniq(['abc', 'acb', 'bac', 'foo', 'bca', 'cab', 'cba'])) // === 'foo'

// other solutions
function findUniq(arr) {
  let [a, b, c] = arr.slice(0, 3)

  if (!similar(a, b) && !similar(a, c)) return a
  for (d of arr)
    if (!similar(a, d)) return d
}

function similar(x, y) {
  x = new Set(x.toLowerCase())
  y = new Set(y.toLowerCase())

  if (x.size !== y.size) return false
  for (z of x)
    if (!y.has(z)) return false

  return true
}


const unique = (x, i, ar) => ar.indexOf(x) === ar.lastIndexOf(x);
const getUniques = x => [...new Set([...x.toLowerCase()].sort())].join('');
const findUniq = arr => arr[arr.map(getUniques).findIndex(unique)];

function findUniq(arr) {
  var word = [];
  var res = ""
  var uniq = [...new Set(arr.join("").toLowerCase())].map(n => (word = arr.filter(v => v.indexOf(n) != -1), word.length == 1) ? res = word.join("") : n);
  return res
}

function findUniq(arr) {
  var tmp = arr.map(el => Array.from(new Set(el.toLowerCase().split(''))).sort().join('')),
    str = tmp[0] == tmp[1] ? tmp[0] : tmp[2];
  for (var i = 0; i < arr.length; i++)
    if (tmp[i] != str) return arr[i]
}

function findUniq(arr) {

  let alphabet = word => [...new Set(word.toLowerCase())]
    .sort()
    .join("")
    .trim();
  let alphabet0 = alphabet(arr[0]);
  let alphabet1 = alphabet(arr[1]);

  if (alphabet0 === alphabet1) return arr.find(word => alphabet(word) !== alphabet0);
  if (alphabet0 === alphabet(arr[2])) return arr[1];
  return arr[0];
}

function findUniq(arr) {
  var hashes = arr.map(str => generateHash(str));
  var hash = findUniqEqual(hashes);

  return arr[hashes.indexOf(hash)];
}

function findUniqEqual(arr) {
  var uniq = null;
  var a = Array.from(arr);

  uniq = a.pop();
  if (a.indexOf(uniq) >= 0) {
    uniq = a.filter(i => i != uniq).pop();
  }

  return uniq;
}

function generateHash(str) {
  return str
    .replace(' ', '')
    .toLowerCase()
    .split('')
    .sort()
    .filter((c, i, a) => a.indexOf(c, i + 1) < 0)
    .join('');
}