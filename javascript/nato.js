// You'll have to translate a string to Pilot's alphabet (NATO phonetic alphabet) wiki.

// ** Input: ** If you can read

// ** Output: ** India Foxtrot Yankee Oscar Uniform Charlie Alfa November Romeo Echo Alfa Delta

// Keep the punctuation, and remove the spaces.
// Use Xray without dash or space.


function to_nato(string) {
  let alphabet = [..."abcdefghijklmnopqrstuvwxyz"]
  let nato = "Alfa, Bravo, Charlie, Delta, Echo, Foxtrot, Golf, Hotel, India, Juliett, Kilo, Lima, Mike, November, Oscar, Papa, Quebec, Romeo, Sierra, Tango, Uniform, Victor, Whiskey, Xray, Yankee, Zulu".split(",").map(char => char.trim())
  let onlyChar = /^[a-zA-z]+$/i
  string = string.split("").filter(char => char != " ").map(char => char.toLowerCase())
  for (let i = 0; i < string.length; i++) {
    if (onlyChar.test(string[i]) == true) {
      let index = alphabet.indexOf(string[i])
      string[i] = nato[index]
    }
  }
  return string.join(" ").trim()
}

// other solutions
let table = {
  'A': 'Alfa',
  'B': 'Bravo',
  'C': 'Charlie',
  'D': 'Delta',
  'E': 'Echo',
  'F': 'Foxtrot',
  'G': 'Golf',
  'H': 'Hotel',
  'I': 'India',
  'J': 'Juliett',
  'K': 'Kilo',
  'L': 'Lima',
  'M': 'Mike',
  'N': 'November',
  'O': 'Oscar',
  'P': 'Papa',
  'Q': 'Quebec',
  'R': 'Romeo',
  'S': 'Sierra',
  'T': 'Tango',
  'U': 'Uniform',
  'V': 'Victor',
  'W': 'Whiskey',
  'X': 'Xray',
  'Y': 'Yankee',
  'Z': 'Zulu',
}

function to_nato(words) {
  return words.split('').filter(c => c !== ' ').map(c => table[c.toUpperCase()] || c).join(' ');
}

function to_nato(words) {
  var translate = {
    a: "Alfa ",
    b: "Bravo ",
    c: "Charlie ",
    d: "Delta ",
    e: "Echo ",
    f: "Foxtrot ",
    g: "Golf ",
    h: "Hotel ",
    i: "India ",
    j: "Juliett ",
    k: "Kilo ",
    l: "Lima ",
    m: "Mike ",
    n: "November ",
    o: "Oscar ",
    p: "Papa ",
    q: "Quebec ",
    r: "Romeo ",
    s: "Sierra ",
    t: "Tango ",
    u: "Uniform ",
    v: "Victor ",
    w: "Whiskey ",
    x: "Xray ",
    y: "Yankee ",
    z: "Zulu ",
    "?": "? ",
    ".": ". ",
    " ": "",
    "!": "! "
  };

  return words.toLowerCase().split("").map(char => translate[char]).join("").trim();
}

function to_nato(words) {
  return words
    .replace(/[A-Z]\s*/ig, match => " " + nato[match[0].toLowerCase()] + " ")
    .replace(/\s\s+/g, " ")
    .trim()
}

const nato = {
  a: "Alfa",
  b: "Bravo",
  c: "Charlie",
  d: "Delta",
  e: "Echo",
  f: "Foxtrot",
  g: "Golf",
  h: "Hotel",
  i: "India",
  j: "Juliett",
  k: "Kilo",
  l: "Lima",
  m: "Mike",
  n: "November",
  o: "Oscar",
  p: "Papa",
  q: "Quebec",
  r: "Romeo",
  s: "Sierra",
  t: "Tango",
  u: "Uniform",
  v: "Victor",
  w: "Whiskey",
  x: "Xray",
  y: "Yankee",
  z: "Zulu",
}