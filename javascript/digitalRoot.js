function digital_root(int) {
  let result = 0
  if (int.toString().length == 1) {
    return parseInt(int)
  } else {
    int.toString().split("").forEach(value => {
      return result += parseInt(value)
    })
    return digital_root(result)
  }
}

function digital_root(n) {
  if (n.toString().length === 1) {
    return n
  } else {
    n = n.toString().split("").map(x => parseInt(x)).reduce((acc, cur) => {
      return acc + cur
    })
    return digital_root(n)
  }
}

// other solutions
function digital_root(n) {
  if (n < 10)
    return n;

  for (var sum = 0, i = 0, n = String(n); i < n.length; i++)
    sum += Number(n[i]);

  return digital_root(sum);
}
