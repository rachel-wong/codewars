function checkExam(answers, responses) {
  let score = 0
  for (let i = 0; i < responses.length; i++) {
    if (responses[i] === "") {
      score += 0
    } else if (responses[i] != answers[i]) {
      score -= 1
    } else if (responses[i] === answers[i]) {
      score += 4
    }
  }
  return score < 0 ? 0 : score
}

console.log(checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"])) // → 6
console.log(checkExam(["a", "a", "c", "b"], ["a", "a", "b", ""])) //→ 7
console.log(checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"])) //→ 16
console.log(checkExam(["b", "c", "b", "a"], ["", "a", "a", "c"])) //→ 0

// other solutions
checkExam = (x, y) => (x = y.reduce((s, e, i) => s + (e === x[i] ? 4 : e === '' ? 0 : -1), 0)) > 0 ? x : 0;

function checkExam(a1, a2) {
  return Math.max(0, a2.reduce((s, a, i) => s + (a ? a === a1[i] ? 4 : -1 : 0), 0));
}