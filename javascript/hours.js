// Write a function, which takes a non - negative integer(seconds) as input and returns the time in a human - readable format(HH: MM: SS)

// HH = hours, padded to 2 digits, range: 00 - 99
// MM = minutes, padded to 2 digits, range: 00 - 59
// SS = seconds, padded to 2 digits, range: 00 - 59
// The maximum time never exceeds 359999(99: 59: 59)

function hours(theSeconds) {
	let hours = Math.floor(theSeconds / 3600)
		.toString()
		.padStart(2, "0")
	let minutes = (Math.floor(theSeconds / 60) % 60).toString().padStart(2, "0")
	let seconds = (theSeconds % 60).toString().padStart(2, "0")
	return `${hours}:${minutes}:${seconds}`
}

console.log(hours(685))

// Other Solutions
// function humanReadable(seconds) {
//   var pad = function (x) {
//     return (x < 10) ? "0" + x : x;
//   }
//   return pad(parseInt(seconds / (60 * 60))) + ":" +
//     pad(parseInt(seconds / 60 % 60)) + ":" +
//     pad(seconds % 60)
// }
