#Count the number of occurrences of each character and return it as a list of tuples in order of appearance.

def ordered_count(str)
    array = str.split('')
    hash = {}
    new_array = []
    for char in array do
        if hash.key?(char)
            hash[char] += 1
        else
            hash[char] = 1
        end
    end
    hash.each {|key, value|
        tuple = []
        tuple << key
        tuple << value
        new_array << tuple
    }
    p new_array
end

## OTHER SOLUTIONS ##

def ordered_count(str)
    s = str.chars.uniq
    s.map { |x| [x, str.chars.count(x)] }  
end

def ordered_count(str)
    str.chars.uniq.map{|i| [i, str.count(i)]}
end

def ordered_count(str)
    str.chars.each_with_object(Hash.new(0)) { |c, hash| hash[c]+=1 }.to_a
  end

  def ordered_count(str)
    arr = str.split('')
    arr.uniq.map { |s| [s] << arr.count(s)}
  end

## TEST CASES
# ordered_count("abracadabra") # => [['a', 5], ['b', 2], ['r', 2], ['c', 1], ['d', 1]])
ordered_count("Code Wars") #=> [['C', 1], ['o', 1], ['d', 1], ['e', 1], [' ', 1], ['W', 1], ['a', 1], ['r', 1], ['s', 1]])