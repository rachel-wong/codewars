// write a function that checks if two provided strings are anagrams of each other; letter casing shouldn’t matter. Also, consider only characters, not spaces or punctuation.

function isAnagram(str1, str2) {
  return sortStr(str1) === sortStr(str2) ? true : false
}

// helper
function sortStr(string) {
  return string.toLowerCase().replace(/[^a-z]+/g, "").split("").filter(item => item != " ").sort((a, b) => a > b).join("")
}

console.log(isAnagram("finder", "Friend")) // => true
console.log(isAnagram("hello", "bye")) // => false