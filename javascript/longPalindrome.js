// A palindrome is a series of characters that read the same forwards as backwards such as "hannah", "racecar" and "lol".

// For this Kata you need to write a function that takes a string of characters and returns the length, as an integer value, of longest alphanumeric palindrome that could be made by combining the characters in any order but using each character only once. The function should not be case sensitive.

// For example if passed "Hannah" it should return 6 and if passed "aabbcc_yYx_" it should return 9 because one possible palindrome would be "abcyxycba".

// unsuccessful
function longPalindrome(string) {
  if (string === null || typeof string != "string") return false // silly error handling
  console.log("before", string.length)
  string = string.replace(/[^0-9a-zA-Z]/gi, '').split("").map(char => char.toLowerCase())
  console.log('string:', string.length, string.join(""))
  let count = {}
  let len = 0
  for (let i = 0; i < string.length; i++) {
    count[string[i]] = (count[string[i]] || 0) + 1
  }
  Object.values(count).forEach(val => {
    if (val >= 2) {
      len += Math.floor(val / 2) * 2
    }
    if (val == 1) {
      len += 1
    }
  })
  console.log(count)
  return len
}

// console.log(longPalindrome("aabbcc_yYx_")) // => 9 "abcyxycba"
// console.log(longPalindrome("Hannah")) //=> 
// console.log(longPalindrome("xyz__a_/b0110//a_zyx")) // => 13
console.log(longPalindrome("$aaabbbccddd_!jJpqlQx_.///yYabababhii_")) // => 28 (couldn't get past this one)