// Given a string of words, you need to find the highest scoring word.
// Each letter of a word scores points according to its position in the alphabet: a = 1, b = 2, c = 3 etc.
// You need to return the highest scoring word as a string.
// If two words score the same, return the word that appears earliest in the original string.
// All letters will be lowercase and all inputs will be valid.

function high(x) {
  let scorekeeper = {}
  let words = x.split(" ")
  // for each word, calculate score and add it to the scorekeeper hash
  for (let i = 0; i < words.length; i++) {
    let chars = words[i].split("")
    let score = 0
    chars.forEach(char => {
      score += getScore(char)
    })
    scorekeeper[words[i]] = score
  }
  // this won't work because it won't distinguish between the keys of more than two maximum values
  // return Object.keys(scorekeeper).reduce((a, b) => scorekeeper[a] > scorekeeper[b] ? a : b)

  return Object.keys(scorekeeper).filter(x => {
    return scorekeeper[x] == Math.max.apply(null, Object.values(scorekeeper))
  })[0] //the [0] here returns the first key that has the maximum, otherwise it'll return all the keys
}

// Helper function
function getScore(char) {
  let alphabet = [..."abcdefghijklmnopqrstuvwxyz"]
  return alphabet.indexOf(char) + 1
}

console.log(high("man i need a taxi up to ubud")) //=> taxi
console.log(high('take me to semynak')) //=> semynak
console.log(high("jmdddlhbnnqzpejbadoyjpfaysg ebupsdsjyznajnvbqgmlncxwzyp tsuaiuwxoxpwmkyzcsegrunfghp mlpvkttpjhwxvplglnedfuutoux")) //=> tsuaiuwxoxpwmkyzcsegrunfghp

// other solution
function high(x) {
  //transform the input string into array & define a string of alphabetical latin characters
  var arr = x.split(' ');
  var str = 'abcdefghijklmnopqrstuvwxyz';
  //Iterate through the array with input words to find the one with the greatest sum
  var newArr = arr.map(function (word) {
    var sum = 0;
    for (var i = 0; i < word.length; i++) {
      sum += str.indexOf(word[i]);
    }
    return sum;
  });
  //Return the word with the greatest sum
  return arr[newArr.indexOf(Math.max(...newArr))];
}

// other solution
function high(s) {
  let as = s.split(' ').map(s => [...s].reduce((a, b) => a + b.charCodeAt(0) - 96, 0));
  return s.split(' ')[as.indexOf(Math.max(...as))];
}

// other solution
function high(x) {
  return x.split(' ').reduce((accum, current) => {
    return score(current) > score(accum) ? current : accum;
  })
}

function score(word) {
  return word.split('').reduce((accum, current) => {
    return accum + (current.charCodeAt() - 96)
  }, 0)
}