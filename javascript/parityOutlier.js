
function parityOutlier(arr) {
  let odds = arr.filter(char => { return char % 2 != 0 })
  if (odds.length === 1) {
    return odds[0]
  } else {
    return arr.filter(char => { return char % 2 === 0})[0]
  }
}

// Other solutions
function findOutlier(int){
  var even = int.filter(a=>a%2==0);
  var odd = int.filter(a=>a%2!==0);
  return even.length==1? even[0] : odd[0];
}
