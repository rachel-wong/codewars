// given a string, return true if the string is a palindrome and false if it isn’t. Include spaces and punctuation in deciding if the string is a palindrome

function isPalindrome(string) {
  return string.toLowerCase() === string.split("").reverse().join("").toLowerCase() ? true : false
}

console.log(isPalindrome("racecar")) // => true
console.log(isPalindrome("table")) // => false