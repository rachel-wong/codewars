// [1,4,4,4,0,4,3,3,1] # should return [1,12,0,4,6,1]

function sumNext(array) {
  let result = []
  let temp = 0
  for (let i = 0; i < array.length; i++) {
    if (array[i] != array[i + 1]) {
      result.push(temp + array[i])
      temp = 0 //reset temp
    } else if (array[i] === array[i + 1]) {
      temp = temp + array[i]
    }
  }
  return result
}

console.log(sumNext([1, 4, 4, 4, 0, 4, 3, 3, 1]))

// other solution
function sumConsecutives(s) {
  return s.reduce(function (prev, curr, i, arr) {
    if (curr != arr[i - 1]) prev.push(curr);
    else prev[prev.length - 1] += curr;
    return prev;
  }, []);
}