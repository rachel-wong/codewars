def stray(numbers)
    key = numbers.uniq
    for item in key do
        if numbers.count(item) == 1
            p item
        end
    end
end

stray([1, 1, 2]) #==> 2z
# find_stray([17, 17, 3, 17, 17, 17, 17]) #==> 3