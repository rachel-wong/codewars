function consonants(string) {
  let alphabet = [..."abcdefghijklmnopqrstuvwxyz"]
  string = string.replace(/[aeiou]/gi, ' ').split(" ").filter(item => item != "")
  let score = []
  for (let i = 0; i < string.length; i++) {
    score.push(string[i].split("").reduce((a, b) => {
      return a += (alphabet.indexOf(b) + 1) // count starts from 1 not 0
    }, 0))
  }
  return Math.max.apply(Math, score)
}

console.log(consonants("strength")) // => 57
console.log(consonants("zodiac")) // => 26

// Messing around with regex for filtering around vowels
let noVowelRegex = /[^aeiou]$/ig
let onlyVowel = /[aeiou]+/ //=> strings with only vowels
let noVowel = /.[^aeiou]+/ //=> strings without any vowels
let vowelOthers = /.*?[aeiou]+?.*?/ // => strings containing vowels and other characters

// other solutions
const solve = s => s.split(/[aeiou]+/).reduce((s, n) => Math.max(s, n.split('').reduce((a, b) => a + b.charCodeAt(0) - 96, 0)), 0);

function solve(s) {
  return Math.max(...s.match(/[^aeiou]+/g).map(x => [...x].reduce((s, v) => s + v.charCodeAt() - 96, 0)))
}

const solve = (s) =>
  s.split(/[aeiou]+/).map((ss) =>
    Array.from(ss).map((ch) =>
      ch.charCodeAt() - 96
    ).reduce((a, b) => a + b, 0)
  ).reduce((a, b) => a > b ? a : b);

const solve = s => Math.max(
  ...s.split(/[aeiou]/g)
  .filter(e => e)
  .map(e => e.split ``.reduce((r, e) => r + e.charCodeAt() - 96, 0))
);