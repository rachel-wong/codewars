function validate(n) {
  n = n.toString().split("").map(item => parseInt(item))
  if (n.length % 2 != 0) {
    n = n.map((item, idx) => idx % 2 != 0 ? item = item * 2 : item)
  } else {
    n = n.map((item, idx) => idx % 2 == 0 ? item = item * 2: item)
  }
  n = n.map(item => item > 9 ? item = item - 9 : item).reduce((a, b) => { return a + b }, 0)

  return n % 10 == 0
}

console.log(validate(2121))

// other solutions
function validate(n) {
  n = n.toString().split('').map(Number).reverse();
  return n.reduce(function (sum, digit, index) {
    if (index & 1) digit <<= 1;
    if (digit > 9) digit -= 9;
    return sum + digit;
  }, 0) % 10 == 0;
}