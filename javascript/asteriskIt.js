function asteriskIt(input) { q
  let array = input.toString().split("").map(Number)
  let result = []
  for (let i = 0; i < array.length; i++) {
    if (array[i] % 2 === 0 && array[i + 1] % 2 === 0) {
      result.push(array[i], "*")
    } else if (array[i] % 2 === 0 && array[i + 1] % 2 != 0) {
      result.push(array[i])
    } else if (array[i] % 2 != 0) {
      result.push(array[i])
    }
  }
  return result.join("")
}
console.log(asteriskIt(5312708)) // => '531270*8'