// John has invited some friends. His list is:

// s = "Fred:Corwill;Wilfred:Corwill;Barney:Tornbull;Betty:Tornbull;Bjon:Tornbull;Raphael:Corwill;Alfred:Corwill";

// makes this string uppercase
// gives it sorted in alphabetical order by last name.
// When the last names are the same, sort them by first name. Last name and first name of a guest come in the result between parentheses separated by a comma.

// "(CORWILL, ALFRED)(CORWILL, FRED)(CORWILL, RAPHAEL)(CORWILL, WILFRED)(TORNBULL, BARNEY)(TORNBULL, BETTY)(TORNBULL, BJON)"

// It can happen that in two distinct families with the same family name two people have the same first name too.

// overthinking, had help
function meeting(s) {
  s = s.toUpperCase().split(";")
  let people = s.map(person => person.split(":").reverse().join(", ")) // an array with the right format
  people = people.sort().join(")(")
  console.log('people:', people)
  return "(" + people + ")"
}

// let meeting = s => '(' + s.toUpperCase().split(';').map(x => x.split(':').reverse().join(', ')).sort().join(')(') + ')';

console.log(meeting("Fred:Corwill;Wilfred:Corwill;Barney:Tornbull;Betty:Tornbull;Bjon:Tornbull;Raphael:Corwill;Alfred:Corwill"))

// // other solutions
const meeting = s => {
  return s.split(';').map(pair => {
    [name, surname] = pair.toUpperCase().split(':');
    return `(${surname}, ${name})`;
  }).sort((a, b) => a.localeCompare(b)).join('');
};

const formatName = name => ([last, first] = name.toUpperCase().split('_'), `(${last}, ${first})`)
const structureName = name => ([first, last] = name.toLowerCase().split(":"), `${last}_${first}`)

const meeting = s => s.split(';').map(structureName).sort().map(formatName).join('')


function meeting(s) {
  let result = '';
  s.split(';').map((x) => {
    return x.split(':').reverse().join(', ').toUpperCase();
  }).sort().map(name => result += `(${name})`);
  return result;
}