// The main idea is to count all the occuring characters(UTF-8) in string. If you have string like this aba then the result should be { 'a': 2, 'b': 1 }

// What if the string is empty ? Then the result should be empty object literal {}

function countChar(string) {
  let check = string.split("")
  let count = {}
  for (i = 0; i < check.length; i++) {
    count[check[i]] = (count[check[i]] || 0) + 1
  }
  return count
}

console.log(countChar("aba"))
console.log(countChar(""))

// other solutions
function count(string) {
  var count = {};
  string.split('').forEach(function (s) {
    count[s] ? count[s]++ : count[s] = 1;
  });
  return count;
}

function count(string) {
  return string.split('').reduce(function (counts, char) {
    counts[char] = (counts[char] || 0) + 1;
    return counts;
  }, {});
}