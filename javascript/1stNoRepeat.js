// Write a function named first_non_repeating_letter that takes a string input, and returns the first character that is not repeated anywhere in the string.

// For example, if given the input 'stress', the function should return 't', since the letter t only occurs once in the string, and occurs first in the string.

// As an added challenge, upper - and lowercase letters are considered the same character, but the function should return the correct case for the initial letter.For example, the input 'sTreSS' should return 'T'.

// If a string contains all repeating characters, it should return an empty string("") or None--see sample tests.

function firstNonRepeatingLetter(s) {
	let original = [...s]
	let check = [] // create a copy that is all lowercase
	if (s == "") {
		return ""
	}
	for (i = 0; i < original.length; i++) {
		check.push(original[i].toLowerCase())
	}

	for (let x = 0; x < check.length; x++) {
		if (check.indexOf(check[x]) == check.lastIndexOf(check[x])) {
			return original[x] // find position of the unique and map it back to original for case
		}
	}
	return "" // returns for all repeating strings abba
}

// other solutions

function firstNonRepeatingLetter(s) {
	for (var i in s) {
		if (s.match(new RegExp(s[i], "gi")).length === 1) {
			return s[i]
		}
	}
	return ""
}

function firstNonRepeatingLetter(s) {
	var t = s.toLowerCase()
	for (var x = 0; x < t.length; x++)
		if (t.indexOf(t[x]) === t.lastIndexOf(t[x])) return s[x]
	return ""
}