// John is a typist. He has a habit of typing: he never use the Shift key to switch case, just using only Caps Lock.
// Given a string s. Your task is to count how many times the keyboard has been tapped by John.
// You can assume that, at the beginning the Caps Lock light is not lit.

function typist(s) {
  let capsLock = false
  let keystroke = 0
  let check = s.split("")
  for (let i = 0; i < check.length; i++) {
    // by default, every time there is a char, there is a keystroke
    keystroke++

    // now to work out whether you needed to press the capslock key or not
    // if char is uppercase
    if (check[i] === check[i].toUpperCase()) {
      if (capsLock === false) {
        capsLock = true
        keystroke++
      }
    }
    // if char is lowercase
    else {
      if (capsLock === true) {
        capsLock = false
        keystroke++
      }
    }
  }
  return keystroke
}

console.log(typist("AmericanRAILWAY")) //=> 18

// other solutions
function typist(s) {

  let count = regex => (`a${s}`.match(regex) || []).length;
  return s.length + count(/[a-z][A-Z]/g) + count(/[A-Z][a-z]/g);

}

function typist(s) {
  let caps = false
  let hits = 0
  for (let i = 0; i < s.length; ++i) {
    const upper = s.charCodeAt(i) < 97
    if (!caps && upper || caps && !upper) {
      caps = !caps
      hits++
    }
  }
  return s.length + hits
}