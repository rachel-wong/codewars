// Soundex is an interesting phonetic algorithm developed nearly 100 years ago for indexing names as they are pronounced in English. 
// The goal is for homophones to be encoded to the same representation so that they can be matched despite minor differences in spelling.

// Save the first letter. 
// Remove all occurrences of h and w except first letter.
// Replace all consonants (include the first letter) with digits as follows:
// b, f, p, v = 1
// c, g, j, k, q, s, x, z = 2
// d, t = 3
// l = 4
// m, n = 5
// r = 6
// Replace all adjacent same digits with one digit.
// Remove all occurrences of a, e, i, o, u, y except first letter.
// If first symbol is a digit replace it with letter saved on step 1.
// Append 3 zeros if result contains less than 3 digits.
// Remove all except first letter and 3 digits after it

// Space separated string of equivalent Soundex codes 
// the first character of each code must be uppercase

// not working i will come back to u (づ￣ ³￣)づ
// the instructions are a bit wack. refer to wikipedia instead
function soundex(string) {
  let consonants = {}
  let vals = ["bfpv", "cgjkqsxz", "dt", "l", "mn", "r"]
  for (let i = 0; i < vals.length; i++) {
    vals[i].split("").forEach(char => {
      consonants[char] = 1 + i
    })
  }
  // console.log(consonants.b) // not sure why the for-loop propagated non-string keys in the object ¯\_(ツ)_/¯

  // convert everything to lowercase
  string = string.split(" ").map(word => {
    return word.toLowerCase()
  })
  for (let word of string) {
    // keep first char separate & uppercase
    let firstLetter = word.charAt(0).toUpperCase()
    let check = word
    for (let i = 0; i < check.length; i++) {
      // replace all instances of h,w,a,e,i,o,u,y
      check = check.replace(/[aeiouhw]/ig, "").split("")
      console.log('check:', check)

      // map needs a return whenever to change the value in the array
      // check = check.map(char => {
      //   if (consonants[char.toString()] != undefined) {
      //     return consonants[char.toString()]
      //   } else {
      //     return char
      //   }
      // })
    }
    console.log(firstLetter + check)
    }
  }

console.log(soundex("Sarah Connorw")) //=> S600 C560