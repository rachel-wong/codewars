// https://www.codewars.com/kata/55c04b4cc56a697bb0000048/train/javascript

// Complete the function scramble(str1, str2) that returns true if a portion of str1 characters can be rearranged to match str2, otherwise returns false.

// scramble('rkqodlw', 'world') == > True
// scramble('cedewaraaossoqqyt', 'codewars') == > True
// scramble('katas', 'steak') == > False

// Only lower case letters will be used (a-z). No punctuation or digits will be included.

function scramble(str1, str2) {
  let str1_arr = str1.split("").sort()
  let str2_arr = str2.split("").sort()
  let count = 0

  // str2 needs to be completely in str1
  for (i = 0; i < str2_arr.length; i++) {
    if (str1_arr.includes(str2_arr[i])) {
      count++
    }
  }
  return (count == str2_arr.length) ? true : false
}
console.log(scramble('jscripts', 'javascript')) // => false
console.log(scramble('aabbcamaomsccdd', 'commas')) // => true                                                                                   