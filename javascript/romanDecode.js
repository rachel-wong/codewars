function solution(roman) {
  const decoder = {
    'M': 1000,
    'D': 500,
    'C': 100,
    'L': 50,
    'X': 10,
    'V': 5,
    'I': 1
  }
  let array = roman.split("")
  let result = 0
  for (let i = 0; i < array.length; i++) {
    // if the first item's value is smaller than the second item's value 
    // add (second item's value - first item's value) to result
    if (decoder[array[i]] < decoder[array[i + 1]]) {
      result += decoder[array[i + 1]] - decoder[array[i]]
      i++ // go straight to the next item
    }
    // if the first item's value is LARGER THAN or EQUAL TO the second item's value
    // add the first item's value to result
    // go to second item
    else {
      result += decoder[array[i]]
      // this doesn't need to increment because once it is done, it drops out of the loop and increments
    }
  }
  return result
}

console.log(solution('XXI')) //21
console.log(solution('IV')) //4
console.log(solution('I')) //1
console.log(solution('MMVIII')) //2008
console.log(solution('MDCLXVI')) //1666

// regex verison which is too crazy 

function solution(roman) {
  var conversion = {
    M: 1000,
    CM: 900,
    D: 500,
    CD: 400,
    C: 100,
    XC: 90,
    L: 50,
    XL: 40,
    X: 10,
    IX: 9,
    V: 5,
    IV: 4,
    I: 1
  };

  return roman.match(/CM|CD|XC|XL|IX|IV|\w/g).reduce((accum, roman) => accum + conversion[roman], 0);
}