function diagonalmatrix(arr) {
  let first = []
  let second = []
  for (let i = 0; i < arr.length; i++) {
    first.push(arr[i][i])
  }

  arr = arr.reverse()

  for (let i = 0; i < arr.length; i++) {
    second.push(arr[i][i])
  }

  return Math.abs(first.reduce((a, b) => a + b) - second.reduce((a, b) => a+ b))
}

console.log(diagonalmatrix([[ 11, 2, 4 ], [ 4, 5, 6 ], [ 10, 8, -12 ]])) // => 15