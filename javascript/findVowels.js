// Write a function that takes a string as argument and returns the number of vowels contained in that string.

function findVowels(string) {
  let vowels = /[aeiou]/ig
  let arr = string.match(vowels)
  return arr ? arr.length : 0
}

console.log(findVowels('hello')) // --> 2
console.log(findVowels('why')) // --> 0