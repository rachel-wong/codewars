// Complete the method so that it formats the words into a single comma separated value. The last word should be separated by the word 'and' instead of a comma. The method takes in an array of strings and returns a single formatted string. Empty string values should be ignored. Empty arrays or null/nil values being passed into the method should result in an empty string being returned.

function formatWords(words) {
  if (words === null || words.length === 0) return "" // error handling 
  words = words.filter(word => word) // filter out space
  if (words.length === 1) return words[0] // only one word with no spaces
  
  let accumulator = [words[0]] // new array that adds on with changed items from words array (don't always have to .push it)

  // keep the first word from being changed. it doesn't need a comma
  for (let i = 1; i < words.length; i++) {
    // when iterated to second last word of array, add in 'and' to the array.
    // don't use splice because that changes the indexing of the array
    if (i === words.length - 1) {
      accumulator[i] = " and " + words[i]
    } 
    // for every word, change it to add a comma and space after it
    else {
      console.log('accumulator:', accumulator)
      accumulator[i] = ", " + words[i]
      console.log('accumulator:', accumulator)
    }
  }
  return accumulator.join('') // add it all back together without spaces
}
// first attempt
// function formatWords(array) {
//   // filter out empty items
//   // if array is empty, return ""
//   // insert and at indexPosition array.length - 1
//   // array.join("")
//   array = array.filter(item => item != "")
//   if (array === null) {
//     return ""
//   } else if (array.length >= 2) {
//     for (let i = 0; i < array.length - 1; i++) {
//       console.log(i)
//     }
//     // array.splice(array.length - 1, 0, "and")
//   }
//   console.log(array.join(" "))
// }

formatWords(['ninja', 'samurai', 'ronin']) // should return "ninja, samurai and ronin"
formatWords(['ninja', '', 'ronin']) // should return "ninja and ronin"
formatWords([]) // should return ""

// other solutions
function formatWords(words) {
  if (!words) return ''
  words = words.filter(v => v.length > 0)
  if (words.length === 0) return ''
  if (words.length === 1) return words[0]
  return words.slice(0, -1).join(', ') + ' and ' + words.slice(-1)
}

function formatWords(words) {
  if (!words) return "";
  return words.filter(function (a) {
    return a !== ''
  }).join(', ').replace(/(, )+(\S+)$/, ' and $2');
}

function formatWords(words) {
  if (!words || words.length === 0) {
    return '';
  }
  words = words.filter(function (word) {
    return word.length;
  });
  if (words.length == 0) {
    return '';
  } else if (words.length == 1) {
    return words[0];
  } else {
    return [words.slice(0, words.length - 1).join(', '), words.slice(words.length - 1)[0]].join(' and ');
  }
}

function formatWords(a) {
  return (a || []).filter(x => x).join(', ').replace(/,(?= [^,]*$)/, ' and')
}