//You are going to be given a word. Your job is to return the middle character of the word. If the word's length is odd, return the middle character. If the word's length is even, return the middle 2 characters.

function getMiddle(s) {
  let arr = s.split("")
  if (arr.length % 2 == 0) {
    let newArr = arr.splice([Math.floor(arr.length / 2) - 1], 2)
    return newArr.join("")
  } else if (arr.length % 2 == 1) {
    return arr[Math.floor(arr.length / 2)]
  } else if (arr.length == 0) {
    return arr[0]
  }
}

getMiddle("test")
getMiddle("testing")
getMiddle("middle")
getMiddle("A")

// second try
function getMiddle(str) {
  str = str.split("")
  if (str.length == 0) {
    return false
  } else if (str.length == 1) {
    return str[0]
  } else {
    let mid = Math.floor(str.length / 2)
    if (str.length % 2 == 0) {
      return str[mid - 1] + str[mid]
    } else if (str.length % 2 == 1) {
      return str[mid]
    }
  }
}

// other solutions
// function getMiddle(s) {
//   var middle = s.length / 2;
//   return (s.length % 2) ?
//     s.charAt(Math.floor(middle)) :
//     s.slice(middle - 1, middle + 1);
// }

// function getMiddle(s) {
//   return s.slice((s.length - 1) / 2, s.length / 2 + 1);
// }