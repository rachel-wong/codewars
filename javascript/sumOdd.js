
function sumOdd(n) {
  let nth = ((n-1) * (n-1)) + ((n-1)+1)
  let answer = 0
  let counter = 0

  while (counter < n) {
    // if starting number in the row is odd
    if (nth % 2 !== 0) {
      answer += nth
      counter++
    }
    nth++
  }
  return answer
}
