// Bob is preparing to pass IQ test. The most frequent task in this test is to find out which one of the given numbers differs from the others. Bob observed that one number usually differs from the others in evenness. Help Bob — to check his answers, he needs a program that among the given numbers finds one that is different in evenness, and return a position of this number.

// ! Keep in mind that your task is to help Bob solve a real IQ test, which means indexes of the elements start from 1 (not 0)

function iqTest(string) {
  string = string.split(" ").map(Number)
  let even = []
  let odd = []
  for (let i = 0; i < string.length; i++) {
    if (string[i] % 2 === 0) {
      even.push(string[i])
    } else {
      odd.push(string[i])
    }
  }
  if (even.length === 1) {
    return string.indexOf(even[0]) + 1
  }
  if (odd.length === 1) {
    return string.indexOf(odd[0]) + 1
  }
}

console.log(iqTest("2 4 7 8 10")) // => 3 // Third number is odd, while the rest of the numbers are even

console.log(iqTest("1 2 1 1")) // => 2 // Second number is even, while the rest of the numbers are odd

// other solutions

function iqTest(numbers) {
  numbers = numbers.split(" ").map(function (el) {
    return parseInt(el)
  });

  var odd = numbers.filter(function (el) {
    return el % 2 === 1
  });
  var even = numbers.filter(function (el) {
    return el % 2 === 0
  });

  return odd.length < even.length ? (numbers.indexOf(odd[0]) + 1) : (numbers.indexOf(even[0]) + 1);
}

function iqTest(numbers) {
  var nums = numbers.split(" ").map(x => x % 2);
  var sum = nums.reduce((a, b) => a + b);
  var target = sum > 1 ? 0 : 1;

  return nums.indexOf(target) + 1;
}

const iqTest = test => {
  const numbers = test.split(" ");
  const evens = numbers.filter(el => el % 2);
  const odds = numbers.filter(el => !(el % 2));
  const differ = evens.length == 1 ? evens[0] : odds[0]

  return numbers.indexOf(differ) + 1
}

function iqTest(numbers) {
  var ary = numbers.split(" ")
  var even = []
  var odd = []

  ary.forEach(function (num) {
    Number(num) % 2 == 0 ? even.push(num) : odd.push(num)
  })

  return even.length > odd.length ? ary.indexOf(odd[0]) + 1 : ary.indexOf(even[0]) + 1
}