# There is an array with some numbers. All numbers are equal except for one. Try to find it!

def find_uniq(array)
    hash = {}
    for item in arr do
        if hash.key?(item)
            hash[item] += 1
        else
            hash[item] = 1
        end
    end
    p hash.key(1)
end

# OTHER SOLUTIONS
def find_uniq(arr)
    arr.uniq.each { |x| return x if arr.count(x) == 1 }
end

def find_uniq(arr)
    arr.uniq.min_by { |n| arr.count(n) }
  end

# TESTS
# find_uniq([ 1, 1, 1, 2, 1, 1 ]) #== 2
# find_uniq([ 0, 0, 0.55, 0, 0 ]) #== 0.55
find_uniq([1,1,1,1,0]) #== 0