// Create a function taking a positive integer as its parameter and returning a string containing the Roman Numeral representation of that integer.

// Modern Roman numerals are written by expressing each digit separately starting with the left most digit and skipping any digit with a value of zero.In Roman numerals 1990 is rendered: 1000 = M, 900 = CM, 90 = XC;
// resulting in MCMXC.2008 is written as 2000 = MM, 8 = VIII;
// or MMVIII.1666 uses each Roman symbol in descending order: MDCLXVI.

function solution(num) {
  if (num < 0 || isNaN(num)) return NaN // romans don't believe in negativity 
  const encoder = {
    M: 1000,
    CM: 900,
    D: 500,
    CD: 400,
    C: 100,
    XC: 90,
    L: 50,
    XL: 40,
    X: 10,
    IX: 9,
    V: 5,
    IV: 4,
    I: 1
  }

  let result = ''
  for (let i in encoder) {
    while (num >= encoder[i]) {
      result += i;
      num -= encoder[i];
    }
  }
  return result
}

console.log(solution(1990)) //= MCMXC

//other solution
function solution(number) {
  var result = '',
    decimals = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1],
    roman = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I'];

  decimals.map(function (value, index) {
    while (number >= value) {
      result += roman[index];
      number -= value;
    }
  });

  return result;
}