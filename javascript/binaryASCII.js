// Write a function that takes in a binary string and returns the equivalent decoded text (the text is ASCII encoded).
// Each 8 bits on the binary string represent 1 character on the ASCII table.
// The input string will always be a valid binary string.
// Characters can be in the range from "00000000" to "11111111" (inclusive)
// Note: In the case of an empty binary string your function should return an empty string.

// stackoverflow driven development
function binaryToString(binary) {
  if (binary === "") return ""
  binary = binary.match(/.{1,8}/g)
    .map(item => {
      return String.fromCharCode(parseInt(item, 2))
    })
    .join("")
  return binary
}

console.log(binaryToString("01001011010101000100100001011000010000100101100101000101")) // => KTHXBYE

// other solutions

function binaryToString(binary) {
  return binary.replace(/[01]{8}/g, function (v) {
    return String.fromCharCode(parseInt(v, 2));
  });
}

function binaryToString(binary) {
  return binary.replace(/[01]{8}/g, n => String.fromCharCode(parseInt(n, 2)))
}

function binaryToString(binary) {
  let arr = [];
  if (binary.length) {
    for (let i = 0; i < binary.length; i += 8) {
      arr.push(binary.substr(i, 8));
    }
    return arr.map(s => String.fromCharCode(parseInt(s, 2))).join('');
  }
  return '';
}

function binaryToString(binary) {
  var res = '';
  if (binary.length > 0) {
    var arr = binary.match(/.{1,8}/g);
    arr.forEach(function (el) {
      res += String.fromCharCode(parseInt(el, 2));
    });
  }
  return res;
}