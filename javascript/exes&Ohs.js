//Check to see if a string has the same amount of 'x's and 'o's. The method must return a boolean and be case insensitive. The string can contain any char.

function XO(str) {
  let arr = str.toLowerCase().split("")
  let x_count = 0
  let o_count = 0
  arr.forEach(char => {
    if (char == "x") {
      x_count++
    } else if (char == "o") {
      o_count++
    }
  })
  if (x_count == o_count) {
    return true
  } else {
    return false
  }
}

function XO(str) {
  let exes = str.replace(/[o]/ig, "")
  let ohs = str.replace(/[x]/ig, "")
  return exes.length == ohs.length ? true : false
}

XO("ooxx") //true
XO("xooxx") //false
XO("ooxXm") //true
XO("zpzpzpp") //true // when no 'x' and 'o' is present should return true
XO("zzoo") // false

//other solutions 
// function XO(str) {
//   let x = str.match(/x/gi);
//   let o = str.match(/o/gi);
//   return (x && x.length) === (o && o.length);
// }

// const XO = str => {
//   str = str.toLowerCase().split('');
//   return str.filter(x => x === 'x').length === str.filter(x => x === 'o').length;
// }

// function XO(str) {
//   return str.toLowerCase().split('x').length === str.toLowerCase().split('o').length;
// }