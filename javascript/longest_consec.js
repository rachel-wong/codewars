// You are given an array strarr of strings and an integer k. 
// Your task is to return the first longest string consisting of k consecutive strings taken in the array.
// n being the length of the string array, if n = 0 or k > n or k <= 0 return "".

// had help
function longest_consec(array, k) {
  if (array.length === 0 || k > array.length || k <= 0) return "" // from the question

  let check = []
  for (let i = 0; i < array.length; i++) {
    check.push(array.slice(i, i + k).join(""))
  }

  // what is first biggest? the one in front and the one after are smaller than the current
  return check
}

console.log(longest_consec(["zone", "abigail", "theta", "form", "libe", "zas", "theta", "abigail"], 2)) // => "abigailtheta"

// There was an attempt and egads! was it gnarly and ugly
// function longest_consec(arr, k) {
//   let count = []
//   let largest_word = ""
//   let second_largest_word = ""
//   if (arr.length === 0 || arr.length < k || k <= 0) return "" // handling general silliness
//   for (let i = 0; i < arr.length; i++) {
//     count.push(arr[i].length)
//   }
//   let largest = Math.max.apply(null, count)
//   count.splice(count.indexOf(largest), 1)
//   let second_largest = Math.max.apply(null, count)
//   for (let i = 0; i < arr.length; i++) {
//     if (arr[i].length === largest) {
//       largest_word = arr[i]
//       i++
//     } else if (arr[i].length === second_largest) {
//       second_largest_word = arr[i]
//       i++
//     }
//   }
//   return largest_word + second_largest_word
// }