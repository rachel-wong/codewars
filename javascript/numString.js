// You are given an input string.

// For each symbol in the string if it's the first character occurence, replace it with a '1', else replace it with the amount of times you've already seen it...

function numericalString(string){
  let count = {}
  let result = []
  string.split("").forEach(item => {
    if (count[item] == undefined) {
      count[item] = 1
      result.push(1)
    } else {
      count[item]++
      result.push(count[item])
    }
  })
  return result.join("")
}

console.log(numericalString("Hello, World!")) // => 1112111121311
console.log(numericalString("aaaaaaaaaaaa")) // => 123456789101112

// other solutions

const numericals = (str, seen = {}) =>
  str.replace(/./g, char =>
    seen[char] = (seen[char] || 0) + 1)

function numericals(s) {
  const count = {};
  return s.split('').map(char => count[char] = ++count[char] || 1).join('');
}

function numericals(s) {
  let seen = {};
  let result = [];
  s.split('').forEach((c) => {
    seen[c] = (seen[c] || 0) + 1;
    result.push(seen[c]);
  });
  return result.join('');
}