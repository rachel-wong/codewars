//You will be given a number and you will need to return it as a string in Expanded Form

// expandedForm(12); // Should return '10 + 2'
// expandedForm(42); // Should return '40 + 2'
// expandedForm(70304); // Should return '70000 + 300 + 4'

function expandedForm(num) {
  let numStringArray = String(num).split('')
  let length = numStringArray.length
  let result = ''
  numStringArray.map((num, index) => {
    if (num > 0) {
      if (result) {
        result += ' + ';
      }
      result += num.padEnd(length - index, '0')
    }
  })
  return result
}

// console.log(expandedForm(12)); // Should return '10 + 2'
// console.log(expandedForm(42)); // Should return '40 + 2'
// console.log(expandedForm(70304)); // Should return '70000 + 300 + 4'
// console.log(expandedForm(9000000));

// other solutions
function expandedForm(num) {
  return String(num)
    .split("")
    .map((num, index, arr) => num + "0".repeat(arr.length - index - 1))
    .filter((num) => Number(num) != 0)
    .join(" + ")
}

var expandedForm = (num) => {
  var arr = num.toString().split('').reverse();
  var result = [];
  for (var i = 0; i < arr.length; i++) {
    arr[i] == 0 ? result.push() : result.push(arr[i] + ('0'.repeat(i)))
  }
  return result.reverse().join(' + ')
}

function expandedForm(num) {
  if (num < 10) return `${num}`;
  let over = num % (Math.pow(10, (num.toString().length - 1)));
  if (!over) return `${num}`;
  return `${num-over} + ${expandedForm(over)}`;
}

function expandedForm(num) {
  // Your code here
  var str = num + '';
  return str.split('').map((item, index) => {
    if (item > 0) {
      return item * Math.pow(10, (str.length - index - 1))
    }
  }).filter((i) => i).join(' + ')
}